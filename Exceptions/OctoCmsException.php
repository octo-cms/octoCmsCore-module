<?php

namespace OctoCmsModule\Core\Exceptions;

use Exception;
use Throwable;

/**
 *
 * Class OctoCmsException
 * @package OctoCmsModule\Core\Exceptions
 */
class OctoCmsException extends Exception
{
    /**
     * OctoCmsException constructor.
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", int $code = 0, Throwable $previous = null)
    {
        if (is_array($message)) {
            $message = implode('<br>', $message);
        }

        parent::__construct($message, $code, $previous);
    }
}
