<?php

/**
 * Name getRouteAction
 *
 * @param string $moduleName     Module Name
 * @param string $controllerName Controller Name
 * @param string $action         Action
 *
 * @return string
 */
function getRouteAction(string $moduleName, string $controllerName, string $action): string
{

    $defaultController = "OctoCmsModule\\" . $moduleName . "\Http\Controllers\\" . $controllerName;
    $customController = "OctoCms\\" . $moduleName . "\Http\Controllers\\" . $controllerName;

    return class_exists($customController)
        ? $customController . '@' . $action
        : $defaultController . '@' . $action;
}

/**
 * Name getBindingImplementation
 *
 * @param mixed  $moduleNames Module name
 * @param string $path        path
 *
 * @return string
 */
function getBindingImplementation($moduleNames, string $path): string
{
    if (!is_array($moduleNames)) {
        $moduleNames = [$moduleNames];
    }

    foreach ($moduleNames as $moduleName) {
        $customImplementation = "OctoCms\\" . $moduleName . "\\" . $path;
        if (class_exists($customImplementation)) {
            return $customImplementation;
        }
    }

    foreach ($moduleNames as $moduleName) {
        $defaultImplementation = "OctoCmsModule\\" . $moduleName . "\\" . $path;
        if (class_exists($defaultImplementation)) {
            return $defaultImplementation;
        }
    }

    return '';
}

/**
 * Name getBindingStub
 *
 * @param mixed  $moduleNames Module name
 * @param string $path        path
 *
 * @return string
 */
function getBindingStub($moduleNames, string $path): string
{
    if (!is_array($moduleNames)) {
        $moduleNames = [$moduleNames];
    }

    foreach ($moduleNames as $moduleName) {
        $customImplementation = "Tests\\" . $moduleName . "\\Mocks\\" . $path;
        if (class_exists($customImplementation)) {
            return $customImplementation;
        }
    }

    foreach ($moduleNames as $moduleName) {
        $defaultImplementation = "OctoCmsModule\\" . $moduleName . "\\Tests\Mocks\\" . $path;
        if (class_exists($defaultImplementation)) {
            return $defaultImplementation;
        }
    }

    return '';
}
