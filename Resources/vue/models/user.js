const User = {
    id: null,
    username: '',
    defaultEmail: '',
    roles: [],
    emails: [],
};

export default User
