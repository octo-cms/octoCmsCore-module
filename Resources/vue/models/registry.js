const Registry = {
    id: null,
    code: '',
    type: '',
    privateRegistry: {
        name: '',
        surname: '',
        codfis: '',
        birth_date: ''
    },
    companyRegistry: {
        businessname: '',
        referral: '',
        codfis: '',
        piva: '',
        web: '',
        company_form: null
    },
    emails: [],
};

const RegistryType = {
    COMPANY: "company",
    PRIVATE: "private"
};

export {Registry, RegistryType};
