const routes = {

    /** USERS **/
    USER_STORE: 'api/admin/v1/users',
    USER_UPDATE: 'api/admin/v1/users/{id}',
    USER_GENERATE_NEW_PASSWORD: 'api/admin/v1/users/{id}/generate-new-password',

    /** REGISTRY **/
    REGISTRY_STORE: 'api/admin/v1/registries',
    REGISTRY_UPDATE: 'api/admin/v1/registries/{id}',

    /** ROLES **/
    ROLE_INDEX: 'api/admin/v1/roles',

    /** DATATABLES **/
    DATATABLES: {
        registries: 'api/admin/v1/datatables/core/registries',
        users: 'api/admin/v1/datatables/core/users',
    },
};

export {
    routes
}
