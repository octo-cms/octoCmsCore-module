const CoreSettings = {
    admin_mail: '',
    active_campaign: {enabled: false, api_key: null, url: null, list_id: null},
};

export default CoreSettings;
