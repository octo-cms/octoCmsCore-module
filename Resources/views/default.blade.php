<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{env('APP_NAME')}}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 60px;
            text-transform: uppercase;
        }

        .subtitle {
            font-size: 48px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        .error {
            color: #6c1d19;
        }

        .success {
            color: #1c4b30;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <div class="title m-b-md">
            {{env('APP_NAME')}}<br><small>{{env('APP_ENV')}}</small>
        </div>
        <div class="subtitle m-b-md">
            Laravel {{ app()->version() }}
        </div>
        <div class="links">
            <a href>Module: <span class="success">{!! $module !!}</span></a>
        </div>
        <div class="links">
            <a href>Blade: <span class="success">{!! $blade !!}</span></a>
        </div>
        @php($pdo = optional(DB::connection())->getPdo())
        <div class="links">
            <a href>Database:
                @if(!empty($pdo)) <span class="success">OK</span> @else <span class="error">KO</span>@endif
            </a>
        </div>
    </div>
</div>
</body>
</html>

