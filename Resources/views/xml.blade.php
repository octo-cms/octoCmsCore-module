<phpunit xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:noNamespaceSchemaLocation="https://schema.phpunit.de/9.3/phpunit.xsd">
    <coverage processUncoveredFiles="true">
        <include>
            @foreach($modules as $module)
                <directory suffix=".php">./Modules/{{$module['folder']}}/Http/Controllers</directory>
                <directory suffix=".php">./Modules/{{$module['folder']}}/Entities</directory>
                <directory suffix=".php">./Modules/{{$module['folder']}}/Services</directory>
                <directory suffix=".php">./Modules/{{$module['folder']}}/Console</directory>
                <directory suffix=".php">./Modules/{{$module['folder']}}/View</directory>
                <directory suffix=".php">./Modules/{{$module['folder']}}/Rules</directory>
            @endforeach
        </include>
    </coverage>
    <testsuites>
        <testsuite name="all">
            <directory suffix="Test.php">./tests</directory>
        </testsuite>
        @foreach($modules as $module)
        <testsuite name="{{$module['suite']}}">
            <directory suffix="Test.php">./Modules/{{$module['folder']}}/Tests</directory>
        </testsuite>
        @endforeach
    </testsuites>
    <php>
        <server name="APP_ENV" value="testing"/>
        <server name="BCRYPT_ROUNDS" value="4"/>
        <server name="CACHE_DRIVER" value="array"/>
        <server name="DB_CONNECTION" value="sqlite"/>
        <server name="DB_DATABASE" value=":memory:"/>
        <server name="MAIL_MAILER" value="array"/>
        <server name="QUEUE_CONNECTION" value="sync"/>
        <server name="SESSION_DRIVER" value="array"/>
        <server name="LOGGER_ENABLED" value="false"/>
        <server name="CACHE_ENABLED" value="true"/>
    </php>
</phpunit>
