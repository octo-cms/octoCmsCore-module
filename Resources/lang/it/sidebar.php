<?php

// resources/lang/en/messages.php

return [
    'registry' => 'anagrafica',
    'list'     => 'lista',
    'import'   => 'importazione',
    'users'    => 'utenti',
];
