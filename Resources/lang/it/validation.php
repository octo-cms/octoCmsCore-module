<?php

return [
    'username'     => [
        'required' => 'Il campo username è obbligatorio.',
        'unique'   => 'Il campo username deve essere unico.',
        'string'   => 'Il campo username deve essere una stringa.',
    ],
    'roles'        => [
        'required' => 'Devi selezionare almeno un (1) ruolo.',
        '*'        => [
            'id' => ['required' => 'Non è presente il campo id in almeno uno dei ruoli selezionati']
        ]
    ],
    'defaultEmail' => [
        'required' => 'Il campo default email è obbligatorio.',
        'email'    => 'Inserire un indirizzo email valido',
    ],
    'active'       => [
        'required' => 'Il campo active è obbligatorio.',
    ],
    'password'     => [
        'required' => 'Il campo password è obbligatorio.',
        'string'   => 'Il campo password deve essere una stringa.',
    ],
    'email' => [
        'required' => 'Il campo default email è obbligatorio.',
        'email'    => 'Inserire un indirizzo email valido',
    ],
];
