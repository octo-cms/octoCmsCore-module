<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Entities\ProviderImportData;
use OctoCmsModule\Core\Entities\Registry;

/**
 * Class ProviderFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class ProviderImportDataFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProviderImportData::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date'               => $this->faker->date(),
            'provider_import_id' => ProviderImport::factory(),
            'registry_id'        => Registry::factory(),
            'provider_id'        => Provider::factory(),
            'name'               => $this->faker->firstName,
            'surname'            => $this->faker->lastName,
            'email'              => $this->faker->email,
            'price'              => 0,
            'data'               => [],
            'registry_type'      => ProviderImportData::REGISTRY_TYPE_NEW,
        ];
    }
}
