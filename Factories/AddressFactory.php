<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\Address;
use OctoCmsModule\Core\Entities\Country;

/**
 * Class AddressFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class AddressFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Address::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'default'                     => false,
            'alias'                       => $this->faker->slug('3'),
            'place_id'                    => null,
            'street_address'              => $this->faker->address,
            'route'                       => '',
            'political'                   => '',
            'country'                     => $this->faker->country,
            'administrative_area_level_1' => $this->faker->randomElement(['Lombardia', 'Veneto', 'Piemonte']),
            'administrative_area_level_2' => $this->faker->randomElement(['MI', 'TO', 'LC']),
            'administrative_area_level_3' => '',
            'administrative_area_level_4' => '',
            'administrative_area_level_5' => '',
            'colloquial_area'             => null,
            'locality'                    => $this->faker->city,
            'sublocality'                 => null,
            'neighborhood'                => null,
            'premise'                     => null,
            'subpremise'                  => null,
            'plus_code'                   => null,
            'postal_code'                 => $this->faker->postcode,
            'natural_feature'             => null,
            'airport'                     => null,
            'park'                        => null,
            'point_of_interest'           => null,
            'other'                       => null,

        ];
    }
}
