<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\ProviderImport;

/**
 * Class ProviderFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class ProviderImportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProviderImport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'status'  => 'pending',
            'errors'  => []
        ];
    }
}
