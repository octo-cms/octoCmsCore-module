<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\PrivateData;
use OctoCmsModule\Core\Entities\User;

/**
 * Class PrivateDataFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class PrivateDataFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PrivateData::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'codfis'  => $this->faker->ean13,
        ];
    }
}
