<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\Provider;

/**
 * Class ProviderFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class ProviderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Provider::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->unique()->words(5, true);

        return [
            'name'         => $name,
            'slug'         => $this->faker->unique()->text(30),
            'enabled'      => $this->faker->boolean,
            'xls_mapping'  => $this->faker->text,
            'record_price' => $this->faker->randomFloat(2, 2, 10)
        ];
    }
}
