<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\Media;
use OctoCmsModule\Core\Entities\MediaLang;

/**
 * Class MediaLangFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class MediaLangFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MediaLang::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'media_id'    => Media::factory(),
            'lang'        => $this->faker->randomElement(['it', 'en', 'fr']),
            'alt'         => $this->faker->slug(3),
            'title'       => $this->faker->text(50),
            'caption'     => $this->faker->text(50),
            'description' => $this->faker->text(100),
        ];
    }
}
