<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\Video;

/**
 * Class VideoFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class VideoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Video::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'source_type' => 'default',
        ];
    }
}
