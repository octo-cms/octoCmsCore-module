<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\Tag;

/**
 * Class TagFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class TagFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tag::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'         => 'tag-' . $this->faker->randomNumber(5),
            'tag_group_id' => null,
        ];
    }
}
