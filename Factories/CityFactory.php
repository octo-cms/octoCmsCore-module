<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\City;

/**
 * Class CityFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class CityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = City::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'cod_regione'                 => $this->faker->ean8,
            'regione'                     => $this->faker->country,
            'cod_sovracomunale'           => $this->faker->countryCode,
            'sovracomunale'               => $this->faker->city,
            'cod_provincia'               => $this->faker->countryCode,
            'progressivo'                 => $this->faker->numberBetween(100, 999),
            'cod_catastale'               => $this->faker->countryCode,
            'cod_comune_alfanumerico'     => $this->faker->unique()->countryCode,
            'cod_comune_numerico'         => $this->faker->countryCode,
            'denominazione'               => $this->faker->city,
            'cod_ripartizione_geografica' => $this->faker->countryCode,
            'ripartizione_geografica'     => $this->faker->numberBetween(1, 5),
            'capoluogo'                   => $this->faker->numberBetween(0, 10),
            'sigla_automobilistica'       => $this->faker->countryCode,
            'nuts1'                       => $this->faker->ean8,
            'nuts2'                       => $this->faker->ean8,
            'nuts3'                       => $this->faker->ean8,
        ];
    }
}
