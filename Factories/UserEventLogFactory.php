<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Entities\UserEventLog;

/**
 * Class UserEventLogFactory
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Factories
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class UserEventLogFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = UserEventLog::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'user_id'    => User::factory(),
            'ip_address' => $this->faker->ipv4,
            'type'       => $this->faker->randomElement([UserEventLog::EVENT_LOGIN, UserEventLog::EVENT_LOGOUT]),
        ];
    }
}
