<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\CustomFieldEntityDecimal;

/**
 * Class CustomFieldEntityDecimalFactory
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Core\Factories
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class CustomFieldEntityDecimalFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CustomFieldEntityDecimal::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'value' => $this->faker->randomFloat(2, 0, 100),
        ];
    }
}
