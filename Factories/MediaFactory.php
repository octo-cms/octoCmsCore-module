<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\Media;

/**
 * Class MediaFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class MediaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Media::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $types = config('mediable.aggregate_types');
        $type = $this->faker->randomElement(array_keys($types));

        return [
            'disk'           => 'tmp',
            'directory'      => implode('/', $this->faker->words($this->faker->randomDigit)),
            'filename'       => $this->faker->word,
            'extension'      => $this->faker->randomElement($types[$type]['extensions']),
            'mime_type'      => $this->faker->randomElement($types[$type]['mime_types']),
            'aggregate_type' => $type,
            'size'           => $this->faker->randomNumber(),
        ];
    }
}
