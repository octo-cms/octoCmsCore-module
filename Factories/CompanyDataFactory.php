<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\CompanyData;
use OctoCmsModule\Core\Entities\User;

/**
 * Class CompanyDataFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class CompanyDataFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CompanyData::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'codfis'  => $this->faker->ean13,
            'piva'    => $this->faker->ean13,
        ];
    }
}
