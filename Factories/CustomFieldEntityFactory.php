<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\CustomField;
use OctoCmsModule\Core\Entities\CustomFieldEntity;

/**
 * Class CustomFieldEntityFactory
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Core\Factories
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class CustomFieldEntityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CustomFieldEntity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'custom_field_id' => CustomField::factory(),
        ];
    }
}
