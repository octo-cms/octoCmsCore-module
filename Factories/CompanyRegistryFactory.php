<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\CompanyRegistry;
use OctoCmsModule\Core\Entities\Registry;

/**
 * Class CompanyRegistryFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class CompanyRegistryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CompanyRegistry::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'registry_id'  => Registry::factory(),
            'businessname' => $this->faker->name,
            'codfis'       => $this->faker->ean13,
            'piva'         => $this->faker->ean13,
        ];
    }
}
