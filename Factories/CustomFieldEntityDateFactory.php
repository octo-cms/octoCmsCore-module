<?php

namespace OctoCmsModule\Core\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\CustomFieldEntityDate;

/**
 * Class CustomFieldEntityDateFactory
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Core\Factories
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class CustomFieldEntityDateFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CustomFieldEntityDate::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'value' => Carbon::now()->format('Y-m-d'),
        ];
    }
}
