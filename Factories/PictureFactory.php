<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\Picture;

/**
 * Class PictureFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class PictureFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Picture::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'src' => $this->faker->ean13 . '.jpeg',
            'tag' => Picture::TAG_MAIN,
        ];
    }
}
