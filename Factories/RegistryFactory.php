<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Entities\User;

/**
 * Class RegistryFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class RegistryFactory extends Factory
{

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Registry::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'code'    => $this->faker->ean8,
            'user_id' => null,
            'type'    => $this->faker->randomElement([ Registry::TYPE_PRIVATE, Registry::TYPE_COMPANY]),
        ];
    }
}
