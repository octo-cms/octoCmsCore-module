<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Core\Entities\PictureLang;

/**
 * Class PictureLangFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class PictureLangFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PictureLang::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'picture_id'  => Picture::factory(),
            'lang'        => $this->faker->randomElement(['it', 'en', 'fr']),
            'alt'         => $this->faker->slug(3),
            'title'       => $this->faker->text(50),
            'caption'     => $this->faker->text(50),
            'description' => $this->faker->text(100),
        ];
    }
}
