<?php

declare(strict_types=1);

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\PrivateRegistry;
use OctoCmsModule\Core\Entities\Registry;

/**
 * @package OctoCmsModule\Core\Factories
 */
class PrivateRegistryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     */
    // phpcs:disable
    protected $model = PrivateRegistry::class;
    // phpcs:enable

    /**
     * Name definition
     *
     * @return array|mixed[]
     */
    public function definition(): array
    {
        return [
            'registry_id' => Registry::factory(),
            'name'        => $this->faker->firstName,
            'surname'     => $this->faker->lastName,
            'codfis'      => $this->faker->ean13,
            'gender'      => $this->faker->randomElement(['M', 'F'])
        ];
    }
}
