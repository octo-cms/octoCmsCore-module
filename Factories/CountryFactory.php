<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\Country;

/**
 * Class CountryFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class CountryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Country::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'sigla_numerica' => $this->faker->numberBetween(100, 999),
            'sigla_alpha_2'  => $this->faker->unique()->countryCode,
            'sigla_alpha_3'  => $this->faker->countryCode,
            'name_it'        => $this->faker->country,
            'name_en'        => $this->faker->country,
            'name_es'        => $this->faker->country,
            'name_de'        => $this->faker->country,
            'name_fr'        => $this->faker->country,
        ];
    }
}
