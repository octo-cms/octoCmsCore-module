<?php

namespace OctoCmsModule\Core\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use OctoCmsModule\Core\Entities\Video;
use OctoCmsModule\Core\Entities\VideoLang;

/**
 * Class VideoLangFactory
 *
 * @package OctoCmsModule\Core\Factories
 */
class VideoLangFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = VideoLang::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'video_id'    => Video::factory(),
            'lang'        => $this->faker->randomElement(['it', 'en', 'fr']),
            'alt'         => $this->faker->slug(3),
            'title'       => $this->faker->text(50),
            'caption'     => $this->faker->text(50),
            'description' => $this->faker->text(100),
        ];
    }
}
