## Module Core

### Overview
This module provides to set CORE functions for OctoCms.\
It is mandatory to use OctoCms

### Installation
Add in your composer.json
```
"repositories": [
        ...
        {
            "type": "vcs",
            "url": "https://gitlab.com/octo-cms/octoCmsCore-module.git"
        },
        ...
    ]
```

Install or update module
```
composer install octo-cms-module/core-module
or
composer update octo-cms-module/core-module
```
Run Migrations
```
php artisan module:migrate Core
```
Install module by artisan command
```
php artisan install:core --lang=*
```
This command provides to install platform basic setting.\
If you would install a multilanguage platform version, yuo can set any number of languages ( es. php artisan install:core --lang=it --lang=en --lang=fr) 
It will create an HomePage and a Main Menu.

### Other Artisan Commands
To generate sitemap
```
php artisan sitemap:generate
```
It's recommended to set a cronjob on your server to generare new sitemap everyday.



