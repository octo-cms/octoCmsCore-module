<?php

namespace OctoCmsModule\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use OctoCmsModule\Core\Traits\SavePictureRequestTrait;

/**
 * Class SaveProviderRequest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Http\Requests
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class StoreProviderRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'enabled'      => 'required|boolean',
            'name'         => 'required',
            'xls_mapping'  => 'sometimes|nullable|array',
            'slug'         => 'required|unique:providers,slug',
            'record_price' => 'sometimes',
            'acronym'      => 'sometimes|nullable',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
