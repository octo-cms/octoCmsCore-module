<?php

namespace OctoCmsModule\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class LoginUserRequest
 *
 * @package OctoCmsModule\Core\Http\Requests
 * @author  danielepasi
 */
class StoreMediaRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'filename'       => 'required|string',
            'directory'      => 'required|string',
            'size'           => 'sometimes',
            'mime_type'      => 'sometimes|string|nullable',
            'aggregate_type' => 'sometimes|string|nullable',
            'extension'      => 'sometimes|string|nullable',
            'disk'           => 'sometimes|string|nullable',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
