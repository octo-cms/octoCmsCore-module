<?php

namespace OctoCmsModule\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateUserPasswordRequest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Http\Requests
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class UpdateUserPasswordRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $addLowercase = config('octo-cms.password_reqs.add_lowercase');
        $addUppercase = config('octo-cms.password_reqs.add_uppercase');
        $addDigit = config('octo-cms.password_reqs.add_digit');
        $addSpecial = config('octo-cms.password_reqs.add_special');

        return [
            'password' => 'required|string|min:8' .        // must be at least 8 characters in length
                ($addLowercase ? '|regex:/[a-z]/' : '') .  // must contain at least one lowercase letter
                ($addUppercase ? '|regex:/[A-Z]/' : '') .  // must contain at least one uppercase letter
                ($addDigit ? '|regex:/[0-9]/' : '') .      // must contain at least one digit
                ($addSpecial ? '|regex:/[@$!%*#?&]/' : '') // must contain a special character
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
