<?php

namespace OctoCmsModule\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use OctoCmsModule\Core\Traits\SavePictureRequestTrait;

/**
 * Class UpdateProviderRequest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Http\Requests
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class UpdateProviderRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'           => 'required|integer',
            'enabled'      => 'required|boolean',
            'name'         => 'required',
            'xls_mapping'  => 'sometimes|nullable|array',
            'slug'         => 'required|unique:providers,slug,' . $this->id,
            'record_price' => 'sometimes',
            'acronym'      => 'sometimes|nullable',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
