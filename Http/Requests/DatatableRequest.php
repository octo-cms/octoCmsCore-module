<?php

namespace OctoCmsModule\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class DatatableRequest
 *
 * @package OctoCmsModule\Core\Http\Requests
 */
class DatatableRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'query'          => 'sometimes',
            'filters'        => 'sometimes',
            'currentPage'    => 'required',
            'rowsInPage'     => 'required',
            'orderBy'        => 'nullable',
            'orderDirection' => 'nullable',
            'export'         => 'sometimes'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
