<?php

namespace OctoCmsModule\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Rules\CodFis;
use OctoCmsModule\Core\Rules\PIva;
use OctoCmsModule\Core\Traits\SaveAddressRequestTrait;
use OctoCmsModule\Core\Traits\SaveEmailRequestTrait;
use OctoCmsModule\Core\Traits\SavePhoneRequestTrait;

/**
 * Class SaveRegistryRequest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Http\Requests
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class SaveRegistryRequest extends FormRequest
{
    use SaveAddressRequestTrait, SaveEmailRequestTrait, SavePhoneRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return Arr::collapse(
            [
                [
                    'type'                         => 'required|in:' . Registry::TYPE_PRIVATE . ','
                        . Registry::TYPE_COMPANY,
                    'code'                         => 'sometimes|string|max:20|nullable',
                    'user_id'                      => 'sometimes|exists:users,id|nullable',
                    'privateRegistry.name'         => 'sometimes|exclude_unless:type,' . Registry::TYPE_PRIVATE .
                        '|string',
                    'privateRegistry.surname'      => 'sometimes|exclude_unless:type,' . Registry::TYPE_PRIVATE .
                        '|string',
                    'privateRegistry.gender'       => 'sometimes|exclude_unless:type,' . Registry::TYPE_PRIVATE .
                        '|string',
                    'privateRegistry.codfis'       => ['sometimes', 'string', new CodFis(), 'nullable'],
                    'privateRegistry.birth_date'   => 'sometimes|exclude_unless:type,' . Registry::TYPE_PRIVATE
                        . '|date|nullable',
                    'companyRegistry.businessname' => 'sometimes|exclude_unless:type,' . Registry::TYPE_COMPANY .
                        '|string',
                    'companyRegistry.referral'     => 'sometimes|exclude_unless:type,' . Registry::TYPE_COMPANY
                        . '|string|nullable',
                    'companyRegistry.piva'         => ['sometimes', 'string', new PIva(), 'nullable'],
                    'companyRegistry.codfis'       => ['sometimes', 'string', new CodFis(), 'nullable'],
                    'companyRegistry.web'          => 'sometimes|exclude_unless:type,' . Registry::TYPE_COMPANY
                        . '|string|nullable',
                    'customFields'                 => 'sometimes|array',
                ],
                $this->getAddressRules(),
                $this->getPhoneRules(),
                $this->getEmailRules()
            ]
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
