<?php

namespace OctoCmsModule\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class SaveLeadRequest
 *
 * @package OctoCmsModule\Core\Http\Requests
 */
class SaveLeadRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'      => 'required',
            'first_name' => 'sometimes',
            'last_name'  => 'sometimes',
            'phone'      => 'sometimes',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
