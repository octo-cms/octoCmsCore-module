<?php

namespace OctoCmsModule\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Rules\CodFis;
use OctoCmsModule\Core\Rules\PIva;

/**
 * Class UpdateRegistryPersonalDataRequest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Http\Requests
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class UpdateRegistryPersonalDataRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'type'                         => 'required|in:' . Registry::TYPE_PRIVATE . ','
                . Registry::TYPE_COMPANY,
            'code'                         => 'sometimes|string|max:20|nullable',
            'user_id'                      => 'sometimes|exists:users,id|nullable',
            'privateRegistry.name'         => 'sometimes|exclude_unless:type,' . Registry::TYPE_PRIVATE .
                '|string',
            'privateRegistry.surname'      => 'sometimes|exclude_unless:type,' . Registry::TYPE_PRIVATE .
                '|string',
            'privateRegistry.gender'       => 'sometimes|exclude_unless:type,' . Registry::TYPE_PRIVATE .
                '|string',
            'privateRegistry.codfis'       => ['sometimes', 'string', new CodFis(), 'nullable'],
            'privateRegistry.birth_date'   => 'sometimes|exclude_unless:type,' . Registry::TYPE_PRIVATE
                . '|date|nullable',
            'companyRegistry.businessname' => 'sometimes|exclude_unless:type,' . Registry::TYPE_COMPANY .
                '|string',
            'companyRegistry.referral'     => 'sometimes|exclude_unless:type,' . Registry::TYPE_COMPANY
                . '|string|nullable',
            'companyRegistry.piva'         => ['sometimes', 'string', new PIva(), 'nullable'],
            'companyRegistry.codfis'       => ['sometimes', 'string', new CodFis(), 'nullable'],
            'companyRegistry.web'          => 'sometimes|exclude_unless:type,' . Registry::TYPE_COMPANY
                . '|string|nullable',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
