<?php

namespace OctoCmsModule\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class LoginUserRequest
 *
 * @package OctoCmsModule\Core\Http\Requests
 * @author  danielepasi
 */
class HandleMediaToAddRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image'               => 'sometimes|array',
            'crop'                => 'sometimes|array',
            'image.width'         => 'sometimes|nullable',
            'image.height'        => 'sometimes|nullable',
            'crop.width'          => 'sometimes|nullable',
            'crop.height'         => 'sometimes|nullable',
            'crop.containerWidth'  => 'sometimes|nullable',
            'crop.containerWeight' => 'sometimes|nullable',
            'crop.offsetX'        => 'sometimes|nullable',
            'crop.offsetY'        => 'sometimes|nullable',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
