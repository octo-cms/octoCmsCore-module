<?php

namespace OctoCmsModule\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class LoginUserRequest
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Http\Requests
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class LoginUserRequest extends FormRequest
{
    /**
     * Name rules
     *
     * @return string[]
     */
    public function rules()
    {
        return [
            'username' => 'required|string',
            'password' => 'required|string',
        ];
    }

    /**
     * Name authorize
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Name messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username.required' => __('core::validation.username.required'),
            'username.string'   => __('core::validation.username.string'),
            'password.required' => __('core::validation.password.required'),
            'password.string'   => __('core::validation.password.string'),
        ];
    }
}
