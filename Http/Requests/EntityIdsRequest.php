<?php

namespace OctoCmsModule\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class EntityIdsRequest
 *
 * @package OctoCmsModule\Core\Http\Requests
 */
class EntityIdsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'excludedIds' => 'present|array',
            'currentPage' => 'required',
            'rowsInPage'  => 'required',
            'orderBy'     => 'sometimes',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
