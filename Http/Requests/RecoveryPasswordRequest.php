<?php

namespace OctoCmsModule\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RecoveryPasswordRequest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Http\Requests
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class RecoveryPasswordRequest extends FormRequest
{

    /**
     * Name rules
     *
     * @return string[]
     */
    public function rules()
    {
        return
            [
                'username' => 'required|string',
                'email'    => 'required|email',
            ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Name messages
     *
     * @return array|string[]
     */
    public function messages()
    {
        return [
            'username.required' => __('core::validation.username.required'),
            'email.required'    => __('core::validation.email.required'),
            'email.email'       => __('core::validation.email.email'),
        ];
    }
}
