<?php

namespace OctoCmsModule\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Traits\SaveAddressRequestTrait;
use OctoCmsModule\Core\Traits\SaveEmailRequestTrait;
use OctoCmsModule\Core\Traits\SavePhoneRequestTrait;

/**
 * Class StoreUserRequest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Http\Requests
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class StoreUserRequest extends FormRequest
{
    use SaveAddressRequestTrait, SaveEmailRequestTrait, SavePhoneRequestTrait;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Arr::collapse(
            [
            [
                'username'     => 'required|unique:users,username',
                'roles'        => 'required|array',
                'roles.*.id'   => 'required',
                'customFields' => 'sometimes|array',
            ],
            $this->getAddressRules(),
            $this->getPhoneRules(),
            $this->getEmailRules()
            ]
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Name messages
     *
     * @return array|string[]
     */
    public function messages()
    {
        return [
            'username.required'     => __('core::validation.username.required'),
            'username.unique'       => __('core::validation.username.unique'),
            'roles.required'        => __('core::validation.roles.required'),
            'roles.*.id.required'   => __('core::validation.roles.*.id.required'),
            'defaultEmail.required' => __('core::validation.defaultEmail.required'),
            'defaultEmail.email'    => __('core::validation.defaultEmail.email'),
        ];
    }
}
