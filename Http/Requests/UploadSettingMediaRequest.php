<?php

namespace OctoCmsModule\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UploadSettingMediaRequest
 *
 * @package OctoCmsModule\Core\Http\Requests
 */
class UploadSettingMediaRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'src'    => 'required',
            'height' => 'sometimes',
            'width'  => 'sometimes',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
