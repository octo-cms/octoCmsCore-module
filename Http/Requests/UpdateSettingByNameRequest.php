<?php

namespace OctoCmsModule\Core\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class UpdateSettingByNameRequest
 *
 * @package OctoCmsModule\Core\Http\Requests
 */
class UpdateSettingByNameRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'   => 'required|string',
            'value'  => 'present',
            'entity' => 'sometimes|string',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
