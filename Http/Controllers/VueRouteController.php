<?php

namespace OctoCmsModule\Core\Http\Controllers;

use App\Http\Controllers\Controller;

/**
 * Class VueRouteController
 * @package OctoCmsModule\Admin\Http\Controllers
 */
class VueRouteController extends Controller
{
    /**
     * @return mixed
     */
    public function registry()
    {

        return view()->first([
            'admin.vue-full-page',
            'admin::vue-full-page'
        ], ['script' => 'core/registry']);
    }
    /**
     * @return mixed
     */
    public function users()
    {

        return view()->first([
            'admin.vue-full-page',
            'admin::vue-full-page'
        ], ['script' => 'core/users']);
    }
}
