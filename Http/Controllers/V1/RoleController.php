<?php

namespace OctoCmsModule\Core\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use OctoCmsModule\Core\Entities\Role;
use OctoCmsModule\Core\Transformers\RoleResource;

/**
 * Class RoleController
 *
 * @package OctoCmsModule\Core\Http\Controllers\V1
 */
class RoleController extends Controller
{
    /**
     * Name index
     *
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', Role::class);

        return response()->json(RoleResource::collection(Role::all()), Response::HTTP_OK);
    }
}
