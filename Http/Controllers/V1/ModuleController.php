<?php

namespace OctoCmsModule\Core\Http\Controllers\V1;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Nwidart\Modules\Facades\Module;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ModuleController
 *
 * @package OctoCmsModule\Core\Http\Controllers
 */
class ModuleController extends Controller
{

    /**
     * @return JsonResponse|object
     */
    public function index()
    {
        $moduleNames = [];

        /**
 * @var \Nwidart\Modules\Laravel\Module $module
*/
        foreach (Module::all() as $module) {
            if ($module->isEnabled()) {
                $moduleNames[] = $module->getLowerName();
            }
        }

        return response()->json($moduleNames)->setStatusCode(Response::HTTP_OK);
    }
}
