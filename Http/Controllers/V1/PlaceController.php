<?php

namespace OctoCmsModule\Core\Http\Controllers\V1;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Entities\City;
use OctoCmsModule\Core\Entities\Country;
use OctoCmsModule\Core\Http\Requests\SelectRequest;
use OctoCmsModule\Core\Services\SelectService;

/**
 * Class PlaceController
 *
 * @package OctoCmsModule\Core\Http\Controllers
 */
class PlaceController extends Controller
{
    /** @var SelectService */
    protected $selectService;

    /**
     * PlaceController constructor.
     *
     * @param SelectService $selectService
     */
    public function __construct(SelectService $selectService)
    {
        $this->selectService = $selectService;
    }

    /**
     * @param SelectRequest $request
     *
     * @return JsonResponse|object
     */
    public function countriesIndex(SelectRequest $request)
    {
        /** @var array $fields */
        $fields = $request->validated();

        /** @var string $query */
        $query = Arr::get($fields, 'query', '');

        /** @var string $lang */
        $columnName = Country::checkColumnName(Arr::get($fields, 'columnName', ''))
            ? Arr::get($fields, 'columnName', '')
            : 'name_en';

        /** @var Collection $countries */
        $countries = Country::where($columnName, 'like', "%$query%")
            ->orderBy($columnName)
            ->get();

        return response()
            ->json($this->selectService->parseCollection($countries, $columnName, "sigla_alpha_2"))
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @param SelectRequest $request
     *
     * @return JsonResponse|object
     */
    public function regionsIndex(SelectRequest $request)
    {
        return $this->extractData($request->validated(), "regione", "cod_regione");
    }

    /**
     * @param SelectRequest $request
     *
     * @return JsonResponse|object
     */
    public function provincesIndex(SelectRequest $request)
    {
        return $this->extractData($request->validated(), "sovracomunale", "sigla_automobilistica");
    }

    /**
     * @param SelectRequest $request
     *
     * @return JsonResponse|object
     */
    public function citiesIndex(SelectRequest $request)
    {
        return $this->extractData($request->validated(), "denominazione", "cod_comune_alfanumerico");
    }

    /**
     * @param array  $fields
     * @param string $name
     * @param string $key
     *
     * @return JsonResponse|object
     */
    protected function extractData(array $fields, string $name, string $key)
    {
        /** @var string $query */
        $query = Arr::get($fields, 'query', '');

        /** @var Collection $list */
        $collection = City::where($name, 'like', "%$query%")
            ->orderBy($name)
            ->groupBy($name)
            ->get();

        return response()
            ->json($this->selectService->parseCollection($collection, $name, $key))
            ->setStatusCode(Response::HTTP_OK);
    }
}
