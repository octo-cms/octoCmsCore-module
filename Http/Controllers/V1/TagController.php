<?php

namespace OctoCmsModule\Core\Http\Controllers\V1;

use OctoCmsModule\Core\Entities\Tag;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Http\Requests\GetTagsByGroupRequest;
use OctoCmsModule\Core\Http\Requests\StoreTagRequest;
use OctoCmsModule\Core\Interfaces\DatatableServiceInterface;
use OctoCmsModule\Core\Interfaces\TagServiceInterface;
use OctoCmsModule\Core\Transformers\TagCollectionResource;
use OctoCmsModule\Core\Transformers\TagResource;

/**
 * Class TagController
 *
 * @package OctoCmsModule\Core\Http\Controllers\V1
 */
class TagController extends Controller
{
    /**
     * @var TagServiceInterface
     */
    protected $tagService;

    /**
     * TagController constructor.
     *
     * @param TagServiceInterface $tagService
     */
    public function __construct(TagServiceInterface $tagService)
    {
        $this->tagService = $tagService;
    }

    /**
     * @param StoreTagRequest $request
     *
     * @return JsonResponse|object
     */
    public function store(StoreTagRequest $request)
    {
        /**
 * @var array $fields
*/
        $fields = $request->validated();

        /**
 * @var Tag $tag
*/
        $tag = $this->tagService->saveTag(
            new Tag(['name' => Arr::get($fields, 'name', '')]),
            Arr::get($fields, 'group', '')
        );

        return (new TagResource($tag))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * @param GetTagsByGroupRequest     $request
     * @param DatatableServiceInterface $datatableService
     *
     * @return JsonResponse
     */
    public function getByGroup(GetTagsByGroupRequest $request, DatatableServiceInterface $datatableService)
    {
        $fields = $request->validated();

        $tags = Tag::query()->orderBy('name');

        if (!empty($group = Arr::get($fields, 'group', null))) {
            $tags->whereHas(
                'group',
                function (Builder $builder) use ($group) {
                    $builder->where('name', '=', $group);
                }
            );
        }

        $datatableDTO = $datatableService->getDatatableDTO($request->validated(), $tags);

        $collection = $datatableDTO->builder->get();

        $datatableDTO->collection = TagCollectionResource::collection($collection);

        return response()->json($datatableDTO, Response::HTTP_OK);
    }
}
