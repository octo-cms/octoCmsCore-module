<?php

namespace OctoCmsModule\Core\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Http\Requests\DatatableRequest;
use OctoCmsModule\Core\Http\Requests\SaveRegistryRequest;
use OctoCmsModule\Core\Http\Requests\UpdateRegistryPersonalDataRequest;
use OctoCmsModule\Core\Interfaces\DatatableServiceInterface;
use OctoCmsModule\Core\Interfaces\RegistryServiceInterface;
use OctoCmsModule\Core\Transformers\RegistryResource;

/**
 * Class RegistryController
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Http\Controllers\V1
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class RegistryController extends Controller
{

    protected $registryService;

    /**
     * RegistryController constructor.
     *
     * @param RegistryServiceInterface $registryService RegistryServiceInterface
     */
    public function __construct(RegistryServiceInterface $registryService)
    {
        $this->registryService = $registryService;
    }

    /**
     * Name datatableIndex
     *
     * @param DatatableRequest          $request          DatatableRequest
     * @param DatatableServiceInterface $datatableService DatatableServiceInterface
     *
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function datatableIndex(DatatableRequest $request, DatatableServiceInterface $datatableService)
    {
        $this->authorize('datatableIndex', Registry::class);

        /**
         * Array
         *
         * @var array $fields
         */
        $fields=$request->validated();

        $query = Arr::get($fields, 'query', '');
        $registries = Registry::query();

        if (!empty($query)) {
            $registries = $registries->where('code', 'LIKE', '%' . $query . '%')
                ->orWhereHas(
                    'privateRegistry',
                    function (Builder $privateRegistryBuilder) use ($query) {
                        $privateRegistryBuilder->where('name', 'LIKE', '%' . $query . '%')
                            ->orWhere('surname', 'LIKE', '%' . $query . '%');
                    }
                )
                ->orWhereHas(
                    'companyRegistry',
                    function (Builder $companyRegistryBuilder) use ($query) {
                        $companyRegistryBuilder->where('businessname', 'LIKE', '%' . $query . '%');
                    }
                );
        }

        $datatableDTO = $datatableService->getDatatableDTO($fields, $registries);

        $collection = $datatableDTO->builder->get();

        $collection
            ->load('defaultEmail')
            ->load('defaultPhone')
            ->load('privateRegistry')
            ->load('companyRegistry');

        $datatableDTO->collection = RegistryResource::collection($collection);

        return response()->json($datatableDTO, Response::HTTP_OK);
    }

    /**
     * Name store
     *
     * @param SaveRegistryRequest $request SaveRegistryRequest
     *
     * @return JsonResponse|object
     * @throws AuthorizationException
     */
    public function store(SaveRegistryRequest $request)
    {
        $this->authorize('store', Registry::class);

        /**
         * Registry
         *
         * @var Registry $registry
         */
        $registry = $this->registryService->saveRegistry(new Registry(), $request->validated());

        return (new RegistryResource($this->registryFullLoad($registry)))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Name show
     *
     * @param mixed $id Registry Id
     *
     * @return JsonResponse|object
     * @throws AuthorizationException
     */
    public function show($id)
    {
        /**
         * Registry
         *
         * @var Registry $registry entity to be displayed
         */
        $registry = Registry::findOrFail($id);

        $this->authorize('show', $registry);

        return (new RegistryResource($this->registryFullLoad($registry)))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Name update
     *
     * @param SaveRegistryRequest $request Registry Request
     * @param int                 $id      Registry ID
     *
     * @return JsonResponse|object
     * @throws AuthorizationException
     */
    public function update(SaveRegistryRequest $request, int $id)
    {
        /**
         * Registry
         *
         * @var Registry $registry entity to be updated
         */
        $registry = Registry::findOrFail($id);

        $this->authorize('update', $registry);

        /**
         * Updated Registry
         *
         * @var Registry $updatedRegistry registry returned by RegistryService
         */
        $updatedRegistry = $this->registryService->saveRegistry($registry, $request->validated());

        return (new RegistryResource($this->registryFullLoad($updatedRegistry)))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Name registryFullLoad
     *
     * @param Registry $registry Registry
     *
     * @return Registry
     */
    protected function registryFullLoad(Registry $registry): Registry
    {
        $registry->load("privateRegistry");
        $registry->load("companyRegistry");
        $registry->load("defaultEmail");
        $registry->load("defaultPhone");
        $registry->load("addresses");
        $registry->load("emails");
        $registry->load("phones");
        $registry->load(
            'customFieldEntities',
            'customFieldEntities.valuable',
            'customFieldEntities.customField'
        );

        return $registry;
    }

    /**
     * Name updatePersonalData
     *
     * @param UpdateRegistryPersonalDataRequest $request
     * @param int                               $id
     *
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function updatePersonalData(UpdateRegistryPersonalDataRequest $request, int $id): JsonResponse
    {
        /**
         * Registry
         *
         * @var Registry $registry entity to be updated
         */
        $registry = Registry::findOrFail($id);

        $this->authorize('update', $registry);

        /**
         * Updated Registry
         *
         * @var Registry $updatedRegistry registry returned by RegistryService
         */
        $updatedRegistry = $this->registryService->updatePersonalData($registry, $request->validated());

        return (new RegistryResource($this->registryFullLoad($updatedRegistry)))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }
}
