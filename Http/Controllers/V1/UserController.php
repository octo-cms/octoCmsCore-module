<?php

declare(strict_types=1);

namespace OctoCmsModule\Core\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use OctoCmsModule\Core\Constants\CacheTagConst;
use OctoCmsModule\Core\DTO\DatatableDTO;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Entities\UserEventLog;
use OctoCmsModule\Core\Exceptions\OctoCmsException;
use OctoCmsModule\Core\Http\Requests\DatatableRequest;
use OctoCmsModule\Core\Http\Requests\LoginUserRequest;
use OctoCmsModule\Core\Http\Requests\RecoveryPasswordRequest;
use OctoCmsModule\Core\Http\Requests\StoreUserRequest;
use OctoCmsModule\Core\Http\Requests\UpdateUserPasswordRequest;
use OctoCmsModule\Core\Http\Requests\UpdateUserRequest;
use OctoCmsModule\Core\Interfaces\DatatableServiceInterface;
use OctoCmsModule\Core\Interfaces\PermissionServiceInterface;
use OctoCmsModule\Core\Interfaces\UserServiceInterface;
use OctoCmsModule\Core\Services\CacheService;
use OctoCmsModule\Core\Transformers\UserEventLogResource;
use OctoCmsModule\Core\Transformers\UserResource;

use function response;
use function strval;

/**
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Http\Controllers\V1
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 */
class UserController extends Controller
{

    protected HasherContract $hasher;
    protected UserServiceInterface $userService;
    protected PermissionServiceInterface $permissionService;

    /**
     * @param HasherContract             $hasher            HasherContract
     * @param UserServiceInterface       $userService       UserServiceInterface
     * @param PermissionServiceInterface $permissionService PermissionServiceInterface
     */
    public function __construct(
        HasherContract $hasher,
        UserServiceInterface $userService,
        PermissionServiceInterface $permissionService
    ) {
        $this->hasher = $hasher;
        $this->userService = $userService;
        $this->permissionService = $permissionService;
    }

    /**
     * Name login
     *
     * @param LoginUserRequest $request LoginUserRequest
     *
     * @throws OctoCmsException
     */
    public function login(LoginUserRequest $request): UserResource
    {
        /**
         * Fields
         *
         * @var array $fields
         */
        $fields = $request->validated();

        /**
         * User
         *
         * @var User $user
         */
        $user = User::where(
            'username',
            '=',
            Arr::get($fields, 'username', null)
        )->firstOrFail();

        if (
            !$this->hasher->check(
                Arr::get($fields, 'password', null),
                $user->getAuthPassword()
            )
        ) {
            throw new OctoCmsException('Credential Mismatch');
        }

        Auth::guard()->login($user);

        $this->userService->saveUserEventLog(
            $user,
            UserEventLog::EVENT_LOGIN,
            ['ipAddress' => $request->ip()]
        );

        CacheService::set(
            CacheTagConst::USER_ROLES,
            strval($user->id),
            $this->permissionService->getUserRoles($user)
        );

        return new UserResource($user);
    }

    /**
     * Name logout
     *
     * @param Request $request Request
     */
    public function logout(Request $request): JsonResponse
    {
        $user = Auth::user();

        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        $this->userService->saveUserEventLog(
            $user,
            UserEventLog::EVENT_LOGOUT,
            ['ipAddress' => $request->ip()]
        );

        return response()->json([], Response::HTTP_OK);
    }

    /**
     * Name store
     *
     * @param StoreUserRequest $request StoreUserRequest
     *
     * @return JsonResponse|object
     *
     * @throws AuthorizationException
     */
    public function store(StoreUserRequest $request)
    {
        $this->authorize('store', User::class);

        $user = $this->userService->saveUser(new User(), $request->validated());

        return (new UserResource($this->userFullLoad($user)))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Name update
     *
     * @param UpdateUserRequest $request UpdateUserRequest
     * @param mixed             $id      User id
     *
     * @return JsonResponse|object
     *
     * @throws AuthorizationException
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $user = User::findOrFail($id);

        $this->authorize('update', $user);

        $userUpdated = $this->userService->saveUser($user, $request->validated());

        return (new UserResource($this->userFullLoad($user)))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Name datatableIndex
     *
     * @param DatatableRequest          $request          DatatableRequest
     * @param DatatableServiceInterface $datatableService DatatableServiceInterface
     *
     * @throws AuthorizationException
     */
    public function datatableIndex(
        DatatableRequest $request,
        DatatableServiceInterface $datatableService
    ): JsonResponse {
        $this->authorize('datatableIndex', User::class);

        $users = User::query();

        /**
         * DatatableDTO
         *
         * @var DatatableDTO $datatableDTO
         */
        $datatableDTO = $datatableService->getDatatableDTO($request->validated(), $users);

        $collection = $datatableDTO->builder->get();

        $collection
            ->load('roles')
            ->load('defaultEmail');

        $datatableDTO->collection = UserResource::collection($collection);

        return response()->json($datatableDTO, Response::HTTP_OK);
    }

    /**
     * Name generateNewPassword
     *
     * @param mixed $id integer User Id
     *
     * @return JsonResponse|object
     *
     * @throws OctoCmsException
     * @throws AuthorizationException
     */
    public function generateNewPassword($id)
    {
        /**
         * User
         *
         * @var User $user
         */
        $user = User::findOrFail($id);

        $this->authorize('generateNewPassword', $user);

        if (empty($user->defaultEmail)) {
            throw new OctoCmsException('User is missing default Email.');
        }

        return (new UserResource($this->userService->generateNewPassword($user)))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Name currentUser
     *
     * @return JsonResponse|object
     */
    public function currentUser()
    {
        $user = Auth::user();

        return (new UserResource($this->userFullLoad($user)))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Name show
     *
     * @param mixed $id User Id
     *
     * @return JsonResponse|object
     *
     * @throws AuthorizationException
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        $this->authorize('show', $user);

        return (new UserResource($this->userFullLoad($user)))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Name updatePassword
     *
     * @param UpdateUserPasswordRequest $request UpdateUserPasswordRequest
     *
     * @return JsonResponse|object
     */
    public function updatePassword(UpdateUserPasswordRequest $request)
    {
        /**
         * User
         *
         * @var User $user
         */
        $user = Auth::user();

        // phpcs:disable
        $user->password = Arr::get($request->validated(), 'password');
        $user->temp_password = false;
        $user->save();
        // phpcs:enable

        return (new UserResource($user))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Name userFullLoad
     *
     * @param User|Collection $user User
     *
     * @return User|Collection
     */
    protected function userFullLoad($user)
    {
        $user->load('roles');
        $user->load('emails');
        $user->load('phones');
        $user->load('addresses');
        $user->load('defaultEmail');
        $user->load('defaultPhone');
        $user->load(
            'customFieldEntities',
            'customFieldEntities.valuable',
            'customFieldEntities.customField'
        );

        return $user;
    }

    /**
     * Name recoveryPassword
     *
     * @param RecoveryPasswordRequest $request RecoveryPasswordRequest
     *
     * @return JsonResponse|object
     *
     * @throws OctoCmsException
     */
    public function recoveryPassword(RecoveryPasswordRequest $request)
    {
        $fields = $request->validated();

        $username = Str::lower(Arr::get($fields, 'username', ''));
        $email = Str::lower(Arr::get($fields, 'email', ''));

        $user = User::where('username', '=', $username)->whereHas(
            'emails',
            static function (Builder $builder) use ($email): void {
                $builder->where('email', '=', $email);
            }
        )->first();

        if (empty($user)) {
            throw new OctoCmsException('User is missing');
        }

        return (new UserResource($this->userService->generateNewPassword($user)))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Name datatableIndex
     *
     * @param DatatableRequest          $request          DatatableRequest
     * @param DatatableServiceInterface $datatableService DatatableServiceInterface
     * @param int                       $id               User ID
     *
     * @throws AuthorizationException
     */
    public function userEventLogDatatableIndexAccesses(
        DatatableRequest $request,
        DatatableServiceInterface $datatableService,
        int $id
    ): JsonResponse {
        $this->authorize('userEventLogDatatableIndexAccesses', User::class);

        /**
         * User event log builder
         *
         * @var Builder $userEventLogs
         */
        $userEventLogs = UserEventLog::where('user_id', '=', $id);

        /**
         * DatatableDTO
         *
         * @var DatatableDTO $datatableDTO
         */
        $datatableDTO = $datatableService->getDatatableDTO($request->validated(), $userEventLogs);

        $datatableDTO->collection = UserEventLogResource::collection($datatableDTO->builder->get());

        return response()->json($datatableDTO, Response::HTTP_OK);
    }

    /**
     * Name indexByRole
     *
     * @param string $role user's role
     *
     * @throws AuthorizationException
     */
    public function indexByRole(string $role): JsonResponse
    {
        $this->authorize('indexByRole', User::class);

        /**
         * User collection with request role
         *
         * @var Collection $users User Collection
         */
        $users = User::whereHas(
            'roles',
            static function (Builder $roles) use ($role): void {
                $roles->where('name', '=', $role);
            }
        )->get();

        return response()->json(UserResource::collection($this->userFullLoad($users)), Response::HTTP_OK);
    }
}
