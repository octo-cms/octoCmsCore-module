<?php

namespace OctoCmsModule\Core\Http\Controllers\V1;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use OctoCmsModule\Core\Constants\CacheTagConst;
use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Core\Http\Requests\UpdateSettingByNameRequest;
use OctoCmsModule\Core\Http\Requests\UpdateSettingsRequest;
use OctoCmsModule\Core\Interfaces\SettingServiceInterface;
use OctoCmsModule\Core\Transformers\SettingResource;

/**
 * Class SettingController
 *
 * @package OctoCmsModule\Core\Http\Controllers
 */
class SettingController extends Controller
{
    /**
     * @return JsonResponse|object
     */
    public function index()
    {
        return response()->json($this->mapAllSettings())->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @param UpdateSettingByNameRequest $request
     *
     * @return JsonResponse|object
     */
    public function updateSettingByName(UpdateSettingByNameRequest $request)
    {
        $fields = $request->validated();

        /** @var Setting $setting */
        $setting = Setting::where('name', '=', Arr::get($fields, 'name', ''))
            ->firstOrFail();

        if (!empty($entity = Arr::get($fields, 'entity', ''))) {
            /** @var array $valueFromDb */
            $valueFromDb = $setting->value;
            $valueFromDb[$entity] = Arr::get($fields, 'value', []);
            $setting->value = $valueFromDb;
        } else {
            $setting->value = Arr::get($fields, 'value', '');
        }

        $setting->save();

        return (new SettingResource($setting))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * @return array
     */
    protected function mapAllSettings()
    {
        $settings = [];

        /** @var Setting $setting */
        foreach (Setting::all() as $setting) {
            $settings[$setting->name] = $setting->value;
        }

        return $settings;
    }

    /**
     * @return JsonResponse
     */
    public function flushSettingsCache()
    {
        Cache::tags([CacheTagConst::SETTING])->flush();

        return response()->json([], Response::HTTP_OK);
    }

    /**
     * @param UpdateSettingsRequest   $request
     * @param SettingServiceInterface $settingService
     *
     * @return JsonResponse|object
     */
    public function updateSettings(UpdateSettingsRequest $request, SettingServiceInterface $settingService)
    {
        $fields = $request->validated();

        $settingService->saveSettings($fields);

        return response()->json($this->mapAllSettings())->setStatusCode(Response::HTTP_OK);
    }
}
