<?php

namespace OctoCmsModule\Core\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use OctoCmsModule\Core\DTO\DatatableDTO;
use OctoCmsModule\Core\Http\Requests\DatatableRequest;
use OctoCmsModule\Core\Http\Requests\UpdateProviderRequest;
use OctoCmsModule\Core\Interfaces\DatatableServiceInterface;
use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Core\Http\Requests\StoreProviderRequest;
use OctoCmsModule\Core\Interfaces\ProviderServiceInterface;
use OctoCmsModule\Core\Transformers\ProviderResource;

/**
 * Class ProviderController
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Http\Controllers\V1
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class ProviderController extends Controller
{
    protected $providerService;

    /**
     * ServiceController constructor.
     *
     * @param ProviderServiceInterface $providerService ProviderServiceInterface
     */
    public function __construct(ProviderServiceInterface $providerService)
    {
        $this->providerService = $providerService;
    }

    /**
     * Name index
     *
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', Provider::class);
        return response()->json(ProviderResource::collection(Provider::all()), Response::HTTP_OK);
    }

    /**
     * Name show
     *
     * @param $id Provider
     *
     * @return JsonResponse|object
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show($id)
    {

        $provider = Provider::findOrFail($id);

        $this->authorize('show', $provider);

        return (new ProviderResource($this->providerFullLoad($provider)))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Name store
     *
     * @param StoreProviderRequest $request StoreProviderRequest
     *
     * @return JsonResponse|object
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(StoreProviderRequest $request)
    {
        $this->authorize('store', Provider::class);

        /**
         * Provider
         *
         * @var Provider
         */
        $provider = $this->providerService->saveProvider(new Provider(), $request->validated());

        return (new ProviderResource($provider))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Name update
     *
     * @param UpdateProviderRequest $request UpdateProviderRequest
     * @param $id      integer
     *
     * @return JsonResponse|object
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(UpdateProviderRequest $request, $id)
    {
        /**
         * Provider
         *
         * @var Provider $provider
         */
        $provider = Provider::findOrFail($id);

        $this->authorize('update', $provider);

        /**
         * Updated Provider
         *
         * @var Provider $updatedProvider
         */
        $updatedProvider = $this->providerService
            ->saveProvider($provider, $request->validated());

        return (new ProviderResource($updatedProvider))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Name datatableIndex
     *
     * @param DatatableRequest          $request          DatatableRequest
     * @param DatatableServiceInterface $datatableService DatatableServiceInterface
     *
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function datatableIndex(DatatableRequest $request, DatatableServiceInterface $datatableService)
    {
        $this->authorize('datatableIndex', Provider::class);

        /**
         * Builder
         *
         * @var Builder $providers
         */
        $providers = Provider::query();

        /**
         * DatatableDTO
         *
         * @var DatatableDTO $datatableDTO
         */
        $datatableDTO = $datatableService->getDatatableDTO($request->validated(), $providers);

        $datatableDTO->collection = ProviderResource::collection($datatableDTO->builder->get());

        return response()->json($datatableDTO, Response::HTTP_OK);
    }

    /**
     * Name providerFullLoad
     *
     * @param Provider $provider Provider
     *
     * @return Provider
     */
    protected function providerFullLoad(Provider $provider)
    {
        return $provider;
    }
}
