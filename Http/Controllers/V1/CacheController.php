<?php

namespace OctoCmsModule\Core\Http\Controllers\V1;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Http\Requests\FlushCacheRequest;
use OctoCmsModule\Core\Services\CacheService;

/**
 * Class CacheController
 *
 * @package OctoCmsModule\Core\Http\Controllers\V1
 */
class CacheController extends Controller
{
    /**
     * @param FlushCacheRequest $request
     *
     * @return JsonResponse
     */
    public function flushCache(FlushCacheRequest $request)
    {
        CacheService::flushCacheByTag(Arr::get($request->validated(), 'tag', ''));

        return response()->json([], Response::HTTP_OK);
    }
}
