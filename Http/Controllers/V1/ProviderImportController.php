<?php

namespace OctoCmsModule\Core\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use OctoCmsModule\Core\DTO\DatatableDTO;
use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Entities\ProviderImportData;
use OctoCmsModule\Core\Http\Requests\DatatableRequest;
use OctoCmsModule\Core\Http\Requests\UploadProviderImportRequest;
use OctoCmsModule\Core\Interfaces\DatatableServiceInterface;
use OctoCmsModule\Core\Interfaces\ProviderServiceInterface;
use OctoCmsModule\Core\Jobs\SyncProviderImportWithRegistryJob;
use OctoCmsModule\Core\Transformers\ProviderImportDataResource;
use OctoCmsModule\Core\Transformers\ProviderImportResource;
use OctoCmsModule\Core\Transformers\ProviderResource;

/**
 * Class ProviderImportController
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Http\Controllers\V1
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class ProviderImportController extends Controller
{
    protected $providerService;

    /**
     * ServiceController constructor.
     *
     * @param ProviderServiceInterface $providerService ProviderServiceInterface
     */
    public function __construct(ProviderServiceInterface $providerService)
    {
        $this->providerService = $providerService;
    }

    /**
     * Name upload
     *
     * @param UploadProviderImportRequest $request UploadProviderImportRequest
     *
     * @return JsonResponse|object
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function upload(UploadProviderImportRequest $request)
    {
        $this->authorize('upload', ProviderImport::class);

        /**
         * UploadedFile
         *
         * @var UploadedFile $file
         */
        $file = $request->file('file');

        /**
         * ProviderImport
         *
         * @var ProviderImport $providerImport
         */
        $providerImport = $this->providerService->uploadImport($file);

        return (new ProviderImportResource($providerImport))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Name show
     *
     * @param $id id del provider
     *
     * @return JsonResponse|object
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show($id)
    {
        $providerImport = ProviderImport::findOrFail($id);

        $this->authorize('show', $providerImport);

        return (new ProviderImportResource($providerImport))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Name datatableProviderImportData
     *
     * @param DatatableRequest          $request          DatatableRequest
     * @param DatatableServiceInterface $datatableService DatatableServiceInterface
     * @param $id               int
     *
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function datatableProviderImportData(
        DatatableRequest $request,
        DatatableServiceInterface $datatableService,
        $id
    ) {
        $this->authorize('datatableProviderImportData', ProviderImport::class);

        $users = ProviderImportData::where('provider_import_id', '=', $id);

        /**
         * DatatableDTO
         *
         * @var DatatableDTO $datatableDTO
         */
        $datatableDTO = $datatableService->getDatatableDTO($request->validated(), $users);

        $collection = $datatableDTO->builder->get();

        $collection
            ->load('provider')
            ->load('registry');

        $datatableDTO->collection = ProviderImportDataResource::collection($collection);

        return response()->json($datatableDTO, Response::HTTP_OK);
    }

    /**
     * Name syncWithRegistry
     *
     * @param mixed $id Provider Import Id
     *
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function syncWithRegistry($id)
    {
        /**
         * Provider import
         *
         * @var ProviderImport $providerImport
         */
        $providerImport = ProviderImport::findOrFail($id);

        $this->authorize('syncWithRegistry', $providerImport);

        SyncProviderImportWithRegistryJob::dispatch($providerImport);

        return response()->json([], Response::HTTP_OK);
    }

    /**
     * Name datatableIndex
     *
     * @param DatatableRequest          $request          DatatableRequest
     * @param DatatableServiceInterface $datatableService DatatableServiceInterface
     *
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function datatableIndex(DatatableRequest $request, DatatableServiceInterface $datatableService)
    {
        $this->authorize('datatableIndex', ProviderImport::class);

        /**
         * Builder
         *
         * @var Builder $providers
         */
        $providers = ProviderImport::query();

        /**
         * DatatableDTO
         *
         * @var DatatableDTO $datatableDTO
         */
        $datatableDTO = $datatableService->getDatatableDTO($request->validated(), $providers);

        $datatableDTO->collection = ProviderImportResource::collection($datatableDTO->builder->get());

        return response()->json($datatableDTO, Response::HTTP_OK);
    }

    /**
     * Name destroy
     *
     * @param $id id
     *
     * @return JsonResponse|object
     * @throws \Exception
     */
    public function destroy($id)
    {
        /**
         * Provider import
         *
         * @var ProviderImport $providerImport
         */
        $providerImport = ProviderImport::findOrFail($id);

        $this->authorize('destroy', $providerImport);


        $providerImport->delete();

        return (new ProviderImportResource($providerImport))
            ->response()
            ->setStatusCode(Response::HTTP_OK);
    }
}
