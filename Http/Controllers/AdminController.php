<?php

namespace OctoCmsModule\Core\Http\Controllers;

use App\Http\Controllers\Controller;

/**
 * Class LeadController
 *
 * @package OctoCmsModule\Core\Http\Controllers
 */
class AdminController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('core::admin.index');
    }
}
