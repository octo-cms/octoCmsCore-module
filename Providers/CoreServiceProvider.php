<?php

namespace OctoCmsModule\Core\Providers;

use Config;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use OctoCmsModule\Core\Console\GeneratePhpUnitXMLCommand;
use OctoCmsModule\Core\Console\RemoveExcelTempFolderCommand;
use OctoCmsModule\Core\Entities\CustomFieldEntityDate;
use OctoCmsModule\Core\Entities\CustomFieldEntityDecimal;
use OctoCmsModule\Core\Entities\CustomFieldEntityNumeric;
use OctoCmsModule\Core\Entities\CustomFieldEntityString;
use OctoCmsModule\Core\Entities\CustomFieldEntityText;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Interfaces\ActiveCampaignServiceInterface;
use OctoCmsModule\Core\Interfaces\EntityIdsServiceInterface;
use OctoCmsModule\Core\Interfaces\PermissionServiceInterface;
use OctoCmsModule\Core\Interfaces\ProviderServiceInterface;
use OctoCmsModule\Core\Interfaces\RegistryServiceInterface;
use OctoCmsModule\Core\Interfaces\UserServiceInterface;
use OctoCmsModule\Core\Services\EntityIdsService;
use OctoCmsModule\Core\Console\CreateUserCommand;
use OctoCmsModule\Core\Console\InstallCoreCommand;
use OctoCmsModule\Core\Console\SetPlacesCommand;
use OctoCmsModule\Core\Interfaces\PictureServiceInterface;
use OctoCmsModule\Core\Interfaces\TagServiceInterface;
use OctoCmsModule\Core\Interfaces\SettingServiceInterface;
use OctoCmsModule\Sitebuilder\Interfaces\PageRedirectServiceInterface;

/**
 * Class CoreServiceProvider
 *
 * @package OctoCmsModule\Core\Providers
 * @author  danielepasi
 */
class CoreServiceProvider extends ServiceProvider
{
    /**
     * @var string $moduleName
     */
    protected $moduleName = 'Core';

    /**
     * @var string $moduleNameLower
     */
    protected $moduleNameLower = 'core';

    public function boot()
    {
        $this->registerTranslations();
        $this->registerConfig();
        $this->registerViews();
        $this->registerFactories();
        $this->loadMigrationsFrom(module_path($this->moduleName, 'Database/Migrations'));

        $this->commands(
            [
                SetPlacesCommand::class,
                InstallCoreCommand::class,
                CreateUserCommand::class,
                GeneratePhpUnitXMLCommand::class,
                RemoveExcelTempFolderCommand::class,
            ]
        );

//        $router->aliasMiddleware('auth-admin', Authenticate::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);

        $this->app->bind(EntityIdsServiceInterface::class, EntityIdsService::class);

        $this->app->bind(
            PermissionServiceInterface::class,
            getBindingImplementation($this->moduleName, "Services\PermissionService")
        );


        if ($this->app->environment() == 'testing') {
            // Services
            $this->app->bind(
                PictureServiceInterface::class,
                getBindingStub($this->moduleName, "PictureServiceMock")
            );
            $this->app->bind(
                PageRedirectServiceInterface::class,
                getBindingStub($this->moduleName, "PageRedirectServiceMock")
            );
            $this->app->bind(
                TagServiceInterface::class,
                getBindingStub($this->moduleName, "TagServiceMock")
            );
            $this->app->bind(
                SettingServiceInterface::class,
                getBindingStub($this->moduleName, "SettingServiceMock")
            );
            $this->app->bind(
                TagServiceInterface::class,
                getBindingStub($this->moduleName, "TagServiceMock")
            );
            $this->app->bind(
                ActiveCampaignServiceInterface::class,
                getBindingStub($this->moduleName, "ActiveCampaignServiceMock")
            );
            $this->app->bind(
                ProviderServiceInterface::class,
                getBindingStub($this->moduleName, "ProviderServiceMock")
            );
            $this->app->bind(
                UserServiceInterface::class,
                getBindingStub($this->moduleName, "UserServiceMock")
            );
            $this->app->bind(
                RegistryServiceInterface::class,
                getBindingStub($this->moduleName, "RegistryServiceMock")
            );
        } else {
            // Services
            $this->app->bind(
                SettingServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\SettingService")
            );
            $this->app->bind(
                TagServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\TagService")
            );
            $this->app->bind(
                PictureServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\PictureService")
            );
            $this->app->bind(
                PageRedirectServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\PageRedirectService")
            );
            $this->app->bind(
                TagServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\TagService")
            );
            $this->app->bind(
                ActiveCampaignServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\ActiveCampaignService")
            );
            $this->app->bind(
                ProviderServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\ProviderService")
            );
            $this->app->bind(
                UserServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\UserService")
            );
            $this->app->bind(
                RegistryServiceInterface::class,
                getBindingImplementation($this->moduleName, "Services\RegistryService")
            );


        }

        Relation::morphMap(
            [
                'Registry'                 => Registry::class,
                'CustomFieldEntityString'  => CustomFieldEntityString::class,
                'CustomFieldEntityText'    => CustomFieldEntityText::class,
                'CustomFieldEntityDecimal' => CustomFieldEntityDecimal::class,
                'CustomFieldEntityNumeric' => CustomFieldEntityNumeric::class,
                'CustomFieldEntityDate'    => CustomFieldEntityDate::class,
                'User'                     => User::class,
            ]
        );
    }

    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig()
    {
        $this->publishes(
            [
                module_path($this->moduleName, 'Config/config.php') => config_path($this->moduleNameLower . '.php'),
            ], 'config'
        );
        $this->mergeConfigFrom(
            module_path($this->moduleName, 'Config/config.php'),
            $this->moduleNameLower
        );
    }

    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews()
    {
        $viewPath = resource_path('views/modules/' . $this->moduleNameLower);

        $sourcePath = module_path($this->moduleName, 'Resources/views');

        $this->publishes(
            [
                $sourcePath => $viewPath,
            ], ['views', $this->moduleNameLower . '-module-views']
        );

        $this->loadViewsFrom(array_merge($this->getPublishableViewPaths(), [$sourcePath]), $this->moduleNameLower);
    }

    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations()
    {
        $langPath = resource_path('lang/modules/' . $this->moduleNameLower);

        if (is_dir($langPath)) {
            $this->loadTranslationsFrom($langPath, $this->moduleNameLower);
        } else {
            $this->loadTranslationsFrom(module_path($this->moduleName, 'Resources/lang'), $this->moduleNameLower);
        }
    }

    /**
     * Register an additional directory of factories.
     *
     * @return void
     */
    public function registerFactories()
    {
        if (!app()->environment('production') && $this->app->runningInConsole()) {
            app(Factory::class)->load(module_path($this->moduleName, 'Database/factories'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }

    private function getPublishableViewPaths(): array
    {
        $paths = [];
        foreach (Config::get('view.paths') as $path) {
            if (is_dir($path . '/modules/' . $this->moduleNameLower)) {
                $paths[] = $path . '/modules/' . $this->moduleNameLower;
            }
        }
        return $paths;
    }
}
