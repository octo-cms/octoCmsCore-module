<?php

namespace OctoCmsModule\Core\Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Entities\Email;
use OctoCmsModule\Core\Entities\Role;
use OctoCmsModule\Core\Entities\User;
use ReflectionClass;
use Tests\CreatesApplication;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

/**
 * Class TestCase
 * @package OctoCmsModule\Core\Tests
 */
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    use RefreshDatabase;

    /**
     * @param array $attributes
     *
     * @return User
     */
    protected static function createAdminUser(array $attributes = []): User
    {
        return self::createGenericUser($attributes, [Role::ROLE_ADMIN]);
    }

    /**
     * Name createUser
     * @param array $attributes
     * @param array $roles
     *
     * @return User
     */
    protected static function createGenericUser(array $attributes = [], array $roles = []): User
    {
        /** @var User $user */
        $user = User::factory()
            ->has(Email::factory())
            ->create($attributes);

        foreach ($roles as $item) {
            $user->roles()->attach(Role::firstOrCreate([
                'name' => $item,
            ]));
        }


        return $user;
    }

    /**
     * Name invokeMethod
     * @param       $object
     * @param       $methodName
     * @param array $parameters
     *
     * @return mixed
     * @throws \ReflectionException
     */
    public function invokeMethod(&$object, $methodName, array $parameters = [])
    {
        $reflection = new ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }

    /**
     * Name datatableTest
     * @param string $routeName
     * @param array  $params
     *
     * @return void
     */
    protected function datatableTest(string $routeName, array $params = [])
    {
        $response = $this->json(
            'POST',
            empty($params) ? route($routeName) : route($routeName, $params),
            [
                'currentPage' => 1,
                'rowsInPage'  => 10,
            ]
        );

        $content = json_decode($response->getContent(), true);

        $response->assertStatus(Response::HTTP_OK);

        $this->assertEquals(15, Arr::get($content, 'total', 0));

        $this->assertEquals(1, Arr::get($content, 'currentPage', 0));

        $this->assertEquals(10, Arr::get($content, 'rowsInPage', 0));

        $this->assertNotEmpty(Arr::get($content, 'collection', []));
    }

}
