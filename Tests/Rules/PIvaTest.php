<?php

namespace OctoCmsModule\Core\Tests\Rules;

use OctoCmsModule\Core\Rules\PIva;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class PIvaTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Rules
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class PIvaTest extends TestCase
{

    /**
     * Name test_PivaRuleValid
     *
     * @return void
     */
    public function test_PivaRuleValid()
    {
        $rule=new PIva();
        $this->assertTrue($rule->passes("piva","08563860157"));
        $this->assertTrue($rule->passes("piva",""));
    }

    public function test_PivaRuleInvalid()
    {
        $rule=new PIva();
        $this->assertFalse($rule->passes("piva","08563860151"));
        $this->assertFalse($rule->passes("piva","1221"));
        $this->assertFalse($rule->passes("piva","AQWSDERFTGY"));
    }

}
