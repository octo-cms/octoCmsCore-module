<?php

namespace OctoCmsModule\Core\Tests\Rules;

use OctoCmsModule\Core\Rules\CodFis;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class CodFisTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Rules
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class CodFisTest extends TestCase
{

    public function test_CodFisRuleValid()
    {
        $rule=new CodFis();
        $this->assertTrue($rule->passes("codfis","LRCRRT71E14F205A"));
        $this->assertTrue($rule->passes("codfis","RSSMRC20A41I625E"));
        $this->assertTrue($rule->passes("codfis","RSSMRC20D41F839U"));
        $this->assertTrue($rule->passes("codfis","08382510967"));
    }

    public function test_CodFisRuleInvalid()
    {
        $rule=new CodFis();
        $this->assertFalse($rule->passes("codfis","LRCRRT71E14F205B"));
        $this->assertFalse($rule->passes("codfis","1221"));
        $this->assertFalse($rule->passes("codfis","312312ljljll123j"));
        $this->assertFalse($rule->passes("codfis","08382510968"));
        $this->assertFalse($rule->passes("codfis","12345678901234561"));
    }

}
