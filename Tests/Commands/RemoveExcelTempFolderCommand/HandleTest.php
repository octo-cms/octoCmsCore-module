<?php

namespace OctoCmsModule\Core\Tests\Commands\RemoveExcelTempFolderCommand;

use Illuminate\Support\Facades\Storage;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class HandleTest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Core\Tests\Commands\RemoveExcelTempFolderCommand
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class HandleTest extends TestCase
{
    public function test_command()
    {
        Storage::fake();

        $this->artisan('remove-excel-temp-folder')
            ->expectsOutput('Deleting storage/framework/laravel-excel directory...');
    }
}
