<?php

namespace OctoCmsModule\Core\Tests\Commands\CreateUserCommand;

use Illuminate\Database\Eloquent\Builder;
use  OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class CreateUserCommandTest
 *
 * @package OctoCmsModule\Core\Tests\Commands\CreateUserCommand
 */
class CreateUserCommandTest extends TestCase
{



    public function test_createUserCommandWithRole()
    {
        $this->artisan('createUser usernameWithRole password admin')
            ->expectsOutput('User Created');

        $this->assertDatabaseHas('roles', [
            'name' => 'admin'
        ]);

        $this->assertDatabaseHas('users', [
            'username' => 'usernameWithRole'
        ]);

        $this->assertDatabaseHas('user_role', [
            'user_id' => 1,
            'role_id' => 1
        ]);


    }
}
