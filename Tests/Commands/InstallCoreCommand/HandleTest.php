<?php

namespace OctoCmsModule\Core\Tests\Commands\InstallCoreCommand;

use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class HandleTest
 *
 * @package OctoCmsModule\Core\Tests\Commands\InstallCoreCommand
 */
class HandleTest extends TestCase
{


    public function test_command()
    {

        $this->artisan('install:core')
            ->expectsOutput('Running Install Core Command ...');

        $this->assertDatabaseHas('settings', [
            'name'  => 'admin_mail',
            'value' => serialize(''),
        ]);

        $this->assertDatabaseHas('settings', [
            'name'  => 'active_campaign',
            'value' => serialize([
                'enabled' => false,
                'api_key' => null,
                'url'     => null,
                'list_id' => null,
            ]),
        ]);

    }
}
