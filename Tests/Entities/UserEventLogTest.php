<?php

namespace OctoCmsModule\Core\Tests\Entities;

use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Entities\UserEventLog;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class UserEventLogTest
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Entities
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class UserEventLogTest extends TestCase
{
    public function test_UserEventLogBelongToUsers()
    {
        /** @var UserEventLog $userEventLog */
        $userEventLog = UserEventLog::factory()->create();

        $userEventLog->load('user');

        $this->assertInstanceOf(User::class, $userEventLog->user);
    }
}
