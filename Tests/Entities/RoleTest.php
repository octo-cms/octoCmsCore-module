<?php

namespace OctoCmsModule\Core\Tests\Entities;

use OctoCmsModule\Core\Entities\Role;
use OctoCmsModule\Core\Entities\User;
use Illuminate\Support\Collection;
use OctoCmsModule\Core\Tests\TestCase;

class RoleTest extends TestCase
{


    public function test_RoleHasManyUsers()
    {
        /** @var Role $role */
        $role = Role::factory()->create();

        User::factory()
            ->count(3)
            ->create()
            ->each(function (User $user) use ($role) {
                $user->roles()->attach($role);
            });

        $role->load('users');

        $this->assertInstanceOf(Collection::class, $role->users);
        $this->assertInstanceOf(User::class, $role->users->first());
    }
}
