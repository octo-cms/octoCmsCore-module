<?php

namespace OctoCmsModule\Core\Tests\Entities;

use OctoCmsModule\Core\Entities\Video;
use OctoCmsModule\Core\Entities\VideoLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class VideoLangTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class VideoLangTest extends TestCase
{


    public function test_VideoLangBelongsToVideo()
    {
        /** @var VideoLang $videoLang */
        $videoLang = VideoLang::factory()->create();

        $videoLang->load('video');

        $this->assertInstanceOf(Video::class, $videoLang->video);
    }
}
