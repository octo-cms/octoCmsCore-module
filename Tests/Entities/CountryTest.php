<?php

namespace OctoCmsModule\Core\Tests\Entities;

use OctoCmsModule\Core\Entities\Country;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class CountryTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class CountryTest extends TestCase
{


    public function test_checkColumnName()
    {
        $this->assertTrue(Country::checkColumnName('name_it'));

        $this->assertFalse(Country::checkColumnName('name_cn'));

        $this->assertFalse(Country::checkColumnName(''));

        $this->assertFalse(Country::checkColumnName(null));
    }
}
