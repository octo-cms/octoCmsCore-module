<?php

namespace OctoCmsModule\Core\Tests\Entities;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Core\Entities\Address;
use OctoCmsModule\Core\Entities\CompanyRegistry;
use OctoCmsModule\Core\Entities\CustomFieldEntity;
use OctoCmsModule\Core\Entities\Email;
use OctoCmsModule\Core\Entities\PrivateRegistry;
use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Entities\ProviderImportData;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class RegistryTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class RegistryTest extends TestCase
{


    public function test_RegistryHasOneCompanyRegistry()
    {
        /** @var Registry $registry */
        $registry = Registry::factory()->has(CompanyRegistry::factory())->create();

        $registry->load('companyRegistry');

        $this->assertInstanceOf(CompanyRegistry::class, $registry->companyRegistry);
    }

    public function test_RegistryHasOnePrivateRegistry()
    {
        /** @var Registry $registry */
        $registry = Registry::factory()->has(PrivateRegistry::factory())->create();

        $registry->load('privateRegistry');

        $this->assertInstanceOf(PrivateRegistry::class, $registry->privateRegistry);
    }

    public function test_RegistryHasManyCustomFieldEntities()
    {
        /** @var Registry $registry */
        $registry = Registry::factory()->has(CustomFieldEntity::factory()->count(2))->create();

        $registry->load('customFieldEntities');

        $this->assertInstanceOf(Collection::class, $registry->customFieldEntities);
        $this->assertInstanceOf(CustomFieldEntity::class, $registry->customFieldEntities->first());
    }


    public function test_RegistryHasManyEmails()
    {
        /** @var Registry $registry */
        $registry = Registry::factory()->has(Email::factory()->count(2))->create();

        $registry->load('emails');

        $this->assertInstanceOf(Collection::class, $registry->emails);
        $this->assertInstanceOf(Email::class, $registry->emails->first());
    }


    public function test_RegistryHasOneDefaultEmail()
    {
        /**
         * Registry
         *
         * @var Registry $registry
         */
        $registry = Registry::factory()
            ->has(Email::factory()->state(['default' => true]))
            ->has(Email::factory()->count(2)->state(['default' => false]))
            ->create();

        /**
         * Email
         *
         * @var Email $email
         */
        $email = $registry->defaultEmail()->first();

        $this->assertInstanceOf(Email::class, $email);

        $this->assertEquals(true, $email->default);
    }

    public function test_RegistryHasManyAddresses()
    {
        /**
         * Registry
         *
         * @var Registry $registry
         */
        $registry = Registry::factory()
            ->has(Address::factory()->count(2))
            ->create();

        $registry->load('addresses');

        $this->assertInstanceOf(Collection::class, $registry->addresses);
        $this->assertInstanceOf(Address::class, $registry->addresses->first());
    }

    public function test_RegistryHasManyProviederImportData()
    {
        /** @var Registry $registry */
        $registry = Registry::factory()
            ->has(ProviderImportData::factory()->count(2))
            ->create();

        $registry->load('providerImportData');

        $this->assertInstanceOf(Collection::class, $registry->providerImportData);
        $this->assertInstanceOf(ProviderImportData::class, $registry->providerImportData->first());
    }
}
