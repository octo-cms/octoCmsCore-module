<?php

namespace OctoCmsModule\Core\Tests\Entities;

use OctoCmsModule\Core\Entities\Address;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class AddressTest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Core\Tests\Entities
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class AddressTest extends TestCase
{
    public function test_AddressableMorphToRegistry()
    {
        /** @var Address $address */
        $address = Address::factory()->for(Registry::factory(), 'addressable')->create();

        $address->load('addressable');

        $this->assertInstanceOf(Registry::class, $address->addressable);
        $this->assertEquals(1, $address->addressable->id);
    }
}
