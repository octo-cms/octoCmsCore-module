<?php

namespace OctoCmsModule\Core\Tests\Entities;

use Illuminate\Support\Collection;
use OctoCmsModule\Core\Entities\Media;
use OctoCmsModule\Core\Entities\MediaLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class MediaTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class MediaTest extends TestCase
{


    public function test_MediaHasManyMediaLang()
    {
        /** @var Media $media */
        $media = Media::factory()->has(MediaLang::factory()->count(2))->create();

        $media->load('mediaLangs');

        $this->assertInstanceOf(Collection::class, $media->mediaLangs);
        $this->assertInstanceOf(MediaLang::class, $media->mediaLangs->first());
    }
}
