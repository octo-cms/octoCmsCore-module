<?php

namespace OctoCmsModule\Core\Tests\Entities;

use OctoCmsModule\Core\Entities\CustomFieldEntity;
use OctoCmsModule\Core\Entities\CustomFieldEntityNumeric;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class CustomFieldEntityNumericTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Entities
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class CustomFieldEntityNumericTest extends TestCase
{
    public function test_CustomFieldEntityHasOneCustomFieldEntityNumeric()
    {
        /** @var CustomFieldEntity $customFieldRegistry */
        $customFieldRegistry = CustomFieldEntity::factory()->create();

        $customFieldRegistry->valuable()
            ->associate(CustomFieldEntityNumeric::factory()->create(['value' => 1]));

        $customFieldRegistry->load('valuable');

        $this->assertEquals(
            'CustomFieldEntityNumeric',
            $customFieldRegistry->valuable_type
        );

        $this->assertEquals(
            1,
            $customFieldRegistry->valuable->value
        );

        $this->assertInstanceOf(CustomFieldEntityNumeric::class, $customFieldRegistry->valuable);
    }

    public function dataProvider()
    {
        $providers = [];

        $providers[] = [true];
        $providers[] = [null];
        $providers[] = ['string'];
        $providers[] = [1.0];
        $providers[] = [[]];

        return $providers;
    }

    /**
     * @param $value
     *
     * @dataProvider dataProvider
     */
    public function test_setValueAttributeNull($value)
    {
        /**
         * CustomFieldEntityNumeric
         *
         * @var CustomFieldEntityNumeric $customFieldEntityNumeric
         */
        $customFieldEntityNumeric = CustomFieldEntityNumeric::factory()->create();

        $customFieldEntityNumeric->value = $value;

        $this->assertNull($customFieldEntityNumeric->value);
    }

    public function test_setValueAttributeOK()
    {
        /**
         * CustomFieldEntityNumeric
         *
         * @var CustomFieldEntityNumeric $customFieldEntityNumeric
         */
        $customFieldEntityNumeric = CustomFieldEntityNumeric::factory()->create();

        $customFieldEntityNumeric->value = 1;

        $this->assertEquals(1, $customFieldEntityNumeric->value);
    }
}
