<?php

namespace OctoCmsModule\Core\Tests\Entities;

use Illuminate\Support\Collection;
use OctoCmsModule\Core\Entities\Video;
use OctoCmsModule\Core\Entities\VideoLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class VideoTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class VideoTest extends TestCase
{


    public function test_VideoHasManyVideoLangs()
    {
        /** @var Video $video */
        $video = Video::factory()->has(VideoLang::factory()->count(2))->create();

        $video->load('videoLangs');

        $this->assertInstanceOf(Collection::class, $video->videoLangs);
        $this->assertInstanceOf(VideoLang::class, $video->videoLangs->first());
    }

    public function test_getAndSetDataAttribute()
    {
        /** @var Video $video */
        $video = Video::factory()->create();

        /** @var array $dataArray */
        $dataArray = ['data'];

        $video->data = $dataArray;

        $video->save();

        $this->assertDatabaseHas('videos', [
            'data' => serialize($dataArray),
        ]);

        $this->assertEquals(
            $video->data,
            $dataArray
        );
    }
}
