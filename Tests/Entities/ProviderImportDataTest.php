<?php

namespace OctoCmsModule\Core\Tests\Entities;

use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Entities\ProviderImportData;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ProviderImportDataTest
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Entities
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class ProviderImportDataTest extends TestCase
{


    public function test_ProviderImportDatabelongTo()
    {
        /** @var ProviderImportData $providerImportData */
        $providerImportData = ProviderImportData::factory()
            ->create();

        $providerImportData->load('provider')
            ->load('registry')
            ->load('providerImport');

        $this->assertInstanceOf(ProviderImport::class, $providerImportData->providerImport);
        $this->assertInstanceOf(Provider::class, $providerImportData->provider);
        $this->assertInstanceOf(Registry::class, $providerImportData->registry);
    }

}
