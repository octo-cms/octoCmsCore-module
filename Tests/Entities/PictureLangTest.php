<?php

namespace OctoCmsModule\Core\Tests\Entities;

use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Core\Entities\PictureLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class PictureLangTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class PictureLangTest extends TestCase
{


    public function test_PictureLangBelongsToPicture()
    {
        /** @var PictureLang $pictureLang */
        $pictureLang = PictureLang::factory()->create();

        $pictureLang->load('picture');

        $this->assertInstanceOf(Picture::class, $pictureLang->picture);
    }
}
