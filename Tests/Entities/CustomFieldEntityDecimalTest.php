<?php

namespace OctoCmsModule\Core\Tests\Entities;

use OctoCmsModule\Core\Entities\CustomFieldEntity;
use OctoCmsModule\Core\Entities\CustomFieldEntityDecimal;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class CustomFieldEntityDecimalTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Entities
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class CustomFieldEntityDecimalTest extends TestCase
{
    public function test_CustomFieldEntityHasOneCustomFieldEntityDecimal()
    {
        /** @var CustomFieldEntity $customFieldRegistry */
        $customFieldRegistry = CustomFieldEntity::factory()->create();

        $customFieldRegistry->valuable()
            ->associate(CustomFieldEntityDecimal::factory()->create(['value' => 20.77]));

        $customFieldRegistry->load('valuable');

        $this->assertEquals(
            'CustomFieldEntityDecimal',
            $customFieldRegistry->valuable_type
        );

        $this->assertEquals(
            20.77,
            $customFieldRegistry->valuable->value
        );

        $this->assertInstanceOf(CustomFieldEntityDecimal::class, $customFieldRegistry->valuable);
    }

    public function dataProvider()
    {
        $providers = [];

        $providers[] = [true];
        $providers[] = [null];
        $providers[] = [1];
        $providers[] = ['string'];
        $providers[] = [[]];

        return $providers;
    }

    /**
     * @param $value
     *
     * @dataProvider dataProvider
     */
    public function test_setValueAttributeNull($value)
    {
        /**
         * CustomFieldEntityDecimal
         *
         * @var CustomFieldEntityDecimal $customFieldEntityDecimal
         */
        $customFieldEntityDecimal = CustomFieldEntityDecimal::factory()->create();

        $customFieldEntityDecimal->value = $value;

        $this->assertNull($customFieldEntityDecimal->value);
    }

    public function test_setValueAttributeOK()
    {
        /**
         * CustomFieldEntityDecimal
         *
         * @var CustomFieldEntityDecimal $customFieldEntityDecimal
         */
        $customFieldEntityDecimal = CustomFieldEntityDecimal::factory()->create();

        $customFieldEntityDecimal->value = 3.14;

        $this->assertEquals(3.14, $customFieldEntityDecimal->value);
    }
}
