<?php

namespace OctoCmsModule\Core\Tests\Entities;

use OctoCmsModule\Core\Entities\PrivateRegistry;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class PrivateRegistryTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class PrivateRegistryTest extends TestCase
{


    public function test_PrivateRegistryBelongsToRegistry()
    {
        /** @var PrivateRegistry $privateRegistry */
        $privateRegistry = PrivateRegistry::factory()->create();

        $privateRegistry->load('registry');

        $this->assertInstanceOf(Registry::class, $privateRegistry->registry);
    }
}
