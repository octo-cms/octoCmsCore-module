<?php

namespace OctoCmsModule\Core\Tests\Entities;

use OctoCmsModule\Core\Entities\Media;
use OctoCmsModule\Core\Entities\MediaLang;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class MediaLangTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class MediaLangTest extends TestCase
{


    public function test_MediaLangBelongsToMedia()
    {
        /** @var MediaLang $mediaLang */
        $mediaLang = MediaLang::factory()->create();

        $mediaLang->load('media');

        $this->assertInstanceOf(Media::class, $mediaLang->media);
    }
}
