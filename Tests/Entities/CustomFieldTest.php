<?php

namespace OctoCmsModule\Core\Tests\Entities;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Core\Entities\CustomField;
use OctoCmsModule\Core\Entities\CustomFieldEntity;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class CustomFieldTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class CustomFieldTest extends TestCase
{


    public function test_CustomFieldHasManyCustomFieldEntities()
    {
        /** @var CustomField $customField */
        $customField = CustomField::factory()->has(CustomFieldEntity::factory()->count(2))->create();

        $customField->load('customFieldEntities');

        $this->assertInstanceOf(Collection::class, $customField->customFieldEntities);
        $this->assertInstanceOf(CustomFieldEntity::class, $customField->customFieldEntities->first());
    }
}
