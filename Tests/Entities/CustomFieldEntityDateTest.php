<?php

namespace OctoCmsModule\Core\Tests\Entities;

use Carbon\Carbon;
use OctoCmsModule\Core\Entities\CustomFieldEntity;
use OctoCmsModule\Core\Entities\CustomFieldEntityDate;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class CustomFieldEntityDateTest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Core\Tests\Entities
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class CustomFieldEntityDateTest extends TestCase
{
    public function test_CustomFieldEntityHasOneCustomFieldEntityDate()
    {
        /** @var CustomFieldEntity $customFieldRegistry */
        $customFieldRegistry = CustomFieldEntity::factory()->create();

        $customFieldRegistry->valuable()->associate(CustomFieldEntityDate::factory()->create());

        $customFieldRegistry->load('valuable');

        $this->assertEquals(
            'CustomFieldEntityDate',
            $customFieldRegistry->valuable_type
        );

        $this->assertInstanceOf(CustomFieldEntityDate::class, $customFieldRegistry->valuable);
    }

    public function dataProvider()
    {
        $providers = [];

        $providers[] = ['string'];
        $providers[] = [true];
        $providers[] = [null];
        $providers[] = [1];
        $providers[] = [1.0];

        return $providers;
    }

    /**
     * @param $value
     *
     * @dataProvider dataProvider
     */
    public function test_setValueAttributeNull($value)
    {
        /**
         * CustomFieldEntityDate
         *
         * @var CustomFieldEntityDate $customFieldEntityDate
         */
        $customFieldEntityDate = CustomFieldEntityDate::factory()->create();

        $customFieldEntityDate->value = $value;

        $this->assertNull($customFieldEntityDate->value);
    }

    public function test_setValueAttributeOK()
    {
        /**
         * CustomFieldEntityDate
         *
         * @var CustomFieldEntityDate $customFieldEntityDate
         */
        $customFieldEntityDate = CustomFieldEntityDate::factory()->create();

        $customFieldEntityDate->value = '1970-01-01';

        $this->assertEquals(
            Carbon::createFromFormat('Y-m-d', '1970-01-01'),
            $customFieldEntityDate->value
        );
    }
}
