<?php

namespace OctoCmsModule\Core\Tests\Entities;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Entities\ProviderImportData;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ProviderTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class ProviderImportTest extends TestCase
{


    public function test_ProviderImportHasManyProviederImportData()
    {
        /** @var ProviderImport $providerImport */
        $providerImport = ProviderImport::factory()
            ->has(ProviderImportData::factory()->count(2))
            ->create();

        $providerImport->load('providerImportData');

        $this->assertInstanceOf(Collection::class, $providerImport->providerImportData);
        $this->assertInstanceOf(ProviderImportData::class, $providerImport->providerImportData->first());
    }
}
