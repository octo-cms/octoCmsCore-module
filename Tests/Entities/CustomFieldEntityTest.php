<?php

namespace OctoCmsModule\Core\Tests\Entities;

use OctoCmsModule\Core\Entities\CustomField;
use OctoCmsModule\Core\Entities\CustomFieldEntity;
use OctoCmsModule\Core\Entities\CustomFieldEntityString;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class CustomFieldEntityTest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Core\Tests\Entities
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class CustomFieldEntityTest extends TestCase
{

    public function test_CustomFieldEntityBelongsToCustomField()
    {
        /** @var CustomFieldEntity $customFieldRegistry */
        $customFieldRegistry = CustomFieldEntity::factory()->create();

        $customFieldRegistry->load('customField');

        $this->assertInstanceOf(CustomField::class, $customFieldRegistry->customField);
    }

}
