<?php

namespace OctoCmsModule\Core\Tests\Entities;

use Carbon\Carbon;
use OctoCmsModule\Core\Entities\Address;
use OctoCmsModule\Core\Entities\CustomField;
use OctoCmsModule\Core\Entities\CustomFieldEntity;
use OctoCmsModule\Core\Entities\CustomFieldEntityDate;
use OctoCmsModule\Core\Entities\CustomFieldEntityDecimal;
use OctoCmsModule\Core\Entities\CustomFieldEntityNumeric;
use OctoCmsModule\Core\Entities\CustomFieldEntityString;
use OctoCmsModule\Core\Entities\CustomFieldEntityText;
use OctoCmsModule\Core\Entities\Email;
use OctoCmsModule\Core\Entities\Phone;
use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Core\Entities\Role;
use OctoCmsModule\Core\Entities\User;
use Illuminate\Support\Collection;
use OctoCmsModule\Core\Entities\UserEventLog;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class UserTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class UserTest extends TestCase
{


    public function test_UserHasManyRoles()
    {
        /** @var User $user */
        $user = User::factory()
            ->has(Role::factory()->count(2), 'roles')
            ->create();

        $user->load('roles');

        $this->assertInstanceOf(Collection::class, $user->roles);
        $this->assertInstanceOf(Role::class, $user->roles->first());
    }

    public function test_UserHasManyPictures()
    {
        /** @var User $user */
        $user = User::factory()
            ->has(Picture::factory()->count(2), 'pictures')
            ->create();

        $user->load('pictures');

        $this->assertInstanceOf(Collection::class, $user->pictures);
        $this->assertInstanceOf(Picture::class, $user->pictures->first());
    }

    public function test_UserHasManyPicturesWithMainTag()
    {
        /** @var User $user */
        $user = User::factory()
            ->has(
                Picture::factory()->count(2)->state(['tag' => Picture::TAG_MAIN]),
                'pictures'
            )->has(
                Picture::factory()->count(3)->state(['tag' => 'not main']),
                'pictures'
            )->create();

        /** @var Collection $pictures */
        $pictures = $user->getPictures(Picture::TAG_MAIN)->get();

        $this->assertInstanceOf(Collection::class, $pictures);

        $this->assertInstanceOf(Picture::class, $pictures->first());

        $this->assertEquals(Picture::TAG_MAIN, $pictures->first()->tag);

        $this->assertCount(2, $pictures);
    }

    public function test_UserLoadPicturesWithMainTag()
    {
        /** @var User $user */
        $user = User::factory()
            ->has(
                Picture::factory()->count(2)->state(['tag' => Picture::TAG_MAIN]),
                'pictures'
            )->has(
                Picture::factory()->count(3)->state(['tag' => 'not main']),
                'pictures'
            )->create();

        $user->loadPictures(Picture::TAG_MAIN);

        $this->assertInstanceOf(Collection::class, $user->pictures);

        $this->assertCount(2, $user->pictures);

        $this->assertInstanceOf(Picture::class, $user->pictures->first());

        $this->assertEquals(Picture::TAG_MAIN, $user->pictures->first()->tag);
    }

    public function test_UserHasManyEmails()
    {
        /** @var User $user */
        $user = User::factory()->has(Email::factory()->count(2))->create();

        $user->load('emails');

        $this->assertInstanceOf(Collection::class, $user->emails);
        $this->assertInstanceOf(Email::class, $user->emails->first());
    }

    public function test_UserHasManyPhones()
    {
        /** @var User $user */
        $user = User::factory()->has(Phone::factory()->count(2))->create();

        $user->load('phones');

        $this->assertInstanceOf(Collection::class, $user->phones);
        $this->assertInstanceOf(Phone::class, $user->phones->first());
    }

    public function test_ParentBelongsToManyChildren()
    {
        /** @var User $parent */
        $parent = User::factory()->create();

        foreach (User::factory()->count(2)->create() as $child) {
            $parent->children()->save($child, ['type' => 'type']);
        }

        $parent->load('children');

        $this->assertInstanceOf(Collection::class, $parent->children);
        $this->assertInstanceOf(User::class, $parent->children->first());
        $this->assertEquals('type', $parent->children->first()->pivot->type);
    }

    public function test_ChildrenBelongsToManyParents()
    {
        /** @var User $child */
        $child = User::factory()->create();

        foreach (User::factory()->count(2)->create() as $parent) {
            $child->parents()->save($parent, ['type' => 'type']);
        }

        $child->load('children');

        $this->assertInstanceOf(Collection::class, $child->parents);
        $this->assertInstanceOf(User::class, $child->parents->first());
        $this->assertEquals('type', $child->parents->first()->pivot->type);
    }

    public function test_scopeTypePivotTable()
    {
        /** @var User $child */
        $child = User::factory()->create();

        $child->parents()->save(User::factory()->create(['active' => false]), ['type' => 'company']);

        $child->parents()->save(User::factory()->create(['active' => true]), ['type' => 'agent']);

        /** @var Collection $agents */
        $agents = $child->parents()->type('agent')->get();

        $this->assertCount(1, $agents);
        $this->assertEquals(true, $agents->first()->active);
    }

    public function test_UserHasOneDefaultEmail()
    {
        /**
         * User
         *
         * @var User $user
         */
        $user = User::factory()
            ->has(Email::factory()->state(['default' => true]))
            ->has(Email::factory()->count(2)->state(['default' => false]))
            ->create();

        /**
         * Email
         *
         * @var Email $email
         */
        $email = $user->defaultEmail()->first();

        $this->assertInstanceOf(Email::class, $email);

        $this->assertEquals(true, $email->default);
    }

    public function test_UserHasManyCustomFieldEntities()
    {
        /** @var User $user */
        $user = User::factory()->has(CustomFieldEntity::factory()->count(2))->create();

        $user->load('customFieldEntities');

        $this->assertInstanceOf(Collection::class, $user->customFieldEntities);
        $this->assertInstanceOf(CustomFieldEntity::class, $user->customFieldEntities->first());
    }


    public function customFieldDataProvider()
    {
        $providers = [];

        $providers[] = [CustomField::TYPE_STRING, 'string', 'test-string', CustomFieldEntityString::class];
        $providers[] = [CustomField::TYPE_TEXT, 'text', 'test-text', CustomFieldEntityText::class];
        $providers[] = [CustomField::TYPE_DATE, 'date', Carbon::now(), CustomFieldEntityDate::class];
        $providers[] = [CustomField::TYPE_NUMERIC, 'numeric', 10, CustomFieldEntityNumeric::class];
        $providers[] = [CustomField::TYPE_DECIMAL, 'decimal', 10.0, CustomFieldEntityDecimal::class];
        return $providers;
    }

    /**
     * Name test_saveCustomFieldValue
     * @param $type
     * @param $name
     * @param $value
     * @param $class
     *
     * @dataProvider customFieldDataProvider
     * @return void
     */
    public function test_saveCustomFieldValue($type, $name, $value, $class)
    {
        /** @var User $user */
        $user = User::factory()->create();

        CustomField::factory()->create(['name' => $name, 'type' => $type]);

        $result = $user->saveCustomFieldValue($name, $value);

        $this->assertInstanceOf(
            CustomFieldEntity::class,
            $result
        );

        $this->assertInstanceOf(
            $class,
            $result->valuable
        );

        $this->assertEquals(
            $value,
            $result->valuable->value
        );

    }

    public function test_saveCustomFieldNull()
    {
        /** @var User $user */
        $user = User::factory()->create();

        CustomField::factory()->create(['name' => 'firstname', 'type' => 'wrong-type']);

        $this->assertNull(
            $user->saveCustomFieldValue('firstname', 'test-firstname')
        );
    }

    public function test_updateCustomFieldValue()
    {
        /** @var User $user */
        $user = User::factory()->create();

        CustomField::factory()->create(['name' => 'firstname', 'type' => CustomField::TYPE_STRING]);

        $result = $user->saveCustomFieldValue('firstname', 'test-firstname');

        $resultUpdated = $user->saveCustomFieldValue('firstname', 'test-firstname-updated');

        $this->assertEquals(
            'test-firstname-updated',
            $resultUpdated->valuable->value
        );

        $this->assertEquals(
            $result->id,
            $resultUpdated->id
        );
    }

    public function test_UserHasManyAddresses()
    {
        /**
         * User
         *
         * @var User $user
         */
        $user = User::factory()
            ->has(Address::factory()->count(2))
            ->create();

        $user->load('addresses');

        $this->assertInstanceOf(Collection::class, $user->addresses);
        $this->assertInstanceOf(Address::class, $user->addresses->first());
    }

    public function dataproviderPhones()
    {
        $providers = [];

        $providers[] = [
            [],
            [
                ['label' => 'label-12345', 'phone' => 12345, 'default' => true],
                ['label' => 'label-67890', 'phone' => 67890],
            ],
            [
                ['label' => 'label-12345', 'phone' => 12345, 'default' => true],
                ['label' => 'label-67890', 'phone' => 67890],
            ]
        ];

        $providers[] = [
            [
                ['label' => 'label-12345', 'phone' => 12345, 'default' => true]
            ],
            [
                ['label' => 'label-67890', 'phone' => 67890],
            ],
            [
                ['label' => 'label-67890', 'phone' => 67890, 'default' => true],
            ]
        ];

        $providers[] = [
            [
                ['label' => 'label-12345', 'phone' => 12345, 'default' => true],
            ],
            [
                ['id' => 1, 'label' => 'label-12345-new', 'phone' => 123456789, 'default' => true],
                ['label' => 'label-67890', 'phone' => 67890],
            ],
            [
                ['label' => 'label-12345-new', 'phone' => 123456789, 'default' => true],
                ['label' => 'label-67890', 'phone' => 67890],
            ]
        ];

        $providers[] = [
            [
                ['label' => 'label-12345', 'phone' => 12345, 'default' => true],
            ],
            [ ],
            [ ]
        ];

        $providers[] = [
            [
                ['label' => 'label-12345', 'phone' => 12345, 'default' => true],
                ['label' => 'label-67890', 'phone' => 67890],
            ],
            [
                ['label' => 'label-12345-new', 'phone' => 12345],
                ['label' => 'label-67890-new', 'phone' => 67890],
            ],
            [
                ['label' => 'label-12345-new', 'phone' => 12345, 'default' => true],
                ['label' => 'label-67890-new', 'phone' => 67890],
            ]
        ];

        return $providers;
    }

    /**
     * Name test_syncPhones
     * @param $dataPhones
     * @param $newPhones
     * @param $dbPhones
     * @return void
     * @dataProvider dataproviderPhones
     */
    public function test_syncPhones($dataPhones, $newPhones, $dbPhones)
    {
        /**
         * User
         *
         * @var User $user
         */
        $user = User::factory()->create();

        foreach ($dataPhones as $dataPhone) {
            $user->phones()->save(Phone::factory()->create($dataPhone));
        }

        $user->syncPhones($newPhones);

        if (empty($dbPhones)) {
            $this->assertEmpty($user->phones()->get());
        } else {
            foreach ($dbPhones as $dbPhone) {
                $dbPhone['phoneable_id'] = $user->id;
                $dbPhone['phoneable_type'] = 'User';
                $this->assertDatabaseHas('phones', $dbPhone);
            }
        }


    }

    public function dataproviderEmail()
    {
        $providers = [];

        $providers[] = [
            [],
            [
                ['label' => 'label-1', 'email' => 'test1@gmail.com', 'default' => true],
                ['label' => 'label-2', 'email' => 'test2@gmail.com'],
            ],
            [
                ['label' => 'label-1', 'email' => 'test1@gmail.com', 'default' => true],
                ['label' => 'label-2', 'email' => 'test2@gmail.com'],
            ]
        ];

        $providers[] = [
            [
                ['label' => 'label-1', 'email' => 'test1@gmail.com', 'default' => true]
            ],
            [
                ['label' => 'label-2', 'email' => 'test2@gmail.com'],
            ],
            [
                ['label' => 'label-2', 'email' => 'test2@gmail.com', 'default' => true],
            ]
        ];

        $providers[] = [
            [
                ['label' => 'label-1', 'email' => 'test1@gmail.com', 'default' => true],
            ],
            [
                ['id' => 1, 'label' => 'label-1-new', 'email' => 123456789, 'default' => true],
                ['label' => 'label-2', 'email' => 'test2@gmail.com'],
            ],
            [
                ['label' => 'label-1-new', 'email' => 123456789, 'default' => true],
                ['label' => 'label-2', 'email' => 'test2@gmail.com'],
            ]
        ];

        $providers[] = [
            [
                ['label' => 'label-1', 'email' => 'test1@gmail.com', 'default' => true],
            ],
            [ ],
            [ ]
        ];

        $providers[] = [
            [
                ['label' => 'label-1', 'email' => 'test1@gmail.com', 'default' => true],
                ['label' => 'label-2', 'email' => 'test2@gmail.com'],
            ],
            [
                ['label' => 'label-1-new', 'email' => 'test1@gmail.com'],
                ['label' => 'label-2-new', 'email' => 'test2@gmail.com'],
            ],
            [
                ['label' => 'label-1-new', 'email' => 'test1@gmail.com', 'default' => true],
                ['label' => 'label-2-new', 'email' => 'test2@gmail.com'],
            ]
        ];

        return $providers;
    }

    /**
     * Name test_syncPhones
     * @param $dataEmails
     * @param $newEmails
     * @param $dbEmails
     * @return void
     * @dataProvider dataproviderEmail
     */
    public function test_syncEmails($dataEmails, $newEmails, $dbEmails)
    {
        /**
         * User
         *
         * @var User $user
         */
        $user = User::factory()->create();

        foreach ($dataEmails as $dataEmail) {
            $user->emails()->save(Email::factory()->create($dataEmail));
        }

        $user->syncEmails($newEmails);

        if (empty($dbEmails)) {
            $this->assertEmpty($user->emails()->get());
        } else {
            foreach ($dbEmails as $dbEmail) {
                $dbPhone['emailable_id'] = $user->id;
                $dbPhone['emailable_type'] = 'User';
                $this->assertDatabaseHas('emails', $dbEmail);
            }
        }


    }

    public function dataproviderAddress()
    {
        $providers = [];

        $providers[] = [
            [],
            [
                ['alias' => 'alias-1', 'locality' => 'locality-1', 'default' => true],
                ['alias' => 'alias-2', 'locality' => 'locality-2'],
            ],
            [
                ['alias' => 'alias-1', 'locality' => 'locality-1', 'default' => true],
                ['alias' => 'alias-2', 'locality' => 'locality-2'],
            ]
        ];

        $providers[] = [
            [
                ['alias' => 'alias-1', 'locality' => 'locality-1', 'default' => true]
            ],
            [
                ['alias' => 'alias-2', 'locality' => 'locality-2'],
            ],
            [
                ['alias' => 'alias-2', 'locality' => 'locality-2', 'default' => true],
            ]
        ];

        $providers[] = [
            [
                ['alias' => 'alias-1', 'locality' => 'locality-1', 'default' => true],
            ],
            [
                ['id' => 1, 'alias' => 'alias-1-new', 'locality' => 'locality-1-new', 'default' => true],
                ['alias' => 'alias-2', 'locality' => 'locality-2'],
            ],
            [
                ['alias' => 'alias-1-new', 'locality' => 'locality-1-new', 'default' => true],
                ['alias' => 'alias-2', 'locality' => 'locality-2'],
            ]
        ];

        $providers[] = [
            [
                ['alias' => 'alias-1', 'locality' => 'locality-1', 'default' => true],
            ],
            [ ],
            [ ]
        ];

        $providers[] = [
            [
                ['alias' => 'alias-1', 'locality' => 'locality-1', 'default' => true],
                ['alias' => 'alias-2', 'locality' => 'locality-2'],
            ],
            [
                ['alias' => 'alias-1-new', 'locality' => 'locality-1'],
                ['alias' => 'alias-2-new', 'locality' => 'locality-2'],
            ],
            [
                ['alias' => 'alias-1-new', 'locality' => 'locality-1', 'default' => true],
                ['alias' => 'alias-2-new', 'locality' => 'locality-2'],
            ]
        ];

        return $providers;
    }

    /**
     * Name test_syncPhones
     * @param $dataAddresses
     * @param $newAddresses
     * @param $dbAddresses
     * @return void
     * @dataProvider dataproviderAddress
     */
    public function test_syncAddresses($dataAddresses, $newAddresses, $dbAddresses)
    {
        /**
         * User
         *
         * @var User $user
         */
        $user = User::factory()->create();

        foreach ($dataAddresses as $dataAddress) {
            $user->addresses()->save(Address::factory()->create($dataAddress));
        }

        $user->syncAddresses($newAddresses);

        if (empty($dbAddresses)) {
            $this->assertEmpty($user->addresses()->get());
        } else {
            foreach ($dbAddresses as $dbAddress) {
                $dbPhone['addressable_id'] = $user->id;
                $dbPhone['addressable_type'] = 'User';
                $this->assertDatabaseHas('addresses', $dbAddress);
            }
        }


    }

    public function test_UserHasManyEventLogs()
    {
        /** @var User $user */
        $user = User::factory()
            ->has(UserEventLog::factory()->count(2), 'userEventLogs')
            ->create();

        $user->load('userEventLogs');

        $this->assertInstanceOf(Collection::class, $user->userEventLogs);
        $this->assertInstanceOf(UserEventLog::class, $user->userEventLogs->first());
    }

    public function test_scopeWhereCustomFields()
    {
        /**
         * CustomField entity's name
         *
         * @var string $customFieldName CustomField name string
         */
        $customFieldName = 'custom-field-1';


        /**
         * Query to search
         *
         * @var string $query query string
         */
        $query = 'nome-utente-1';

        /**
         * User with Custom Fields
         *
         * @var User $user User instance
         */
        $user = User::factory()->create();

        $user->customFieldEntities()->save(
            CustomFieldEntity::factory()
                ->for(
                    CustomField::factory()->state(['type' => 'string', 'name' => $customFieldName]),
                    'customField'
                )
                ->for(CustomFieldEntityString::factory()->state(['value' => $query]), 'valuable')
                ->create()
        );

        User::factory()->count(2)->create();

        /**
         * Filtered User collection
         *
         * @var Collection $filteredUsers Collection instance
         */
        $filteredUsers = User::whereCustomFields($customFieldName, 'LIKE', "%$query%")->get();

        $this->assertCount(1, $filteredUsers);

        $this->assertEquals($user->id, $filteredUsers->first()->id);
    }
}
