<?php

namespace OctoCmsModule\Core\Tests\Entities;

use Illuminate\Database\Eloquent\Collection;
use OctoCmsModule\Core\Entities\ImportTemp;
use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Core\Entities\ProviderImportData;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ProviderTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class ProviderTest extends TestCase
{


    public function test_ProviderHasManyProviederImportData()
    {
        /** @var Provider $provider */
        $provider = Provider::factory()
            ->has(ProviderImportData::factory()->count(2))
            ->create();

        $provider->load('providerImportData');

        $this->assertInstanceOf(Collection::class, $provider->providerImportData);
        $this->assertInstanceOf(ProviderImportData::class, $provider->providerImportData->first());
    }

    public function test_getAndSetXlsMappingAttribute()
    {
        /** @var Provider $provider */
        $provider = Provider::factory()->create();

        /** @var array $xlsMappingArray */
        $xlsMappingArray = ['xls_mapping'];

        $provider->xls_mapping = $xlsMappingArray;

        $provider->save();

        $this->assertDatabaseHas('providers', [
            'xls_mapping' => serialize($xlsMappingArray),
        ]);

        $this->assertEquals(
            $provider->xls_mapping,
            $xlsMappingArray
        );
    }
}
