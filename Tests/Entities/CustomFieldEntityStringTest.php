<?php

namespace OctoCmsModule\Core\Tests\Entities;

use OctoCmsModule\Core\Entities\CustomFieldEntity;
use OctoCmsModule\Core\Entities\CustomFieldEntityString;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class CustomFieldEntityStringTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class CustomFieldEntityStringTest extends TestCase
{
    public function test_CustomFieldEntityHasOneCustomFieldEntityString()
    {
        /** @var CustomFieldEntity $customFieldRegistry */
        $customFieldRegistry = CustomFieldEntity::factory()->create();

        $customFieldRegistry->valuable()
            ->associate(CustomFieldEntityString::factory()->create(['value' => 'value']));

        $customFieldRegistry->load('valuable');

        $this->assertEquals(
            'CustomFieldEntityString',
            $customFieldRegistry->valuable_type
        );

        $this->assertEquals(
            'value',
            $customFieldRegistry->valuable->value
        );

        $this->assertInstanceOf(CustomFieldEntityString::class, $customFieldRegistry->valuable);
    }

    public function dataProvider()
    {
        $providers = [];

        $providers[] = [true];
        $providers[] = [null];
        $providers[] = [1];
        $providers[] = [1.0];
        $providers[] = [[]];

        return $providers;
    }

    /**
     * @param $value
     *
     * @dataProvider dataProvider
     */
    public function test_setValueAttributeNull($value)
    {
        /**
         * CustomFieldEntityString
         *
         * @var CustomFieldEntityString $customFieldEntityString
         */
        $customFieldEntityString = CustomFieldEntityString::factory()->create();

        $customFieldEntityString->value = $value;

        $this->assertNull($customFieldEntityString->value);
    }

    public function test_setValueAttributeOK()
    {
        /**
         * CustomFieldEntityString
         *
         * @var CustomFieldEntityString $customFieldEntityString
         */
        $customFieldEntityString = CustomFieldEntityString::factory()->create();

        $customFieldEntityString->value = 'string';

        $this->assertEquals('string', $customFieldEntityString->value);
    }
}
