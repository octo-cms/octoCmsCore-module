<?php

namespace OctoCmsModule\Core\Tests\Entities;

use OctoCmsModule\Core\Entities\CustomFieldEntity;
use OctoCmsModule\Core\Entities\CustomFieldEntityText;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class CustomFieldEntityTextTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class CustomFieldEntityTextTest extends TestCase
{
    public function test_CustomFieldEntityHasOneCustomFieldEntityText()
    {
        /** @var CustomFieldEntity $customFieldRegistry */
        $customFieldRegistry = CustomFieldEntity::factory()->create();

        $customFieldRegistry->valuable()
            ->associate(CustomFieldEntityText::factory()->create(['value' => 'value']));

        $customFieldRegistry->load('valuable');

        $this->assertEquals(
            'CustomFieldEntityText',
            $customFieldRegistry->valuable_type
        );

        $this->assertEquals(
            'value',
            $customFieldRegistry->valuable->value
        );

        $this->assertInstanceOf(CustomFieldEntityText::class, $customFieldRegistry->valuable);
    }

    public function dataProvider()
    {
        $providers = [];

        $providers[] = [true];
        $providers[] = [null];
        $providers[] = [1];
        $providers[] = [1.0];
        $providers[] = [[]];

        return $providers;
    }

    /**
     * @param $value
     *
     * @dataProvider dataProvider
     */
    public function test_setValueAttributeNull($value)
    {
        /**
         * CustomFieldEntityText
         *
         * @var CustomFieldEntityText $customFieldEntityText
         */
        $customFieldEntityText = CustomFieldEntityText::factory()->create();

        $customFieldEntityText->value = $value;

        $this->assertNull($customFieldEntityText->value);
    }

    public function test_setValueAttributeOK()
    {
        /**
         * CustomFieldEntityText
         *
         * @var CustomFieldEntityText $customFieldEntityText
         */
        $customFieldEntityText = CustomFieldEntityText::factory()->create();

        $customFieldEntityText->value = 'text';

        $this->assertEquals('text', $customFieldEntityText->value);
    }
}
