<?php

namespace OctoCmsModule\Core\Tests\Entities;

use OctoCmsModule\Core\Entities\CompanyRegistry;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class CompanyRegistryTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class CompanyRegistryTest extends TestCase
{


    public function test_CompanyRegistryBelongsToRegistry()
    {
        /** @var CompanyRegistry $companyRegistry */
        $companyRegistry = CompanyRegistry::factory()->create();

        $companyRegistry->load('registry');

        $this->assertInstanceOf(Registry::class, $companyRegistry->registry);
    }
}
