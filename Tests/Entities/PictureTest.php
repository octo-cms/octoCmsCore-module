<?php

namespace OctoCmsModule\Core\Tests\Entities;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Core\Entities\PictureLang;
use OctoCmsModule\Core\Services\PictureService;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class PictureTest
 *
 * @package OctoCmsModule\Core\Tests\Entities
 */
class PictureTest extends TestCase
{


    public function test_PictureHasManyPictureLang()
    {
        /** @var Picture $picture */
        $picture = Picture::factory()->has(PictureLang::factory()->count(2))->create();

        $picture->load('pictureLangs');

        $this->assertInstanceOf(Collection::class, $picture->pictureLangs);
        $this->assertInstanceOf(PictureLang::class, $picture->pictureLangs->first());
    }

    /**
     * Name test_deletePicture
     *
     * @return void
     * @throws \Exception
     */
    public function test_deletePicture()
    {
        Storage::fake('gcs');

        /** @var Picture $picture */
        $picture = Picture::factory()->create([
            'src'  => 'picture.jpeg',
            'webp' => 'picture.webp',
        ]);

        Storage::disk('gcs')->putFileAs('', UploadedFile::fake()->image($picture->src), $picture->src);
        Storage::disk('gcs')->assertExists($picture->src);

        Storage::disk('gcs')->putFileAs('', UploadedFile::fake()->image($picture->webp), $picture->webp);
        Storage::disk('gcs')->assertExists($picture->webp);

        $picture->delete();

        $this->assertDatabaseMissing('pictures', [
            'id' => $picture->id,
        ]);

        Storage::disk('gcs')->assertMissing($picture->src);
        Storage::disk('gcs')->assertMissing($picture->webp);
    }

    public function test_getUrlSrc()
    {
        /** @var Picture $picture */
        $picture = Picture::factory()->create([
            'src' => 'path/picture.jpeg',
        ]);

        $this->assertEquals(
            PictureService::BUCKET_BASE_URL . '/' . env('GOOGLE_CLOUD_STORAGE_BUCKET') . '/path/picture.jpeg',
            $picture->getUrlSrc()
        );
    }

    public function test_getUrlWebp()
    {
        /** @var Picture $picture */
        $picture = Picture::factory()->create([
            'webp' => 'path/picture.webp',
        ]);

        $this->assertEquals(
            PictureService::BUCKET_BASE_URL . '/' . env('GOOGLE_CLOUD_STORAGE_BUCKET') . '/path/picture.webp',
            $picture->getUrlWebp()
        );
    }
}
