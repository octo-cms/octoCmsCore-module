<?php

namespace OctoCmsModule\Core\Tests\Controllers\ProviderImportController;

use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Core\Entities\Provider;

/**
 * Class UpdateTest
 *
 * @package OctoCmsModule\Core\Tests\Controllers\ProviderController
 */
class UploadTest extends TestCase
{


    public function test_update()
    {

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('core.provider.import.upload'),
            [
                'file' => UploadedFile::fake()->create('import.xls', 150)
            ]
        );

        $response->assertStatus(Response::HTTP_CREATED);

        $response->assertJsonStructure([
            'data' => [
                'id',
                'status'
            ]
        ]);
    }
}
