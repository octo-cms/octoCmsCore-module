<?php

namespace OctoCmsModule\Core\Tests\Controllers\ProviderImportController;

use Illuminate\Http\Response;
use Illuminate\Support\Facades\Queue;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Jobs\SyncProviderImportWithRegistryJob;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ShowTest
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Controllers\UserController
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class SyncWithRegistryTest extends TestCase
{


    public function test_syncWithRegistry()
    {
        Queue::fake();

        Sanctum::actingAs(self::createAdminUser());

        /** @var ProviderImport $providerImport */
        $providerImport = ProviderImport::factory()->create();

        $response = $this->json(
            'POST',
            route('core.provider.import.registry.sync', ['id' => $providerImport->id]),
        );

        $response->assertStatus(Response::HTTP_OK);

        Queue::assertPushed(SyncProviderImportWithRegistryJob::class);

    }

    public function test_syncWithRegistryNotFound()
    {
        Queue::fake();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('core.provider.import.registry.sync', ['id' => 100]),
        );

        $response->assertStatus(Response::HTTP_NOT_FOUND);

        Queue::assertNotPushed(SyncProviderImportWithRegistryJob::class);
    }

}
