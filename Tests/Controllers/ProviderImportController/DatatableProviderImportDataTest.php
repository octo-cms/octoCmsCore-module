<?php

namespace OctoCmsModule\Core\Tests\Controllers\ProviderImportController;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Entities\ProviderImportData;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class DatatableTest
 *
 * @package OctoCmsModule\Core\Tests\Controllers\UserController
 */
class DatatableProviderImportDataTest extends TestCase
{

    public function test_datatableIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        ProviderImport::factory()->has(ProviderImportData::factory()->count(15))->create();

        $this->datatableTest('admin.datatables.core.provider.import.data', ['id' => 1]);
    }
}
