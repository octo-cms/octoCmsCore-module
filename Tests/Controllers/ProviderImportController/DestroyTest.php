<?php

namespace OctoCmsModule\Core\Tests\Controllers\ProviderImportController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Entities\ProviderImportData;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class DestroyTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Controllers\ProviderImportController
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class DestroyTest extends TestCase
{


    public function test_destroy()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var ProviderImport $providerImport */
        $providerImport = ProviderImport::factory()->has(ProviderImportData::factory()->count(5))->create();

        $response = $this->json(
            'DELETE',
            route('core.provider.import.delete', ['id' => $providerImport->id]),
        );

        $response->assertStatus(Response::HTTP_OK);

        $this->assertDatabaseMissing('provider_imports',['id' => $providerImport->id]);
        $this->assertDatabaseMissing('provider_import_data',['provider_import_id' => $providerImport->id]);

        $response->assertJsonStructure([
            'data' => [
                'id',
                'status',
            ]
        ]);
    }

    public function test_destroyNotFound()
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'DELETE',
            route('core.provider.import.delete', ['id' => 100]),
        );

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function test_destroyNotLogged()
    {
        ProviderImport::factory()->has(ProviderImportData::factory()->count(5))->create();

        $response = $this->json(
            'DELETE',
            route('core.provider.import.delete', ['id' => 1]),
        );

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

}
