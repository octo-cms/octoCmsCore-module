<?php

namespace OctoCmsModule\Core\Tests\Controllers\ProviderImportController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ShowTest
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Controllers\UserController
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class ShowTest extends TestCase
{


    public function test_show()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var ProviderImport $providerImport */
        $providerImport = ProviderImport::factory()->create();

        $response = $this->json(
            'GET',
            route('core.provider.import.show', ['id' => $providerImport->id]),
        );

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonStructure([
            'data' => [
                'id',
                'status',
            ]
        ]);
    }

    public function test_showNotFound()
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'GET',
            route('core.provider.import.show', ['id' => 100]),
        );

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

}
