<?php

namespace OctoCmsModule\Core\Tests\Controllers\ProviderImportController;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Entities\ProviderImportData;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class DatatableProviderImportIndexTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Controllers\ProviderImportController
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class DatatableProviderImportTest extends TestCase
{

    public function test_datatableIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        ProviderImport::factory()->count(15)->has(ProviderImportData::factory()->count(15))->create();

        $this->datatableTest('admin.datatables.core.provider.import');
    }
}
