<?php

namespace OctoCmsModule\Core\Tests\Controllers\TagController;

use OctoCmsModule\Core\Entities\Tag;
use OctoCmsModule\Core\Entities\TagGroup;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetByGroupTest
 *
 * @package OctoCmsModule\Core\Tests\Controllers\TagController
 */
class GetByGroupTest extends TestCase
{


    public function test_getTagByGroup()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var TagGroup $tagGroup */
        $tagGroup = TagGroup::factory()->create([
            'name' => 'group',
        ]);

        Tag::factory()->count(3)->create([
            'tag_group_id' => $tagGroup->id,
        ]);

        Tag::factory()->count(2)->create();

        $response = $this->json(
            'POST',
            route('core.tags.get.by.group'),
            [
                'group'       => 'group',
                'currentPage' => 1,
                'rowsInPage'  => 10,
            ]
        );

        $response->assertStatus(Response::HTTP_OK);

        $content = json_decode($response->getContent(), true);

        $this->assertCount(3, Arr::get($content, 'collection', []));
    }

    public function test_emptyGroup()
    {
        Sanctum::actingAs(self::createAdminUser());

        Tag::factory()->count(3)->create();

        $response = $this->json(
            'POST',
            route('core.tags.get.by.group'),
            [
                'currentPage' => 1,
                'rowsInPage'  => 10,
            ]
        );

        $response->assertStatus(Response::HTTP_OK);

        $content = json_decode($response->getContent(), true);

        $this->assertCount(3, Arr::get($content, 'collection', []));
    }
}
