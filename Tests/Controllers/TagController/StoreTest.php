<?php

namespace OctoCmsModule\Core\Tests\Controllers\TagController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class StoreTest
 *
 * @package OctoCmsModule\Core\Tests\Controllers\TagController
 */
class StoreTest extends TestCase
{


    public function test_store()
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('core.tags.store'),
            [
                'name'  => 'tag name',
                'group' => 'group name',
            ]
        );

        $response->assertStatus(Response::HTTP_CREATED);
    }
}
