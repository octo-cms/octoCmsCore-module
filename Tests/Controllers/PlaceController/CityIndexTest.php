<?php

namespace OctoCmsModule\Core\Tests\Controllers\PlaceController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Core\Entities\City;

class CityIndexTest extends TestCase
{


    public function test_search_city_no_query()
    {
        Sanctum::actingAs(self::createAdminUser());
        $response = $this->json(
            'POST',
            route('core.select.places.cities')
        );
        $response->assertStatus(Response::HTTP_BAD_REQUEST);

    }

    public function test_search_city()
    {
        Sanctum::actingAs(self::createAdminUser());

        $cities = City::factory()->count(2)->create();

        $response = $this->json(
            'POST',
            route('core.select.places.cities'),
            ['query' => $cities[0]->denominazione]
        );

        $response->assertJsonFragment([
            'name'  => $cities[0]->denominazione,
            'value' => $cities[0]->cod_comune_alfanumerico,
        ]);
        $response->assertStatus(Response::HTTP_OK);
    }


}
