<?php

namespace OctoCmsModule\Core\Tests\Controllers\PlaceController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Core\Entities\Country;

class CountryIndexTest extends TestCase
{


    public function test_search_country_no_query()
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('core.select.places.countries')
        );
        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function test_search_country()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var Country $country */
        $country = Country::factory()->create();

        $response = $this->json(
            'POST',
            route('core.select.places.countries'),
            [
                'query'      => $country->name_it,
                'columnName' => 'name_it',
            ]
        );

        $response->assertJsonFragment(['name' => $country->name_it, "value" => $country->sigla_alpha_2]);
        $response->assertStatus(Response::HTTP_OK);

    }

    public function dataProvider()
    {
        /** @var array $providers */
        $providers = [
            ['name_cn'],
            [''],
        ];

        return $providers;
    }

    /**
     * @param string $columnName
     *
     * @dataProvider dataProvider
     */
    public function test_search_country_lang_not_found(string $columnName)
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var Country $country */
        $country = Country::factory()->create();

        $response = $this->json(
            'POST',
            route('core.select.places.countries'),
            [
                'query'      => $country->name_en,
                'columnName' => $columnName,
            ]
        );

        $response->assertJsonFragment(['name' => $country->name_en, "value" => $country->sigla_alpha_2]);
        $response->assertStatus(Response::HTTP_OK);
    }
}
