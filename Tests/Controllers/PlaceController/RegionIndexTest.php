<?php

namespace OctoCmsModule\Core\Tests\Controllers\PlaceController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Core\Entities\City;

class RegionIndexTest extends TestCase
{


    public function test_search_region_no_query()
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('core.select.places.regions')
        );

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function test_search_region()
    {
        Sanctum::actingAs(self::createAdminUser());

        $regions = City::factory()->count(2)->create();

        $response = $this->json(
            'POST',
            route('core.select.places.regions'),
            ['query' => $regions[0]->regione]
        );

        $response->assertJsonFragment(['name' => $regions[0]->regione, "value" => $regions[0]->cod_regione]);
        $response->assertStatus(Response::HTTP_OK);
    }
}
