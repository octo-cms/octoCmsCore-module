<?php

namespace OctoCmsModule\Core\Tests\Controllers\PlaceController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Core\Entities\City;

class ProvinceIndexTest extends TestCase
{


    public function test_search_province_no_query()
    {
        Sanctum::actingAs(self::createAdminUser());
        $response = $this->json(
            'POST',
            route('core.select.places.provinces')
        );
        $response->assertStatus(Response::HTTP_BAD_REQUEST);

    }

    public function test_search_province()
    {
        Sanctum::actingAs(self::createAdminUser());

        $provinces = City::factory()->count(2)->create();

        $response = $this->json(
            'POST',
            route('core.select.places.provinces'),
            ['query' => $provinces[0]->sovracomunale]
        );

        $response->assertJsonFragment([
            'name'  => $provinces[0]->sovracomunale,
            'value' => $provinces[0]->sigla_automobilistica,
        ]);
        $response->assertStatus(Response::HTTP_OK);

    }

}
