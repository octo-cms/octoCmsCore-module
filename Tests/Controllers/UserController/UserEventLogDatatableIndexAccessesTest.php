<?php

namespace OctoCmsModule\Core\Tests\Controllers\UserController;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Entities\UserEventLog;
use Tests\TestCase;

/**
 * Class UserEventLogDatatableIndexAccessesTest
 *
 * @package Tests\Core\Controllers\UserController
 */
class UserEventLogDatatableIndexAccessesTest extends TestCase
{
    public function test_datatableIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        UserEventLog::factory()
            ->for(User::factory())
            ->count(15)
            ->create();

        $this->datatableTest('admin.datatables.core.user.event.log.accesses', ['id' => 2]);
    }
}
