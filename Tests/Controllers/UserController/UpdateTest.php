<?php

namespace OctoCmsModule\Core\Tests\Controllers\UserController;

use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class UpdateTest
 *
 * @package OctoCmsModule\Core\Tests\Controllers\UserController
 */
class UpdateTest extends TestCase
{


    public function dataProvider()
    {
        /** @var array $providers */
        $providers = [];

        /** @var array $data */
        $data = [
            'username'     => 'username',
            'active'       => true,
            'roles'        => [['id' => 1]],
        ];

        $providers[] = [$data, Response::HTTP_OK];

        $fields = $data;
        unset($fields['username']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['active']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['roles']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        $fields['roles'] = [];
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['roles'][0]);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        return $providers;
    }

    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_update(array $fields, int $status)
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var User $user */
        $user = User::factory()->create();

        $response = $this->json(
            'PUT',
            route('core.users.update', ['id' => $user->id]),
            $fields
        );

        $response->assertStatus($status);
    }

    public function test_updateUsernameDuplicated()
    {
        Sanctum::actingAs(self::createAdminUser());

        User::factory()->create([
            'username' => 'usernameAlreadyPresent'
        ]);

        /** @var User $user */
        $user = User::factory()->create();

        $response = $this->json(
            'PUT',
            route('core.users.update', ['id' => $user->id]),
            [
                'username' => 'usernameAlreadyPresent',
                'active'   => true,
                'roles'    => [['id' => 1]],
            ]
        );

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }
}
