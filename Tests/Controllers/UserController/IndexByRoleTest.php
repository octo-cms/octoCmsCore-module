<?php

namespace OctoCmsModule\Core\Tests\Controllers\UserController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\Role;
use OctoCmsModule\Core\Entities\User;
use Tests\TestCase;

/**
 * Class IndexByRoleTest
 *
 * @package OctoCmsModule\Core\Tests\Controllers\UserController
 */
class IndexByRoleTest extends TestCase
{
    public function test_datatableIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        User::factory()
            ->has(Role::factory()->state(['name' => 'role-1']))
            ->count(3)
            ->create();

        User::factory()
            ->has(Role::factory()->state(['name' => 'random']))
            ->create();

        $response = $this->json('GET', route('core.users.index.by.role', ['role' => 'role-1']));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonCount(3);
    }
}
