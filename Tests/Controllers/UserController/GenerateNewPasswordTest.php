<?php

namespace OctoCmsModule\Core\Tests\Controllers\UserController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\Email;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GenerateNewPasswordTest
 *
 * @package OctoCmsModule\Core\Tests\Controllers\UserController
 */
class GenerateNewPasswordTest extends TestCase
{
    public function test_generateNewPassword()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var User $user */
        $user = User::factory()
            ->has(Email::factory(), 'defaultEmail')
            ->create();

        $response = $this->json('PUT', route('core.users.generate.new.password', ['id' => $user->id]));

        $response->assertStatus(Response::HTTP_OK);
    }

    public function test_generateNewPasswordMissingDefaultEmail()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var User $user */
        $user = User::factory()->create();

        $response = $this->json('PUT', route('core.users.generate.new.password', ['id' => $user->id]));

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
        $response->assertJsonFragment(['message' => 'User is missing default Email.']);
    }

}
