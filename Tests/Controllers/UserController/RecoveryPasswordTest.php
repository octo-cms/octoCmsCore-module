<?php

namespace OctoCmsModule\Core\Tests\Controllers\UserController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\Email;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Tests\TestCase;


class RecoveryPasswordTest extends TestCase
{

    public function createUser()
    {
        /** @var User $user */
        $user = User::factory()->create([
            'username' => 'accoral',
            'password' => 'password',
        ]);

        $email = Email::factory()->create(['email' => 'accoral@gmail.com']);
        $user->emails()->save($email);

        return $user;
    }


    public function dataProvider()
    {

        $providers = [];

        $data = [
            'username' => 'accoral',
            'email'    => 'accoral@gmail.com',
        ];

        $providers[] = [$data, Response::HTTP_OK];

        $fields = $data;
        unset($fields['username']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['email']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        $fields['email'] = "pippo";
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        $fields['username'] = "accoral2";
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        $fields['email'] = "pippo";
        $fields['username'] = "accoral2";
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        return $providers;
    }

    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_recovery_password_provider(array $fields, int $status)
    {
        $this->createUser();

        $response = $this->json(
            'POST',
            route('core.recovery.password'),
            $fields
        );

        $response->assertStatus($status);
    }


}
