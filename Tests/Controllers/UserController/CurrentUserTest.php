<?php

namespace OctoCmsModule\Core\Tests\Controllers\UserController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\CustomField;
use OctoCmsModule\Core\Entities\CustomFieldEntity;
use OctoCmsModule\Core\Entities\CustomFieldEntityString;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetUserTest
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Controllers\UserController
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class CurrentUserTest extends TestCase
{

    public function test_currentUser()
    {
        /**
         * Admin user
         *
         * @var User $user
         */
        $user = self::createAdminUser();

        Sanctum::actingAs($user);

        /**
         * Admin's user custom field entity
         *
         * @var CustomFieldEntity $customFieldEntity
         */
        $customFieldEntity = CustomFieldEntity::factory()
            ->for(CustomField::factory()->state(['name' => 'name']))
            ->for(CustomFieldEntityString::factory()->state(['value' => 'value']), 'valuable')
            ->create();

        $user->customFieldEntities()->save($customFieldEntity);

        $response = $this->json(
            'GET',
            route('core.users.current.user')
        );

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonFragment([
            'id' => 1,
            'roles' => [
                [
                    'id' => 1,
                    'name' => 'admin'
                ]
            ],
            'customFields' => [
                'name' => 'value',
            ],
        ]);
    }
}
