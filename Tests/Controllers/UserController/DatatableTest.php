<?php

namespace OctoCmsModule\Core\Tests\Controllers\UserController;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class DatatableTest
 *
 * @package OctoCmsModule\Core\Tests\Controllers\UserController
 */
class DatatableTest extends TestCase
{

    public function test_datatableIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        User::factory()->count(14)->create();

        $this->datatableTest('admin.datatables.core.users');
    }
}
