<?php

namespace OctoCmsModule\Core\Tests\Controllers\UserController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ShowTest
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Controllers\UserController
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class ShowTest extends TestCase
{


    public function test_show()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var User $user */
        $user = User::factory()->create();

        $response = $this->json(
            'GET',
            route('core.users.show', ['id' => $user->id]),
        );

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonStructure([
            'data' => [
                'id',
                'username',
                'active',
                'temp_password',
                'roles',
                'defaultEmail',
            ]
        ]);
    }

    public function test_showNotFound()
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'GET',
            route('core.users.show', ['id' => 100]),
        );

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

}
