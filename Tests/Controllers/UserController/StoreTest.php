<?php

namespace OctoCmsModule\Core\Tests\Controllers\UserController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class StoreTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Controllers\UserController
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class StoreTest extends TestCase
{

    /**
     * Name dataProvider
     *
     * @return array
     */
    public function dataProvider()
    {
        /** @var array $providers */
        $providers = [];

        /** @var array $data */
        $data = [
            'username'     => 'username',
            'roles'        => [['id' => 1]],
        ];

        $providers[] = [$data, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['username']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['roles']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        $fields['roles'] = [];
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['roles'][0]);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        return $providers;
    }

    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_store(array $fields, int $status)
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('core.users.store'),
            $fields
        );

        $response->assertStatus($status);
    }

    public function test_storeDuplicatedUsername()
    {
        Sanctum::actingAs(self::createAdminUser([
            'username' => 'usernameAlreadyPresent'
        ]));

        $response = $this->json(
            'POST',
            route('core.users.store'),
            [
                'username'     => 'usernameAlreadyPresent',
                'name'         => 'name',
                'roles'        => [['id' => 1]],
            ]
        );

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }
}
