<?php

namespace OctoCmsModule\Core\Tests\Controllers\UserController;

use Illuminate\Http\Response;
use OctoCmsModule\Core\Entities\Email;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class LoginTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Controllers\UserController
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class LoginTest extends TestCase
{


    public function test_login()
    {
        $user = User::factory()->create([
            'username' => 'username',
            'password' => 'password',
        ]);

        $response = $this->json(
            'POST',
            route('core.login', [
                'username' => 'username',
                'password' => 'password',
            ])
        );

        $response->assertStatus(Response::HTTP_OK);

        $this->assertAuthenticatedAs($user);

        $response = $this->json(
            'POST',
            route('core.login', [
                'username' => 'username',
                'password' => 'password-wrong',
            ])
        );

        $response->assertStatus(Response::HTTP_BAD_REQUEST);

        $response = $this->json(
            'POST',
            route('core.login', [
                'username' => 'username-wrong',
                'password' => 'password',
            ])
        );

        $response->assertStatus(Response::HTTP_NOT_FOUND);

    }

}
