<?php

namespace OctoCmsModule\Core\Tests\Controllers\UserController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class LogoutTest
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Controllers\UserController
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class LogoutTest extends TestCase
{

    /**
     * Name test_logout
     *
     * @return void
     */
    public function test_logout()
    {

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'GET',
            route('core.logout')
        );

        $response->assertStatus(Response::HTTP_OK);

        $this->assertGuest('web');

    }

}
