<?php

namespace OctoCmsModule\Core\Tests\Controllers\UserController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class UpdatePasswordTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Controllers\UserController
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class UpdatePasswordTest extends TestCase
{
    /**
     * Name dataProvider
     *
     * @return array
     */
    public function dataProvider(): array
    {
        $providers = [];

        $data = ['password' => 'MostComplexPassword83!'];

        $providers[] = [$data, Response::HTTP_OK];

        $fields = $data;
        unset($fields['password']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        $fields['password'] = '';
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        $fields['password'] = 'simple';
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        $fields['password'] = 'NOLOWERCASE';
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        $fields['password'] = 'nouppercase';
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        $fields['password'] = 'NoNumber';
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        $fields['password'] = 'NoSpecialChar123';
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        return $providers;
    }

    /**
     * Name test_updatePassword
     *
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     * @return void
     */
    public function test_updatePassword(array $fields, int $status)
    {
        /**
         * User
         *
         * @var User $user
         */
        $user = self::createAdminUser();

        Sanctum::actingAs($user);

        $response = $this->json(
            'POST',
            route('core.users.update.password'),
            $fields
        );

        $response->assertStatus($status);

        if ($status === Response::HTTP_OK) {
            $response->assertJsonFragment([
                'id'            => $user->id,
                'temp_password' => false,
            ]);
        }
    }
}
