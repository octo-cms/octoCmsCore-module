<?php

namespace OctoCmsModule\Core\Tests\Controllers\SettingController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class UpdateSettingsTest
 *
 * @package OctoCmsModule\Core\Tests\Controllers\SettingController
 */
class UpdateSettingsTest extends TestCase
{


    public function dataProvider()
    {
        $providers = [];

        $data = [
            [
                'name'  => 'name-1',
                'value' => 'value-1',
            ],
            [
                'name'  => 'name-2',
                'value' => 'value-2',
            ],
        ];

        $providers[] = [$data, Response::HTTP_OK];

        $fields = $data;
        unset($fields[0]['name']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields[0]['value']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        return $providers;
    }


    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_updateSettingsRequest(array $fields, int $status)
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'PUT',
            route('core.settings.update'),
            $fields
        );

        $response->assertStatus($status);
    }
}
