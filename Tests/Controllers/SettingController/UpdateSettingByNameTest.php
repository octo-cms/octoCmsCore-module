<?php

namespace OctoCmsModule\Core\Tests\Controllers\SettingController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Core\Entities\Setting;

/**
 * Class UpdateSettingByNameTest
 *
 * @package OctoCmsModule\Core\Tests\Controllers\SettingController
 */
class UpdateSettingByNameTest extends TestCase
{



    public function dataProvider()
    {
        $providers = [];

        $data = [
            'name'  => 'name',
            'value' => 'value',
        ];

        $providers[] = [$data, Response::HTTP_OK];

        $fields = $data;
        unset($fields['name']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['value']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $providers[] = [[], Response::HTTP_BAD_REQUEST];

        return $providers;
    }


    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_updateSettingByNameRequest(array $fields, int $status)
    {
        Sanctum::actingAs(self::createAdminUser());

        Setting::factory()->create([
            'name'  => 'name',
            'value' => 'value',
        ]);

        $response = $this->json(
            'POST',
            route('core.settings.update.setting.by.name'),
            $fields
        );

        $response->assertStatus($status);
    }


    public function test_updateSettingByName()
    {
        Sanctum::actingAs(self::createAdminUser());

        Setting::factory()->create([
            'name'  => 'scalar',
            'value' => 'value',
        ]);

        $value = 'valore nuovo';

        $data = [
            'name'  => 'scalar',
            'value' => $value,
        ];

        $data_db = [
            'name'  => 'scalar',
            'value' => serialize($value),
        ];


        $response = $this->json(
            'POST',
            route('core.settings.update.setting.by.name', $data)
        );

        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseHas('settings', $data_db);

        $response->assertJsonFragment($data);
    }

    public function test_updateSettingByNameNoValueInDB()
    {
        Sanctum::actingAs(self::createAdminUser());

        $data = [
            'name'  => 'scalar',
            'value' => 'valore nuovo',
        ];

        $response = $this->json(
            'POST',
            route('core.settings.update.setting.by.name', $data)
        );

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function test_updateSettingByNameAndEntity()
    {
        Sanctum::actingAs(self::createAdminUser());

        Setting::factory()->create([
            'name'  => 'name',
            'value' => [
                'entity-1' => 'value-1',
                'entity-2' => 'value-2',
            ],
        ]);

        $updatedValueIt = 'updated-value-1';

        $data = [
            'name'   => 'name',
            'entity' => 'entity-1',
            'value'  => $updatedValueIt,
        ];


        $response = $this->json(
            'POST',
            route('core.settings.update.setting.by.name', $data)
        );

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonFragment([
            'entity-1' => 'updated-value-1',
        ]);
    }
}
