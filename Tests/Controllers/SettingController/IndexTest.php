<?php

namespace OctoCmsModule\Core\Tests\Controllers\SettingController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Core\Entities\Setting;

/**
 * Class IndexTest
 *
 * @package OctoCmsModule\Core\Tests\Controllers\SettingController
 */
class IndexTest extends TestCase
{


    public function test_index()
    {
        Sanctum::actingAs(self::createAdminUser());

        Setting::factory()->create([
            'name' => 'scalar',
            'value' => 'value'
        ]);

        Setting::factory()->create([
            'name' => 'array',
            'value' => ['value']
        ]);

        Setting::factory()->create([
            'name' => 'bool_true',
            'value' => true
        ]);

        Setting::factory()->create([
            'name' => 'bool_false',
            'value' => false
        ]);


        $response = $this->json(
            'GET',
            route('core.settings.index')
        );

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonFragment([
            'scalar' => 'value'
        ]);

        $response->assertJsonFragment([
            'array' => ['value']
        ]);

        $response->assertJsonFragment([
            'bool_true' => true
        ]);
    }
}
