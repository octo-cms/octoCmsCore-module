<?php

namespace OctoCmsModule\Core\Tests\Controllers\CacheController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class FlushCacheTest
 *
 * @package OctoCmsModule\Core\Tests\Controllers\LeadController
 */
class FlushCacheTest extends TestCase
{


    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $data = [
            'tag' => 'tag',
        ];

        $providers[] = [$data, Response::HTTP_OK];

        $fields = $data;
        unset($fields['tag']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        $fields['tag'] = '';
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        $fields['tag'] = 1;
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        return $providers;
    }

    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_flushCacheRequest(array $fields, int $status)
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('core.cache.flush'),
            $fields
        );

        $response->assertStatus($status);
    }
}
