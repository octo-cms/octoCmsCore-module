<?php

namespace OctoCmsModule\Core\Tests\Controllers\RegistryController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class StoreTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Controllers\UserController
 * @author   Roberto La Rocca <roberto.larocca@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class StoreTest extends TestCase
{

    /**
     * Name dataProvider
     *
     * @return array
     */
    public function dataProvider()
    {
        /** @var array $providers */
        $providers = [];

        /** @var array $data */
        $data = [
            'code'    => 'code',
            'type'    => Registry::TYPE_PRIVATE,
            'user_id' => null,
        ];

        $providers[] = [$data, Response::HTTP_CREATED];

        $data = [
            'code'            => '',
            'type'            => Registry::TYPE_PRIVATE,
            'user_id'         => null,
            'privateRegistry' => [
                'name'       => 'roberto',
                'surname'    => 'La Rocca',
                'codfis'     => 'LRCRRT71E14F205A',
                'birth_date' => '1971-05-14'
            ]
        ];

        $providers[] = [$data, Response::HTTP_CREATED];

        $data = [
            'code'            => '',
            'type'            => Registry::TYPE_COMPANY,
            'user_id'         => null,
            'companyRegistry' => [
                'businessname' => 'roberto',
                'referral'     => 'roberto',
                'piva'         => '08382510967',
                'codfis'       => '08382510967',
                'web'          => null,
            ]
        ];

        $providers[] = [$data, Response::HTTP_CREATED];

        /** @var array $data */
        $data = [
            'code'    => 'code',
            'type'    => Registry::TYPE_COMPANY,
            'user_id' => null,
        ];

        $providers[] = [$data, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['type']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        $fields['type'] = "octo";
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        $fields['user_id'] = 2;
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $data = [
            'code'            => '',
            'type'            => Registry::TYPE_COMPANY,
            'user_id'         => null,
            'companyRegistry' => [
                'businessname' => 'roberto',
                'referral'     => 'roberto',
                'piva'         => '08382510961',
                'codfis'       => '08382510967',
                'web'          => null,
            ]
        ];

        $providers[] = [$data, Response::HTTP_BAD_REQUEST];

        $data = [
            'code'            => '',
            'type'            => Registry::TYPE_COMPANY,
            'user_id'         => null,
            'companyRegistry' => [
                'businessname' => 'roberto',
                'referral'     => 'roberto',
                'piva'         => '08382510961',
                'codfis'       => '08382510961',
                'web'          => null,
            ]
        ];

        $providers[] = [$data, Response::HTTP_BAD_REQUEST];

        $data = [
            'code'            => '',
            'type'            => Registry::TYPE_PRIVATE,
            'user_id'         => null,
            'privateRegistry' => [
                'name'       => 'roberto',
                'surname'    => 'La Rocca',
                'codfis'     => 'LRCRRT71E14F205B',
                'birth_date' => '1971-05-14'
            ]
        ];

        $providers[] = [$data, Response::HTTP_BAD_REQUEST];

        $data = [
            'code'            => '',
            'type'            => Registry::TYPE_PRIVATE,
            'user_id'         => null,
            'privateRegistry' => [
                'name'       => 'roberto',
                'surname'    => 'La Rocca',
                'codfis'     => 'LRCRRT71E14F205B',
                'birth_date' => '1971-15-14'
            ]
        ];

        $providers[] = [$data, Response::HTTP_BAD_REQUEST];



        return $providers;
    }

    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_store(array $fields, int $status)
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('core.registries.store'),
            $fields
        );

        $response->assertStatus($status);
    }

}
