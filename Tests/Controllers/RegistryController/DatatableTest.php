<?php

namespace OctoCmsModule\Core\Tests\Controllers\RegistryController;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class DatatableTest
 *
 * @package OctoCmsModule\Core\Tests\Controllers\RegistryController
 */
class DatatableTest extends TestCase
{



    public function test_datatableIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        Registry::factory()->count(15)->create();

        $this->datatableTest('admin.datatables.core.registries');
    }
}
