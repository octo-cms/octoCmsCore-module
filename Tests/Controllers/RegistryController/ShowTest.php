<?php

namespace OctoCmsModule\Core\Tests\Controllers\RegistryController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ShowTest
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Core\Tests\Controllers\RegistryController
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class ShowTest extends TestCase
{
    public function test_show()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var Registry $registry */
        $registry = Registry::factory()->create();

        $response = $this->json(
            'GET',
            route('core.registries.show', ['id' => $registry->id]),
        );

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonStructure([
            'data' => [
                'id',
                'code',
                'type',
                'privateRegistry',
                'companyRegistry',
                'emails',
                'phones',
                'addresses',
                'defaultEmail',
                'defaultPhone',
            ]
        ]);
    }

    public function test_showNotFound()
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'GET',
            route('core.registries.show', ['id' => 100]),
        );

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

}
