<?php

namespace OctoCmsModule\Core\Tests\Controllers\ProviderController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class StoreTest
 *
 * @package OctoCmsModule\Services\Tests\Controllers\ServiceController
 */
class StoreTest extends TestCase
{

    public function dataProvider()
    {
        $providers = [];

        $data = [
            'enabled'     => true,
            'name'        => 'label',
            'xls_mapping' => [],
            'slug'        => 'slug'
        ];

        $providers[] = [$data, Response::HTTP_CREATED];

        $fields = $data;
        unset($fields['enabled']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['name']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['slug']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['xls_mapping']);
        $providers[] = [$fields, Response::HTTP_CREATED];

        return $providers;
    }

    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_store(array $fields, int $status)
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'POST',
            route('core.providers.store'),
            $fields
        );

        $response->assertStatus($status);
    }

    /**
     * Name test_store_unique
     *
     * @return void
     */
    public function test_store_unique()
    {
        Sanctum::actingAs(self::createAdminUser());

        Provider::factory()->count(1)->create([ 'slug' => 'slug']);

        $fields = [
            'enabled'     => true,
            'name'       => 'name',
            'xls_mapping' => [],
            'slug'        => 'slug'
        ];

        $response = $this->json(
            'POST',
            route('core.providers.store'),
            $fields
        );

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

}
