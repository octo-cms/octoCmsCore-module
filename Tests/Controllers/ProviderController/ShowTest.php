<?php

namespace OctoCmsModule\Core\Tests\Controllers\ProviderController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ShowTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Controllers\ProviderController
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class ShowTest extends TestCase
{


    public function test_show()
    {
        Sanctum::actingAs(self::createAdminUser());

        /** @var Provider $provider */
        $provider = Provider::factory()->create();

        $response = $this->json(
            'GET',
            route('core.providers.show', ['id' => $provider->id]),
        );

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJsonStructure([
            'data' => [
                'id',
                'name',
                'slug',
                'record_price',
                'xls_mapping',
                'enabled'
            ]
        ]);
    }

    public function test_showNotFound()
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'GET',
            route('core.providers.show', ['id' => 100]),
        );

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

}
