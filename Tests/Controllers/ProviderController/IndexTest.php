<?php

namespace OctoCmsModule\Core\Tests\Controllers\ProviderController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ShowTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Controllers\ProviderController
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class IndexTest extends TestCase
{

    public function test_index()
    {
        Sanctum::actingAs(self::createAdminUser());

        Provider::factory()->count(5)->create();

        $response = $this->json(
            'GET',
            route('core.providers.index')
        );

        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonCount(5);
    }

}
