<?php

namespace OctoCmsModule\Core\Tests\Controllers\ProviderController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Core\Entities\Provider;

/**
 * Class UpdateTest
 *
 * @package OctoCmsModule\Core\Tests\Controllers\ProviderController
 */
class UpdateTest extends TestCase
{

    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $data = [
            'id'          => 1,
            'enabled'     => true,
            'slug'        => 'slug',
            'name'        => 'name',
            'xls_mapping' => [],
        ];

        $providers[] = [$data, Response::HTTP_OK];

        $fields = $data;
        unset($fields['id']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['enabled']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['slug']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['name']);
        $providers[] = [$fields, Response::HTTP_BAD_REQUEST];

        $fields = $data;
        unset($fields['xls_mapping']);
        $providers[] = [$fields, Response::HTTP_OK];

        $fields = $data;
        $providers[] = [$fields, Response::HTTP_OK];

        return $providers;
    }

    /**
     * @param array $fields
     * @param int   $status
     *
     * @dataProvider dataProvider
     */
    public function test_update(array $fields, int $status)
    {
        /** @var Provider $provider */
        $provider = Provider::factory()->create();

        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'PUT',
            route('core.providers.update', ['id' => $provider->id]),
            $fields
        );

        $response->assertStatus($status);
    }

    /**
     * Name test_update_unique
     *
     * @return void
     */
    public function test_update_unique()
    {
        Sanctum::actingAs(self::createAdminUser());

        $provider = Provider::factory()->create(['slug' => 'slug']);

        $fields = [
            'id'          => $provider->id,
            'enabled'     => true,
            'name'        => 'name',
            'xls_mapping' => [],
            'slug'        => 'slug'
        ];

        $response = $this->json(
            'PUT',
            route('core.providers.update', ['id' => $provider->id]),
            $fields
        );

        $response->assertStatus(Response::HTTP_OK);
    }
}
