<?php

namespace OctoCmsModule\Core\Tests\Controllers\ProviderController;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Core\Entities\Provider;

/**
 * Class DatatableTest
 *
 * @package OctoCmsModule\Core\Tests\Controllers\ProviderController
 */
class DatatableTest extends TestCase
{


    public function test_datatableIndex()
    {
        Sanctum::actingAs(self::createAdminUser());

        Provider::factory()->count(15)->create();

        $this->datatableTest('admin.datatables.core.providers');
    }
}
