<?php

namespace OctoCmsModule\Core\Tests\Controllers\RoleController;

use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class IndexTest
 *
 * @package OctoCmsModule\Core\Tests\Controllers\RoleController
 */
class IndexTest extends TestCase
{


    public function test_index()
    {
        Sanctum::actingAs(self::createAdminUser());

        $response = $this->json(
            'GET',
            route('core.roles.index')
        );

        $response->assertStatus(Response::HTTP_OK);
    }
}
