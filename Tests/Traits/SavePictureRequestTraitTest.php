<?php

namespace OctoCmsModule\Core\Tests\Traits;

use OctoCmsModule\Core\Traits\SavePictureRequestTrait;
use OctoCmsModule\Core\Tests\TestCase;

/**
 *
 * Class GetCustomEntitiesTest
 * @package OctoCmsModule\Core\Tests\Utils\LivewireUtils
 */
class SavePictureRequestTraitTest extends TestCase
{

    public function test_getPictureRules()
    {
        $class = new class {
            use SavePictureRequestTrait;
        };

        $this->assertEquals([
            'pictures'                              => 'sometimes|array',
            'pictures.*.id'                         => 'sometimes|nullable',
            'pictures.*.src'                        => 'sometimes',
            'pictures.*.tag'                        => 'sometimes',
            'pictures.*.webp'                       => 'sometimes',
            'pictures.*.newImage'                   => 'sometimes|array',
            'pictures.*.newImage.src'               => 'sometimes',
            'pictures.*.newImage.height'            => 'sometimes',
            'pictures.*.newImage.width'             => 'sometimes',
            'pictures.*.pictureLangs'               => 'sometimes|array',
            'pictures.*.pictureLangs.*.lang'        => 'sometimes',
            'pictures.*.pictureLangs.*.alt'         => 'sometimes',
            'pictures.*.pictureLangs.*.caption'     => 'sometimes',
            'pictures.*.pictureLangs.*.description' => 'sometimes',
            'pictures.*.pictureLangs.*.title'       => 'sometimes',
            'pictures.*.pictureLangs.*.picture_id'  => 'sometimes',
        ], $class->getPictureRules());
    }

    public function test_getPictureRulesWithPrefix()
    {
        $class = new class {
            use SavePictureRequestTrait;
        };

        $this->assertEquals([
            '*.pictures'                              => 'sometimes|array',
            '*.pictures.*.id'                         => 'sometimes|nullable',
            '*.pictures.*.src'                        => 'sometimes',
            '*.pictures.*.tag'                        => 'sometimes',
            '*.pictures.*.webp'                       => 'sometimes',
            '*.pictures.*.newImage'                   => 'sometimes|array',
            '*.pictures.*.newImage.src'               => 'sometimes',
            '*.pictures.*.newImage.height'            => 'sometimes',
            '*.pictures.*.newImage.width'             => 'sometimes',
            '*.pictures.*.pictureLangs'               => 'sometimes|array',
            '*.pictures.*.pictureLangs.*.lang'        => 'sometimes',
            '*.pictures.*.pictureLangs.*.alt'         => 'sometimes',
            '*.pictures.*.pictureLangs.*.caption'     => 'sometimes',
            '*.pictures.*.pictureLangs.*.description' => 'sometimes',
            '*.pictures.*.pictureLangs.*.title'       => 'sometimes',
            '*.pictures.*.pictureLangs.*.picture_id'  => 'sometimes',
        ], $class->getPictureRules('*.'));
    }
}
