<?php

namespace OctoCmsModule\Core\Tests\Utils;

use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Core\Utils\PriceParser;

/**
 * Class PriceParserTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Utils
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class PriceParserTest extends TestCase
{
    /**
     * Name dataProviderParsePrice
     *
     * @return array
     */
    public function dataProviderParsePrice(): array
    {
        $providers = [];

        $providers[] = ['125', 125];
        $providers[] = ['9,6', 9.6];
        $providers[] = ['125.00', 125];
        $providers[] = ['125,00', 125];
        $providers[] = ['125,10', 125.1];
        $providers[] = ['2.125,00', 2125];
        $providers[] = ['22.125,10', 22125.1];
        $providers[] = ['dd25,00', null];

        return $providers;
    }

    /**
     * @dataProvider dataProviderParsePrice
     *
     * @param $price
     * @param $result
     */
    public function test_parsePrice($price, $result)
    {
        $this->assertEquals(
            $result,
            PriceParser::parse($price)
        );

        if ($result) {
            $this->assertIsNumeric($result);
        }
    }
}
