<?php

namespace OctoCmsModule\Core\Tests\Mocks;

use OctoCmsModule\Core\Interfaces\PictureServiceInterface;

/**
 * Class PictureServiceMock
 *
 * @package OctoCmsModule\Core\Tests\Mocks
 */
class PictureServiceMock implements PictureServiceInterface
{

    /**
     * @param array  $pictureFields
     * @param string $path
     *
     * @return array|string[]|null
     */
    public function uploadPicture(array $pictureFields, string $path): ?array
    {
        return [
            'src'  => 'src',
            'webp' => 'webp',
        ];
    }

    /**
     * @param mixed $entity
     * @param array $content
     * @return bool
     */
    public function savePicturesEntity($entity, $content): bool
    {
        return true;
    }
}
