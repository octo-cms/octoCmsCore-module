<?php

namespace OctoCmsModule\Core\Tests\Mocks;

use Illuminate\Http\UploadedFile;
use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Interfaces\ProviderServiceInterface;

/**
 * Class ProviderServiceMock
 *
 * @package OctoCmsModule\Core\Tests\Mocks
 */
class ProviderServiceMock implements ProviderServiceInterface
{
    /**
     * @param Provider $provider
     * @param array    $fields
     *
     * @return Provider
     */
    public function saveProvider(Provider $provider, array $fields): Provider
    {
        return $provider;
    }

    /**
     * Name uploadImport
     * @param UploadedFile $file
     * @param array        $options
     *
     * @return ProviderImport
     */
    public function uploadImport(UploadedFile $file, array $options = []): ProviderImport
    {
        return ProviderImport::factory()->create();
    }

    /**
     * Name parseProviderImport
     * @param ProviderImport $providerImport
     * @param string         $filePath
     *
     * @return mixed|void
     */
    public function parseProviderImport(ProviderImport $providerImport, string $filePath)
    {
        // TODO: Implement parseProviderImport() method.
    }

    /**
     * Name syncProviderImport
     * @param ProviderImport $providerImport
     *
     * @return mixed|void
     */
    public function syncProviderImport(ProviderImport $providerImport)
    {
        // TODO: Implement syncProviderImport() method.
    }

    /**
     * Name syncProviderImportWithRegistry
     * @param ProviderImport $providerImport
     *
     * @return mixed|void
     */
    public function syncProviderImportWithRegistry(ProviderImport $providerImport)
    {
        // TODO: Implement syncProviderImportWithRegistry() method.
    }
}
