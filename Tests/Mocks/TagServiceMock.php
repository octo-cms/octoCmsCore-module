<?php

namespace OctoCmsModule\Core\Tests\Mocks;

use OctoCmsModule\Core\Entities\Tag;
use OctoCmsModule\Core\Interfaces\TagServiceInterface;

/**
 * Class TagServiceMock
 *
 * @package OctoCmsModule\Core\Tests\Mocks
 */
class TagServiceMock implements TagServiceInterface
{
    /**
     * @param Tag    $tag
     * @param string $group
     *
     * @return Tag
     */
    public function saveTag(Tag $tag, string $group): Tag
    {
        return $tag;
    }
}
