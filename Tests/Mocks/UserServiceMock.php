<?php

namespace OctoCmsModule\Core\Tests\Mocks;

use Illuminate\Support\Arr;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Interfaces\UserServiceInterface;

/**
 * Class UserServiceMock
 *
 * @package OctoCmsModule\Core\Tests\Mocks
 */
class UserServiceMock implements UserServiceInterface
{

    /**
     * @param User  $user
     * @param array $fields
     *
     * @return User
     */
    public function saveUser(User $user, array $fields): User
    {
        return User::factory()->create(Arr::only($fields, ['username', 'active']));
    }

    /**
     * @param User $user
     *
     * @return User
     */
    public function generateNewPassword(User $user): User
    {
        return $user;
    }

    /**
     * Name saveUserEventLog
     * @param User   $user
     * @param string $type
     * @param array  $fields
     *
     * @return void
     */
    public function saveUserEventLog(User $user, string $type, array $fields = []): void
    {
    }

    /**
     * Name saveCustomFields
     * @param User  $user
     * @param array $customFields
     *
     * @return void
     */
    public function saveCustomFields(User $user, array $customFields)
    {
        // TODO: Implement saveCustomFields() method.
    }
}
