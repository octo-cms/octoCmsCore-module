<?php

namespace OctoCmsModule\Core\Tests\Mocks;

use OctoCmsModule\Core\Constants\SettingNameConst;
use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Core\Interfaces\SettingServiceInterface;

/**
 * Class SettingServiceMock
 *
 * @package OctoCmsModule\Core\Tests\Mocks
 */
class SettingServiceMock implements SettingServiceInterface
{
    /**
     * @inheritDoc
     */
    public function getSettingByName(string $name, bool $enableCache = true)
    {
        if ($name === SettingNameConst::LANGUAGES) {
            return ['it', 'en'];
        }

        return optional(Setting::whereName($name)->first())->value;
    }

    /**
     * @param array $fields
     */
    public function saveSettings(array $fields)
    {
        // TODO: Implement saveSettings() method.
    }
}
