<?php

namespace OctoCmsModule\Core\Tests\Mocks;

use OctoCmsModule\Core\DTO\ContactFormDataDTO;
use OctoCmsModule\Core\Interfaces\ActiveCampaignServiceInterface;

/**
 * Class ActiveCampaignServiceMock
 *
 * @package OctoCmsModule\Core\Tests\Mocks
 */
class ActiveCampaignServiceMock implements ActiveCampaignServiceInterface
{

    /**
     * Name createContact
     * @param ContactFormDataDTO $contactFormDataDTO
     *
     * @return array|null
     */
    public function createContact(ContactFormDataDTO $contactFormDataDTO): ?array
    {
        return [];
    }

    /**
     * Name subscribeContactToList
     * @param array|null $contact
     *
     * @return bool
     */
    public function subscribeContactToList(?array $contact): bool
    {
        return true;
    }
}
