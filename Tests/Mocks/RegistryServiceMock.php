<?php

namespace OctoCmsModule\Core\Tests\Mocks;

use Illuminate\Support\Arr;
use OctoCmsModule\Core\DTO\ContactFormDataDTO;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Interfaces\RegistryServiceInterface;

/**
 * Class RegistryServiceMock
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Mocks
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class RegistryServiceMock implements RegistryServiceInterface
{

    /**
     * Name sendEmailToAdmin
     *
     * @param ContactFormDataDTO $contactFormDataDTO
     *
     * @return bool
     */
    public function sendEmailToAdmin(ContactFormDataDTO $contactFormDataDTO): bool
    {
        return true;
    }

    /**
     * Name saveRegistry
     *
     * @param Registry $registry
     * @param array    $fields
     *
     * @return Registry
     */
    public function saveRegistry(Registry $registry, array $fields): Registry
    {
        return Registry::factory()->create(Arr::only($fields, ['code', 'type']));
    }

    /**
     * Name updatePersonalData
     *
     * @param Registry $registry
     * @param array    $fields
     *
     * @return Registry
     */
    public function updatePersonalData(Registry $registry, array $fields): Registry
    {
        return Registry::factory()->create(Arr::only($fields, ['code', 'type']));
    }

}
