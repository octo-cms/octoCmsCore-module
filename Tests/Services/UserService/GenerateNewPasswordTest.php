<?php

namespace OctoCmsModule\Core\Tests\Services\UserService;

use Exception;
use OctoCmsModule\Core\Entities\Email;
use OctoCmsModule\Core\Notifications\NewPasswordNotification;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Support\Facades\Notification;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Services\UserService;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GenerateNewPasswordTest
 *
 * @package OctoCmsModule\Core\Tests\Services\UserService
 */
class GenerateNewPasswordTest extends TestCase
{


    /** @var Hasher */
    protected $hasher;

    /**
     * @throws Exception
     */
    public function test_generateNewPassword()
    {
        Notification::fake();

        /** @var string $oldPassword */
        $oldPassword = 'old-password';

        /** @var UserService $userService */
        $userService = new UserService();

        /**
         * User
         *
         * @var User $user
         */
        $user = User::factory()
            ->has(Email::factory()->count(1))
            ->create(['password' => $oldPassword]);

        $userService->generateNewPassword($user);

        Notification::assertSentTo($user->defaultEmail, NewPasswordNotification::class);
    }
}
