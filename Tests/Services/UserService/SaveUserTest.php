<?php

namespace OctoCmsModule\Core\Tests\Services\UserService;

use Exception;
use OctoCmsModule\Core\Notifications\UserRegistrationNotification;
use Illuminate\Support\Facades\Notification;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Services\UserService;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class RegisterUserTest
 *
 * @package OctoCmsModule\Core\Tests\Services\UserService
 */
class SaveUserTest extends TestCase
{

    /**
     * @throws Exception
     */
    public function test_saveNewUser()
    {
        Notification::fake();

        /** @var UserService $userService */
        $userService = new UserService();

        /** @var array $fields */
        $fields = [
            'username' => 'username',
            'emails'   => [
                [ 'email' => 'defaultemail@email.it', 'label' => 'default' ]
            ],
        ];

        /** @var User $user */
        $user = $userService->saveUser(new User(), $fields);

        $this->assertDatabaseHas('users', [
            'id'       => 1,
            'username' => $fields['username'],
        ]);

        $this->assertDatabaseHas('emails', [
            'emailable_id'   => 1,
            'emailable_type' => 'User',
            'email'          => 'defaultemail@email.it',
            'default'        => true,
            'label'          => 'default'
        ]);

        Notification::assertSentTo($user->defaultEmail, UserRegistrationNotification::class);
    }

    /**
     * @throws Exception
     */
    public function test_updateUser()
    {
        Notification::fake();

        /** @var UserService $userService */
        $userService = new UserService();

        /** @var User $user */
        $user = User::factory()->create();

        $user = User::find($user->id);

        /** @var array $fields */
        $fields = [
            'username'     => 'username2',
            'emails'   => [
                [ 'email' => 'defaultemail2@email.it', 'label' => 'default' ]
            ],
        ];

        /** @var User $user */
        $user = $userService->saveUser($user, $fields);

        $this->assertDatabaseHas('users', [
            'id'       => $user->id,
            'username' => 'username2',
        ]);

        $this->assertDatabaseHas('emails', [
            'emailable_id'   => $user->id,
            'emailable_type' => 'User',
            'email'          => 'defaultemail2@email.it',
            'default'        => true,
            'label'          => 'default'
        ]);

        Notification::assertNothingSent();
    }
}
