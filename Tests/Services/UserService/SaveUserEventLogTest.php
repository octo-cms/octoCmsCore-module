<?php

namespace OctoCmsModule\Core\Tests\Services\UserService;

use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Services\UserService;
use Tests\TestCase;

/**
 * Class SaveUserEventLogTest
 *
 * @package Tests\Core\Services\UserService
 */
class SaveUserEventLogTest extends TestCase
{

    public function test_saveUserEventLog()
    {
        /**
         * Request data
         *
         * @var array $data
         */
        $data = [
            'data'          => ['data-1', 'data-2'],
            'ipAddress'    => '127.0.0.1',
        ];

        /**
         *  Custom UserService
         *
         * @var UserService $userService
         */
        $userService = new UserService();

        /**
         * User to be updated
         *
         * @var User $user
         */
        $user = User::factory()->create();

        $userService->saveUserEventLog($user, 'type', $data);

        $this->assertDatabaseHas('user_event_logs', [
            'id'            => 1,
            'user_id'       => $user->id,
            'type'          => 'type',
            'ip_address'    => '127.0.0.1',
            'data'          => serialize(['data-1', 'data-2']),
        ]);
    }
}
