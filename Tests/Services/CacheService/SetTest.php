<?php

namespace OctoCmsModule\Core\Tests\Services\CacheService;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use OctoCmsModule\Core\Services\CacheService;
use Psr\SimpleCache\InvalidArgumentException;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class SetTest
 *
 * @package OctoCmsModule\Core\Tests\Services\CacheService
 */
class SetTest extends TestCase
{


    /**
     * @throws InvalidArgumentException
     */
    public function test_setCache()
    {
        /** @var string $tag */
        $tag = 'tag';

        /** @var int $key */
        $key = 1;

        /** @var array $value */
        $value = ['key' => 'value'];

        /** @var CacheService $cacheService */
        $cacheService = new CacheService();

        $this->assertTrue($cacheService->set($tag, $key, $value));

        $this->assertEquals(
            serialize($value),
            Cache::tags([$tag])->get($key, [])
        );
    }

    /**
     * @throws InvalidArgumentException
     */
    public function test_setCacheDisabled()
    {
        Config::set('octo-cms.cache_enabled', false);

        /** @var CacheService $cacheService */
        $cacheService = new CacheService();

        $this->assertNull($cacheService->set('tag', 'key', 'value'));
    }
}
