<?php

namespace OctoCmsModule\Core\Tests\Services\CacheService;

use OctoCmsModule\Core\Services\CacheService;
use Psr\SimpleCache\InvalidArgumentException;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class FlushCacheByTagTest
 *
 * @package OctoCmsModule\Core\Tests\Services\CacheService
 */
class FlushCacheByTagTest extends TestCase
{


    /**
     * @throws InvalidArgumentException
     */
    public function test_flushCache()
    {
        /** @var string $tag */
        $tag = 'tag';

        /** @var int $key */
        $key = 1;

        CacheService::set($tag, $key, ['key' => 'value']);

        $this->assertTrue(CacheService::flushCacheByTag($tag));

        $this->assertEquals(
            null,
            CacheService::get($tag, $key)
        );
    }
}
