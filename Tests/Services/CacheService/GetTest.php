<?php

namespace OctoCmsModule\Core\Tests\Services\CacheService;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use OctoCmsModule\Core\Services\CacheService;
use Psr\SimpleCache\InvalidArgumentException;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetTest
 *
 * @package OctoCmsModule\Core\Tests\Services\CacheService
 */
class GetTest extends TestCase
{


    /**
     * @throws InvalidArgumentException
     */
    public function test_getCache()
    {
        /** @var string $tag */
        $tag = 'tag';

        /** @var int $key */
        $key = 1;

        /** @var array $value */
        $value = ['key' => 'value'];

        Cache::tags([$tag])->set($key, serialize($value));

        /** @var CacheService $cacheService */
        $cacheService = new CacheService();

        $this->assertEquals(
            $value,
            $cacheService->get($tag, $key)
        );
    }

    public function test_getCacheNullNotFound()
    {
        /** @var CacheService $cacheService */
        $cacheService = new CacheService();

        $this->assertNull($cacheService->get('tag', 'key'));

        Config::set('octo-cms.cache_enabled', false);
        $this->assertNull($cacheService->get('tag', 'key'));
    }

    public function test_getCacheDisabled() {
        /** @var CacheService $cacheService */
        $cacheService = new CacheService();

        Config::set('octo-cms.cache_enabled', false);

        $this->assertNull($cacheService->get('tag', 'key'));
    }
}
