<?php

namespace OctoCmsModule\Core\Tests\Services\TagService;

use OctoCmsModule\Core\Entities\Tag;
use OctoCmsModule\Core\Services\TagService;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class SaveTagTest
 *
 * @package OctoCmsModule\Core\Tests\Services\TagService
 */
class SaveTagTest extends TestCase
{


    public function test_saveTag()
    {
        /** @var Tag $tag */
        $tag = Tag::factory()->create([
            'name' => 'tag',
        ]);

        /** @var string $group */
        $group = 'group';

        /** @var TagService $tagService */
        $tagService = new TagService();

        /** @var Tag $savedTag */
        $savedTag = $tagService->saveTag($tag, $group);

        $this->assertDatabaseHas('tagging_tags', [
            'id'   => 1,
            'name' => ucwords($savedTag->name),
            'slug' => $savedTag->slug,
        ]);

        $this->assertDatabaseHas('tagging_tag_groups', [
            'id'   => 1,
            'name' => $group,
            'slug' => $group,
        ]);

        $this->assertEquals($savedTag->group->name, $group);
    }
}
