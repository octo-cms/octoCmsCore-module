<?php

namespace OctoCmsModule\Core\Tests\Services\DatatableService;

use OctoCmsModule\Core\DTO\DatatableDTO;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Services\DatatableService;
use OctoCmsModule\Core\Tests\TestCase;

/**
 *
 * Class GetSettingByNameTest
 *
 * @package OctoCmsModule\Core\Tests\Services\SettingService
 */
class GetDatatableDTOTest extends TestCase
{


    /**
     * @return array
     */
    public function dataProvider()
    {
        $providers = [];

        $providers[] = [20, 10, 1, 10];
        $providers[] = [20, 10, 2, 10];
        $providers[] = [25, 10, 3, 5];
        $providers[] = [8, 10, 1, 8];

        return $providers;
    }

    /**
     * @param $numEntities
     * @param $rowsInPage
     * @param $currentPage
     * @param $rowResult
     * @dataProvider dataProvider
     */
    public function test_getDatatableDTO($numEntities, $rowsInPage, $currentPage, $rowResult)
    {

        User::factory()->count($numEntities)->create();

        $service = new DatatableService();

        $result = $service->getDatatableDTO([
            'rowsInPage'  => $rowsInPage,
            'currentPage' => $currentPage,
        ],
            User::query()
        );

        $this->assertInstanceOf(
            DatatableDTO::class,
            $result
        );

        $this->assertEquals(
            $numEntities,
            $result->total
        );

        $this->assertEquals(
            $currentPage,
            $result->currentPage
        );

        $this->assertEquals(
            $rowsInPage,
            $result->rowsInPage
        );


        $this->assertNull(
            $result->collection
        );

        $this->assertCount(
            $rowResult,
            $result->builder->get()
        );


    }
}
