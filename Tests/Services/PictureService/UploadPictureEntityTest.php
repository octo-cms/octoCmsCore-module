<?php

namespace OctoCmsModule\Core\Tests\Services\PictureService;

use Illuminate\Support\Collection;
use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Services\PictureService;
use OctoCmsModule\Core\Tests\TestCase;
use Illuminate\Support\Facades\Storage;
use Throwable;

/**
 * Class UploadPictureEntityTest
 *
 * @package OctoCmsModule\Core\Tests\Services\PictureService
 */
class UploadPictureEntityTest extends TestCase
{


    protected const IMG_BASE_64 = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAG" .
    "XRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyppVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWd" .
    "pbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iI" .
    "Hg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTMyIDc5LjE1OTI4NCwgMjAxNi8wNC8xOS0xMzoxMzo0MCAgICAgICAgIj4gPHJ" .
    "kZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwd" .
    "GlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDo" .
    "vL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZ" .
    "XNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUuNSAoTWFjaW50b3NoKSIgeG1wTU06SW5" .
    "zdGFuY2VJRD0ieG1wLmlpZDoxQkRGODNCQUJBMzcxMUU3OUFDRjk0Qjc5NEFEODM2MCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRp" .
    "ZDoxQkRGODNCQkJBMzcxMUU3OUFDRjk0Qjc5NEFEODM2MCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4b" .
    "XAuaWlkOkYxQkZDNzc1QkEzNTExRTc5QUNGOTRCNzk0QUQ4MzYwIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkYxQkZDNzc2Qk" .
    "EzNTExRTc5QUNGOTRCNzk0QUQ4MzYwIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWN" .
    "rZXQgZW5kPSJyIj8++3MIAwAAAU1JREFUeNpi/PjxIxsDA0M3EMcAsRADceA9EC8G4lIWINHFzMyWx8LCzsDIyEiU7v///wv++fMz" .
    "7+/fX98ZgS54y87OK0SsZiRDGH7+/PyOCeRsUjWDAFSPEBMpmi5fvsiwb99uFDGiDfjw4T3Djh1bGbS0dEg34N+/fwxr165ksLGxZ" .
    "5CQkCTdgEOH9jOwsbEzWFhYYcihGHDq1HGGAwf2oih4/PgRw5kzpxgCA0OwRjOKAdraugzXrl2BG/Lz50+GdetWMvj6BjLw8PBijw" .
    "1gOvjPwcEHF/j69QvDwoVzwYH17t1bBnZ2dgZvb3+smn/8+IQZBtzcPAzx8clglzx//ozBzc0Lf3pAdwGyS75+/cogJiaOUzPIBaC8" .
    "8A6YLDFSI8glIIwvKYP0grywGJgxYAJE5wOQHiCYA3JBGTBXMQJxLJAtSGJ2rgUIMACrMYdlnbN45AAAAABJRU5ErkJggg==";

    /**
     * @throws Throwable
     */
    public function test_uploadMediaEntity()
    {
        Storage::fake('gcs');

        /** @var User $user */
        $user = User::factory()->create();

        /** @var PictureService */
        $pictureService = new PictureService();

        $this->assertInstanceOf(
            Picture::class,
            $this->invokeMethod(
                $pictureService,
                'uploadPictureEntity',
                [
                    'entity'         => $user,
                    'pictureFields' => ['src' => self::IMG_BASE_64],
                    'options'        => [ 'removePrevImage' => true ]
                ]
            )
        );

        $user->loadPictures();

        $this->assertInstanceOf(Collection::class, $user->pictures()->get());
        $this->assertCount(1, $user->pictures);
        $this->assertInstanceOf(Picture::class, $user->pictures()->first());

    }

    /**
     * @throws Throwable
     */
    public function test_uploadMediaEntityCatch()
    {
        Storage::fake('gcs');

        /** @var PictureService */
        $pictureService = new PictureService();

        $this->assertNull(
            $this->invokeMethod(
                $pictureService,
                'uploadPictureEntity',
                [
                    'entity'         => null,
                    'pictureFields' => ['src' => self::IMG_BASE_64],
                    'options'        => [ 'removePrevImage' => true ]
                ]
            )
        );

    }
}
