<?php

namespace OctoCmsModule\Core\Tests\Services\PictureService;

use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Core\Entities\PictureLang;
use OctoCmsModule\Core\Services\PictureService;
use ReflectionException;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class SavePictureLangsTest
 *
 * @package OctoCmsModule\Core\Tests\Services\PictureService
 */
class SavePictureLangsTest extends TestCase
{


    /**
     * @throws ReflectionException
     */
    public function test_newPictureLangs()
    {
        /** @var Picture $picture */
        $picture = Picture::factory()->create();

        /** @var array $pictureLangs */
        $pictureLangs = [
            [
                'lang'        => 'it',
                'alt'         => 'alt it',
                'title'       => 'title it',
                'caption'     => 'caption it',
                'description' => 'description it',
            ],
            [
                'lang'        => 'en',
                'alt'         => 'alt en',
                'title'       => 'title en',
                'caption'     => 'caption en',
                'description' => 'description en',
            ],
        ];

        /** @var PictureService */
        $pictureService = new PictureService();

        $this->invokeMethod(
            $pictureService,
            'savePictureLangs',
            ['picture' => $picture, 'pictureLangs' => $pictureLangs]
        );

        foreach ($pictureLangs as $pictureLang) {
            $this->assertDatabaseHas('picture_langs', [
                'picture_id'  => $picture->id,
                'lang'        => $pictureLang['lang'],
                'alt'         => $pictureLang['alt'],
                'title'       => $pictureLang['title'],
                'caption'     => $pictureLang['caption'],
                'description' => $pictureLang['description'],
            ]);
        }
    }

    public function test_updatePictureLangs()
    {
        /** @var Picture $picture */
        $picture = Picture::factory()
            ->has(
                PictureLang::factory()->state([
                    'alt'         => 'alt it',
                    'title'       => 'title it',
                    'caption'     => 'caption it',
                    'description' => 'description it',
                ])
            )->has(
                PictureLang::factory()->state([
                    'alt'         => 'alt en',
                    'title'       => 'title en',
                    'caption'     => 'caption en',
                    'description' => 'description en',
                ])
            )->create();

        /** @var array $pictureLangs */
        $pictureLangs = [
            [
                'lang'        => 'it',
                'alt'         => 'nuovo alt it',
                'title'       => 'nuovo title it',
                'caption'     => 'nuovo caption it',
                'description' => 'nuovo description it',
            ],
            [
                'lang'        => 'en',
                'alt'         => 'new alt en',
                'title'       => 'new title en',
                'caption'     => 'new caption en',
                'description' => 'new description en',
            ],
        ];

        /** @var PictureService */
        $pictureService = new PictureService();

        $this->invokeMethod(
            $pictureService,
            'savePictureLangs',
            ['picture' => $picture, 'pictureLangs' => $pictureLangs]
        );

        foreach ($pictureLangs as $pictureLang) {
            $this->assertDatabaseHas('picture_langs', [
                'picture_id'  => $picture->id,
                'lang'        => $pictureLang['lang'],
                'alt'         => $pictureLang['alt'],
                'title'       => $pictureLang['title'],
                'caption'     => $pictureLang['caption'],
                'description' => $pictureLang['description'],
            ]);
        }
    }
}
