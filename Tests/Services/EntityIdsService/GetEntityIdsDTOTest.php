<?php

namespace OctoCmsModule\Core\Tests\Services\EntityIdsService;

use Illuminate\Database\Eloquent\Builder;
use OctoCmsModule\Core\DTO\EntityIdsDTO;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Services\EntityIdsService;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetEntityIdsDTOTest
 *
 * @package OctoCmsModule\Core\Tests\Services\EntityIdsService
 */
class GetEntityIdsDTOTest extends TestCase
{


    public function test_GetEntityIdsDTO()
    {
        $fields = [
            'orderBy'     => 'created_at',
            'rowsInPage'  => 5,
            'currentPage' => 2,
        ];

        User::factory()->count(2)->create();

        $service = new  EntityIdsService();

        /** @var EntityIdsDTO $entityIdsDTO */
        $entityIdsDTO = $service->getEntityIdsDTO(User::query(), $fields);


        $this->assertEquals($fields['rowsInPage'], $entityIdsDTO->rowsInPage);
        $this->assertEquals($fields['currentPage'], $entityIdsDTO->currentPage);
        $this->assertEquals(2, $entityIdsDTO->total);
        $this->assertInstanceOf(Builder::class, $entityIdsDTO->builder);
    }
}
