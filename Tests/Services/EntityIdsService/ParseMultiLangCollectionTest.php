<?php

namespace OctoCmsModule\Core\Tests\Services\EntityIdsService;

use OctoCmsModule\Core\Services\EntityIdsService;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ParseMultiLangCollectionTest
 *
 * @package OctoCmsModule\Core\Tests\Services\EntityIdsService
 */
class ParseMultiLangCollectionTest extends TestCase
{


    public function test_simpleParse()
    {

        $service = new EntityIdsService();

        $collection = collect([
            [
                'id'          => 1,
                'langs'       => [
                    [
                        'lang'  => 'it',
                        'label' => 'italiano-1',
                        'description' => 'description-1',
                    ],
                    [
                        'lang'  => 'es',
                        'label' => 'español-1',
                        'description' => 'description-1',
                    ],
                ],

            ],
            [
                'id'          => 2,
                'langs'       => [
                    [
                        'lang'  => 'it',
                        'label' => 'italiano-2',
                        'description' => 'description-2',
                    ],
                    [
                        'lang'  => 'es',
                        'label' => 'español-2',
                        'description' => 'description-2',
                    ],
                ],
            ],
        ]);

        $result = $service->parseMultiLangCollection(
            $collection,
            'langs',
            'id',
            'label',
            'description'
        );

        $this->assertEquals(
            ['id' => 1, 'name' => 'italiano-1', 'description' => 'description-1'],
            $result[1]
        );

        $this->assertEquals(
            ['id' => 2, 'name' => 'italiano-2', 'description' => 'description-2'],
            $result[2]
        );
    }
}
