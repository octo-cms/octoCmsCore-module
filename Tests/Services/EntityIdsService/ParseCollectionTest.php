<?php

namespace OctoCmsModule\Core\Tests\Services\EntityIdsService;

use OctoCmsModule\Core\Services\EntityIdsService;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class ParseCollectionTest
 *
 * @package OctoCmsModule\Core\Tests\Services\EntityIdsService
 */
class ParseCollectionTest extends TestCase
{


    public function test_simpleParse()
    {

        $service = new EntityIdsService();

        $collection = collect([
            ['id' => 1, 'label' => 'label-1', 'cod' => 'cod-1'],
            ['id' => 2, 'label' => 'label-2', 'cod' => 'cod-2'],
            ['id' => 3, 'label' => 'label-3', 'cod' => 'cod-3']
        ]);

        $result = $service->parseCollection($collection, 'id', 'label', 'cod');

        $this->assertEquals(
            ['id' => 1, 'name' => 'label-1', 'description' => 'cod-1'],
            $result[1]
        );

        $this->assertEquals(
            ['id' => 2, 'name' => 'label-2', 'description' => 'cod-2'],
            $result[2]
        );

        $this->assertEquals(
            ['id' => 3, 'name' => 'label-3', 'description' => 'cod-3'],
            $result[3]
        );
    }

    public function test_nestedParse()
    {
        $service = new EntityIdsService();

        $collection = collect([
            ['id' => 1, 'label' => ['nested' => 'label-1']],
            ['id' => 2, 'label' => ['nested' => 'label-2']],
            ['id' => 3, 'label' => ['nested' => 'label-3']]
        ]);

        $result = $service->parseCollection($collection, 'id', 'label.nested');

        $this->assertEquals(
            ['id' => 1, 'name' => 'label-1', 'description' => null],
            $result[1]
        );

        $this->assertEquals(
            ['id' => 2, 'name' => 'label-2', 'description' => null],
            $result[2]
        );

        $this->assertEquals(
            ['id' => 3, 'name' => 'label-3', 'description' => null],
            $result[3]
        );
    }
}
