<?php

namespace OctoCmsModule\Core\Tests\Services\PermissionService;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Config;
use OctoCmsModule\Core\DTO\EntityIdsDTO;
use OctoCmsModule\Core\Entities\Role;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Services\PermissionService;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetEntityIdsDTOTest
 *
 * @package OctoCmsModule\Core\Tests\Services\EntityIdsService
 */
class IsAllowedTest extends TestCase
{

    /**
     * Name test_isAllowed
     *
     * @return void
     */
    public function testIsAllowed()
    {
        /** @var User $user */
        $user = User::factory()->create();

        Role::factory()->create(['name' => 'role1']);
        Role::factory()->create(['name' => 'role2']);

        $user->roles()->attach([1, 2]);

        $service = new PermissionService();

        $this->assertTrue($service->isAllowed($user, 'role1'));

        $this->assertTrue($service->isAllowed($user, 'role2'));

        $this->assertTrue($service->isAllowed($user, ['role2']));

        $this->assertFalse($service->isAllowed($user, ['role3']));

        $this->assertTrue($service->isAllowed($user, ['role2', 'role3']));

        $this->assertFalse($service->isAllowed($user, ['role3', 'role4']));

    }

    public function testHierarchy()
    {
        /** @var User $user */
        $user = User::factory()->create();

        $user->roles()->attach([
            Role::factory()->create(['name' => 'role1'])->id,
            Role::factory()->create(['name' => 'role2'])->id,
        ]);

        Role::factory()->create(['name' => 'role3']);

        Config::set('octo-cms.hierarchy_roles', [
            'role1' => [ 'role1-1', 'role1-2']
        ]);

        $service = new PermissionService();

        $this->assertTrue($service->isAllowed($user, 'role1'));
        $this->assertTrue($service->isAllowed($user, 'role2'));
        $this->assertFalse($service->isAllowed($user, 'role3'));
        $this->assertTrue($service->isAllowed($user, 'role1-1'));
        $this->assertTrue($service->isAllowed($user, 'role1-2'));
    }
}
