<?php

namespace OctoCmsModule\Core\Tests\Services\RegistryService;

use OctoCmsModule\Core\Entities\CompanyRegistry;
use OctoCmsModule\Core\Entities\CustomField;
use OctoCmsModule\Core\Entities\PrivateRegistry;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Exceptions\OctoCmsException;
use OctoCmsModule\Core\Services\RegistryService;
use OctoCmsModule\Core\Tests\Mocks\SettingServiceMock;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class SaveRegistryTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Services\UserService
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class SaveRegistryTest extends TestCase
{

    public function test_saveNewRegistryTypeWrong()
    {

        /** @var RegistryService $registryService */
        $registryService = new RegistryService(new SettingServiceMock());

        /** @var array $fields */
        $fields = [
            'type' => "test",
            'code' => 'test',
        ];

        $this->expectException(OctoCmsException::class);

        /** @var Registry $registry */
        $registryService->saveRegistry(new Registry(), $fields);

    }

    /**
     * @throws OctoCmsException
     */
    public function test_saveNewPrivateRegistry()
    {

        /** @var RegistryService $registryService */
        $registryService = new RegistryService(new SettingServiceMock());

        /** @var array $fields */
        $fields = [
            'type'            => Registry::TYPE_PRIVATE,
            'code'            => '1234',
            'privateRegistry' => [
                'name'       => 'Roberto',
                'surname'    => 'La Rocca',
                'codfis'     => 'LRCRRT71E14F205A',
                'birth_date' => '1971-05-14'
            ]
        ];

        $registryService->saveRegistry(new Registry(), $fields);

        $this->assertDatabaseHas('registry', [
            'id'   => 1,
            'type' => $fields['type'],
        ]);

        $this->assertDatabaseHas('private_registry', [
            'registry_id' => '1',
            'name'        => 'Roberto',
            'surname'     => 'La Rocca',
        ]);

    }

    /**
     * @throws OctoCmsException
     */
    public function test_saveNewCompanyRegistry()
    {

        /** @var RegistryService $registryService */
        $registryService = new RegistryService(new SettingServiceMock());

        /** @var array $fields */
        $fields = [
            'type'            => Registry::TYPE_COMPANY,
            'code'            => '1234',
            'companyRegistry' => [
                'businessname' => 'Octopus',
                'piva'         => '08382510967',
                'codfis'       => 'LRCRRT71E14F205A',
                'web'          => 'https://www.octopus.srl'
            ]
        ];

        $registryService->saveRegistry(new Registry(), $fields);

        $this->assertDatabaseHas('registry', [
            'id'   => 1,
            'type' => $fields['type'],
        ]);

        $this->assertDatabaseHas('company_registry', [
            'registry_id'  => '1',
            'businessname' => 'Octopus',
            'piva'         => '08382510967',
        ]);

    }


    /**
     * @throws OctoCmsException
     */
    public function test_updatePrivateRegistry()
    {
        /**
         * Registry Service
         *
         * @var RegistryService $registryService RegistryService instance
         */
        $registryService = new RegistryService(new SettingServiceMock());

        /**
         * Fields array
         *
         * @var array $fields fields to update
         */
        $fields = [
            'type'            => Registry::TYPE_PRIVATE,
            'code'            => '1234',
            'privateRegistry' => [
                'name'       => 'Roberto',
                'surname'    => 'La Rocca',
                'codfis'     => 'LRCRRT71E14F205A',
                'birth_date' => '1971-05-14'
            ]
        ];

        /**
         * Registry
         *
         * @var Registry $registry registry instance
         */
        $registry = Registry::factory()->has(PrivateRegistry::factory())->create(['type' => Registry::TYPE_PRIVATE]);

        $registryService->saveRegistry($registry, $fields);

        $this->assertDatabaseHas('registry', [
            'id'   => 1,
            'type' => $fields['type'],
        ]);

        $this->assertDatabaseHas('private_registry', [
            'registry_id' => '1',
            'name'        => 'Roberto',
            'surname'     => 'La Rocca',
        ]);

        $this->assertCount(1, PrivateRegistry::where('registry_id', '=', $registry->id)->get());
    }

    /**
     * @throws OctoCmsException
     */
    public function test_updateCompanyRegistry()
    {
        /**
         * Registry Service
         *
         * @var RegistryService $registryService RegistryService instance
         */
        $registryService = new RegistryService(new SettingServiceMock());

        /**
         * Fields array
         *
         * @var array $fields fields to update
         */
        $fields = [
            'type'            => Registry::TYPE_COMPANY,
            'code'            => '1234',
            'companyRegistry' => [
                'businessname' => 'Octopus',
                'piva'         => '08382510967',
                'codfis'       => 'LRCRRT71E14F205A',
                'web'          => 'https://www.octopus.srl'
            ]
        ];

        /**
         * Registry
         *
         * @var Registry $registry registry instance
         */
        $registry = Registry::factory()->has(CompanyRegistry::factory())->create(['type' => Registry::TYPE_COMPANY]);

        $registryService->saveRegistry($registry, $fields);

        $this->assertDatabaseHas('registry', [
            'id'   => 1,
            'type' => $fields['type'],
        ]);

        $this->assertDatabaseHas('company_registry', [
            'registry_id'  => '1',
            'businessname' => 'Octopus',
            'piva'         => '08382510967',
        ]);

        $this->assertCount(1, CompanyRegistry::where('registry_id', '=', $registry->id)->get());
    }

    /**
     * @throws OctoCmsException
     */
    public function testSaveCustomFields()
    {
        $registryService = new RegistryService(new SettingServiceMock());

        CustomField::factory()->create([
            'name' => 'custom',
            'type' => 'string'
        ]);

        /** @var Registry $registry */
        $registry = Registry::factory()->has(CompanyRegistry::factory())->create(['type' => Registry::TYPE_COMPANY]);

        $fields = [
            'type'            => Registry::TYPE_COMPANY,
            'companyRegistry' => [
                'businessname' => 'Octopus',
            ],
            'customFields' => [
                'custom' => 'test'
            ]
        ];

        $registryService->saveRegistry($registry, $fields);

        $customFields = $registry->parseCustomFields();

        $this->assertTrue(true);

        $this->assertArrayHasKey('custom', $customFields);

        $this->assertEquals('test', $customFields['custom']);


    }
}
