<?php

namespace OctoCmsModule\Core\Tests\Services\SettingService;

use Laravel\Sanctum\Sanctum;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Core\Services\SettingService;

/**
 * Class SaveSettingsTest
 *
 * @package OctoCmsModule\Core\Tests\Services\SettingService
 */
class SaveSettingsTest extends TestCase
{


    public function test_storeSettings()
    {
        Sanctum::actingAs(self::createAdminUser());

        $settings = [
            [
                'name'  => 'name-1',
                'value' => 'value-1',
            ],
            [
                'name'  => 'fe_logo',
                'value' => 'value-2',
            ],
        ];

        /** @var SettingService $settingService */
        $settingService = new SettingService();

        $settingService->saveSettings($settings);

        foreach ($settings as $setting) {
            $this->assertDatabaseHas('settings', [
                'name'  => $setting['name'],
                'value' => serialize($setting['value']),
            ]);
        }
    }

    public function test_updateSettings()
    {
        Sanctum::actingAs(self::createAdminUser());

        Setting::factory()->create([
            'name'  => 'name-1',
            'value' => 'value-1',
        ]);

        Setting::factory()->create([
            'name'  => 'fe_logo',
            'value' => 'value-2',
        ]);

        $settings = [
            [
                'name'  => 'name-1',
                'value' => 'updated-value-1',
            ],
            [
                'name'  => 'fe_logo',
                'value' => 'updated-value-2',
            ],
        ];

        /** @var SettingService $settingService */
        $settingService = new SettingService();

        $settingService->saveSettings($settings);

        foreach ($settings as $setting) {
            $this->assertDatabaseHas('settings', [
                'name'  => $setting['name'],
                'value' => serialize($setting['value']),
            ]);
        }
    }
}
