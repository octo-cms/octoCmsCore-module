<?php

namespace OctoCmsModule\Core\Tests\Services\SettingService;

use OctoCmsModule\Core\Constants\CacheTagConst;
use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Core\Services\CacheService;
use OctoCmsModule\Core\Services\SettingService;
use Psr\SimpleCache\InvalidArgumentException;
use OctoCmsModule\Core\Tests\TestCase;

/**
 * Class GetSettingByNameTest
 *
 * @package OctoCmsModule\Core\Tests\Services\SettingService
 */
class GetSettingByNameTest extends TestCase
{


    /**
     * @throws InvalidArgumentException
     */
    public function test_withCache()
    {
        /** @var string $name */
        $name = 'cached-setting';

        CacheService::set(CacheTagConst::SETTING, $name, 'cached-value');

        Setting::factory()->create([
            'name'  => 'cached-setting',
            'value' => 'cached-value',
        ]);

        $service = new SettingService();

        $this->assertEquals(
            'cached-value',
            $service->getSettingByName('cached-setting')
        );
    }

    /**
     * @throws InvalidArgumentException
     */
    public function test_withoutCache()
    {
        Setting::factory()->create([
            'name'  => 'name',
            'value' => 'test',
        ]);

        $service = new SettingService();

        $this->assertEquals(
            'test',
            $service->getSettingByName('name', false)
        );
    }

    /**
     * @throws InvalidArgumentException
     */
    public function test_noResult()
    {
        $service = new SettingService();

        $this->assertNull(
            $service->getSettingByName('new-setting-no-sesult')
        );

    }
}
