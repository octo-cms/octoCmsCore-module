<?php

namespace OctoCmsModule\Core\Tests\Services\ActiveCampaignService;

use Illuminate\Support\Facades\Queue;
use OctoCmsModule\Core\Jobs\LoggingJob;
use OctoCmsModule\Core\Services\ActiveCampaignService;
use OctoCmsModule\Core\Tests\Mocks\SettingServiceMock;
use ReflectionException;
use OctoCmsModule\Core\Tests\TestCase;
use Illuminate\Support\Facades\Http;

/**
 * Class SubscribeContactToListTest
 *
 * @package OctoCmsModule\Core\Tests\Services\ActiveCampaignService
 */
class SubscribeContactToListTest extends TestCase
{


    public function test_subscribeContact()
    {
        Http::fake();
        Queue::fake();

        /** @var ActiveCampaignService */
        $activeCampaignService = new ActiveCampaignService(
            new SettingServiceMock()
        );

        /** @var bool $result */
        $activeCampaignService->subscribeContactToList(
            ['contact' => ['id' => 1]]
        );

        Queue::assertPushed(LoggingJob::class);
    }


    public function test_subscribeContactWrong()
    {
        Http::fake();
        Queue::fake();

        /** @var ActiveCampaignService */
        $activeCampaignService = new ActiveCampaignService(
            new SettingServiceMock()
        );

        /** @var bool $result */
        $this->assertFalse($activeCampaignService->subscribeContactToList(
           null
        ));

        Queue::assertNothingPushed();
        Http::assertNothingSent();
    }

    public function test_subscribeContactWrongResponse()
    {
        Http::fake(function ($request) {
            return Http::response([], 400);
        });
        Queue::fake();

        /** @var ActiveCampaignService */
        $activeCampaignService = new ActiveCampaignService(
            new SettingServiceMock()
        );

        /** @var bool $result */
        $this->assertFalse($activeCampaignService->subscribeContactToList(
            ['contact' => ['id' => 1]]
        ));

        Queue::assertPushed(LoggingJob::class);
    }
}
