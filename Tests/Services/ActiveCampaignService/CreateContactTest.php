<?php

namespace OctoCmsModule\Core\Tests\Services\ActiveCampaignService;

use Illuminate\Support\Facades\Queue;
use OctoCmsModule\Core\Constants\SettingNameConst;
use OctoCmsModule\Core\DTO\ContactFormDataDTO;
use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Core\Services\ActiveCampaignService;
use OctoCmsModule\Core\Tests\Mocks\SettingServiceMock;
use OctoCmsModule\Core\Tests\TestCase;
use Illuminate\Support\Facades\Http;

/**
 * Class CreateContactTest
 *
 * @package OctoCmsModule\Core\Tests\Services\ActiveCampaignService
 */
class CreateContactTest extends TestCase
{

    /**
     * Name test_createContactDisabled
     *
     * @return void
     */
    public function test_createContactDisabled()
    {
        Setting::factory()->create([
            'name'  => SettingNameConst::ACTIVE_CAMPAIGN,
            'value' => ['enabled' => false],
        ]);

        /** @var ActiveCampaignService */
        $activeCampaignService = new ActiveCampaignService(
            new SettingServiceMock()
        );

        $this->assertNull($activeCampaignService->createContact(new ContactFormDataDTO()));
    }

    public function test_createContactWrong()
    {
        Queue::fake();

        Http::fake(function ($request) {
            return Http::response([], 400);
        });

        Setting::factory()->create([
            'name'  => SettingNameConst::ACTIVE_CAMPAIGN,
            'value' => ['enabled' => false],
        ]);

        /** @var ActiveCampaignService */
        $activeCampaignService = new ActiveCampaignService(
            new SettingServiceMock()
        );

        $this->assertNull($activeCampaignService->createContact(new ContactFormDataDTO()));
    }

    public function test_createContact()
    {
        Http::fake(function ($request) {
            return Http::response(['contact' => [ 'id' => 'test']], 201);
        });

        Queue::fake();

        Setting::factory()->create([
            'name'  => 'active_campaign',
            'value' => ['enabled' => true],
        ]);

        /** @var ActiveCampaignService */
        $activeCampaignService = new ActiveCampaignService(
            new SettingServiceMock()
        );

        $contactFormDataDTO = new ContactFormDataDTO();
        $contactFormDataDTO->lastName = 'lastname';
        $contactFormDataDTO->firstName = 'firstName';
        $contactFormDataDTO->email = 'email';
        $contactFormDataDTO->phone = 'phone';

        $this->assertEquals(
            ['contact' => [ 'id' => 'test']],
            $activeCampaignService->createContact($contactFormDataDTO)
        );

    }
}
