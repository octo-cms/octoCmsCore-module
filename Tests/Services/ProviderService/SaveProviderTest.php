<?php

namespace OctoCmsModule\Core\Tests\Services\ProviderService;

use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Core\Services\ProviderService;

/**
 * Class SaveProviderTest
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Tests\Services\ProviderService
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class SaveProviderTest extends TestCase
{

    /**
     * Name test_storeProvider
     *
     * @return void
     */
    public function test_storeProvider()
    {
        /** @var array $fields */
        $fields = [
            'enabled'      => true,
            'name'         => 'name',
            'slug'         => 'slug',
            'record_price' => 10,
            'xls_mapping'  => [
                [
                    'column' => 1,
                    'value'  => [
                        'value' => 'value-1',
                        'name'  => 'name-1',
                    ],
                ],
                [
                    'column' => 2,
                    'value'  => [
                        'value' => 'value-2',
                        'name'  => 'name-2',
                    ],
                ],
            ],
        ];

        /** @var ProviderService $providerService */
        $providerService = new ProviderService();

        $providerService->saveProvider(new Provider(), $fields);

        $this->assertDatabaseHas('providers', [
            'id'           => 1,
            'enabled'      => $fields['enabled'],
            'name'         => $fields['name'],
            'slug'         => $fields['slug'],
            'record_price' => $fields['record_price'],
            'xls_mapping'  => serialize($fields['xls_mapping']),
        ]);
    }

    /**
     * Name test_updateService
     *
     * @return void
     */
    public function test_updateService()
    {
        /** @var array $fields */
        $fields = [
            'enabled'      => true,
            'name'         => 'name',
            'slug'         => 'slug',
            'record_price' => 10,
            'xls_mapping'  => [
                [
                    'column' => 1,
                    'value'  => [
                        'value' => 'value-1',
                        'name'  => 'name-1',
                    ],
                ],
                [
                    'column' => 2,
                    'value'  => [
                        'value' => 'value-2',
                        'name'  => 'name-2',
                    ],
                ],
            ],
        ];

        /** @var Provider $provider */
        $provider = Provider::factory()->create([
            'enabled'      => false,
            'name'         => 'old name',
            'slug'         => 'old_slug',
            'record_price' => $fields['record_price'],
            'xls_mapping'  => [],
        ]);

        /** @var ProviderService $providerService */
        $providerService = new ProviderService();

        $providerService->saveProvider($provider, $fields);

        $this->assertDatabaseHas('providers', [
            'id'           => 1,
            'enabled'      => $fields['enabled'],
            'name'         => $fields['name'],
            'slug'         => $fields['slug'],
            'record_price' => $fields['record_price'],
            'xls_mapping'  => serialize($fields['xls_mapping']),
        ]);
    }
}
