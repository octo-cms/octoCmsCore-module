<?php

namespace OctoCmsModule\Core\Tests\Services\ProviderService;

use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Storage;
use OctoCmsModule\Core\Jobs\ParseProviderImportJob;
use OctoCmsModule\Core\Jobs\SyncProviderImportJob;
use OctoCmsModule\Core\Tests\TestCase;
use OctoCmsModule\Core\Services\ProviderService;

/**
 * Class SaveProviderTest
 *
 * @package OctoCmsModule\Core\Tests\Services\ProviderService
 */
class UploadImportTest extends TestCase
{


    public function test_uploadImport()
    {

        Storage::fake('temp');
        Queue::fake();

        /** @var ProviderService $providerService */
        $providerService = new ProviderService();

        $providerService->uploadImport(
            UploadedFile::fake()->create('import.xls', 100),
            ['date' => Carbon::now()->format('Y-m-d')]
        );

        $this->assertDatabaseHas('provider_imports', [
            'id'      => 1,
            'status'  => 'pending'
        ]);

        Storage::disk('temp')->assertExists('imports/import-1.xls');

        Queue::assertPushedWithChain(
            ParseProviderImportJob::class,
            [SyncProviderImportJob::class]
        );
    }
}
