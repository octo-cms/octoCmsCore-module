<?php

namespace OctoCmsModule\Core\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

/**
 * Class NewPasswordNotification
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Notifications
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class NewPasswordNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * User random generated password
     *
     * @var string
     */
    protected $newPassword;

    /**
     * NewPasswordNotification constructor.
     *
     * @param string $newPassword User random generated password
     */
    public function __construct(string $newPassword)
    {
        $this->newPassword = $newPassword;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable class instance
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable class instance
     *
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        $notifiable->load('emailable');

        return (new MailMessage)
            ->subject('Nuova password generata ' . env('APP_NAME'))
            ->greeting('Ciao ' . $notifiable->emailable->name . ',')
            ->line('questa è la tua nuova password: ' . $this->newPassword)
            ->action('Clicca qui per accedere', env('MIX_ADMIN_BASE_URL'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable class instance
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
