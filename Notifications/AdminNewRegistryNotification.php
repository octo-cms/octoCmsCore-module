<?php

namespace OctoCmsModule\Core\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use OctoCmsModule\Core\DTO\ContactFormDataDTO;

/**
 * Class AdminNewRegistryNotification
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Core\Notifications
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class AdminNewRegistryNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Description
     * @var ContactFormDataDTO
     */
    protected $contactFormDataDTO;

    /**
     * AdminNewRegistryNotification constructor.
     * @param ContactFormDataDTO $contactFormDataDTO
     */
    public function __construct(ContactFormDataDTO $contactFormDataDTO)
    {
        $this->contactFormDataDTO = $contactFormDataDTO;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable class instance
     *
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param mixed $notifiable class instance
     *
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Nuova registrazione efettuata su ' . env('APP_NAME'))
            ->greeting('Ciao Amministratore,')
            ->line('Un nuovo utente ti ha contattato.')
            ->line('Nome: ' . $this->contactFormDataDTO->firstName
                . ' ' . $this->contactFormDataDTO->lastName)
            ->line('Email: ' . $this->contactFormDataDTO->email)
            ->line('Messaggio:')
            ->line($this->contactFormDataDTO->message);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable class instance
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
