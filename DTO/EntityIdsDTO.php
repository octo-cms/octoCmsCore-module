<?php

namespace OctoCmsModule\Core\DTO;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Class EntityIdsDTO
 *
 * @package OctoCmsModule\Core\DTO
 */
class EntityIdsDTO
{
    /** @var int */
    public $total;
    /** @var Builder */
    public $builder;
    /** @var Collection */
    public $collection;
    /** @var int */
    public $currentPage;
    /** @var int */
    public $rowsInPage;
}
