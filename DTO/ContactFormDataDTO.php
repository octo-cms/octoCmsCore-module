<?php

namespace OctoCmsModule\Core\DTO;

/**
 * Class ContactFormDataDTO
 *
 * @category Octo
 * @package  OctoCmsModule\Core\DTO
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class ContactFormDataDTO
{
    /**
     * Firstname
     *
     * @var string
     */
    public $firstName;
    /**
     * Lastname
     *
     * @var string
     */
    public $lastName;
    /**
     * Email
     *
     * @var string
     */
    public $email;
    /**
     * Phone
     *
     * @var string
     */
    public $phone;
    /**
     * Message
     *
     * @var string
     */
    public $message;

    /**
     * Name toArray
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'firstName' => $this->firstName,
            'lastName'  => $this->lastName,
            'email'     => $this->email,
            'phone'     => $this->phone,
        ];
    }
}
