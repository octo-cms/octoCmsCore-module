<?php

namespace OctoCmsModule\Core\DTO;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

/**
 * Class DatatableDTO
 *
 * @package OctoCmsModule\Core\DTO
 */
class DatatableDTO
{
    /** @var int */
    public $total;
    /** @var Builder */
    public $builder;
    /** @var Collection */
    public $collection;
    /** @var int */
    public $currentPage;
    /** @var int */
    public $rowsInPage;
}
