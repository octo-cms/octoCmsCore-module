<?php


namespace OctoCmsModule\Core\Constants;

/**
 * Class SettingNameConst
 *
 * @package OctoCmsModule\Core\Constants
 */
class SettingNameConst
{
    public const LOCALE = 'locale';
    public const LANGUAGES = 'languages';
    public const MULTILANGUAGE = 'multilanguage';
    public const ADMIN_MAIL = 'admin_mail';
    public const FALLBACK_LOCALE = 'fallback_locale';
    public const ACTIVE_CAMPAIGN = 'active_campaign';
    public const FE_IUBENDA = 'fe_iubenda';
    public const FE_SOCIAL_SHARING = 'fe_social_sharing';
    public const FE_FAVICON = 'fe_favicon';
    public const FE_FAVICON_DATA = 'fe_favicon_data';
    public const FE_TITLE = 'fe_title';
    public const FE_DESCRIPTION = 'fe_description';
    public const FE_ADDRESS = 'fe_address';
}
