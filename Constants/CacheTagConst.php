<?php

declare(strict_types=1);

namespace OctoCmsModule\Core\Constants;

/**
 * @package OctoCmsModule\Core\Constants
 */
class CacheTagConst
{
    public const FOOTER = 'footers';
    public const SETTING = 'settings';
    public const USER_ROLES = 'user_roles';
}
