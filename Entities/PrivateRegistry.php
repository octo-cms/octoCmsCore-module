<?php

namespace OctoCmsModule\Core\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OctoCmsModule\Core\Factories\PrivateRegistryFactory;



/**
 * OctoCmsModule\Core\Entities\PrivateRegistry
 *
 * @property int $id
 * @property int $registry_id
 * @property string|null $name
 * @property string|null $surname
 * @property string|null $gender
 * @property string|null $codfis
 * @property string|null $birth_date
 * @property-read \OctoCmsModule\Core\Entities\Registry $registry
 * @method static \Illuminate\Database\Eloquent\Builder|PrivateRegistry newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PrivateRegistry newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PrivateRegistry query()
 * @method static \Illuminate\Database\Eloquent\Builder|PrivateRegistry whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrivateRegistry whereCodfis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrivateRegistry whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrivateRegistry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrivateRegistry whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrivateRegistry whereRegistryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PrivateRegistry whereSurname($value)
 * @mixin \Eloquent
 * @method static \OctoCmsModule\Core\Factories\PrivateRegistryFactory factory(...$parameters)
 */
class PrivateRegistry extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'private_registry';

    protected $fillable = [
        'name',
        'surname',
        'codfis',
        'birth_date',
        'gender'
    ];

    protected $casts = [
        'birth_date' => 'date:Y-m-d'
    ];

    /**
     * @return PrivateRegistryFactory
     */
    protected static function newFactory()
    {
        return PrivateRegistryFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function registry()
    {
        return $this->belongsTo(Registry::class);
    }
}
