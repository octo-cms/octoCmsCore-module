<?php

namespace OctoCmsModule\Core\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OctoCmsModule\Core\Factories\CityFactory;

/**
 * Class City
 *
 * @package OctoCmsModule\Core\Entities
 * @property string $cod_comune_alfanumerico
 * @property int $cod_comune_numerico
 * @property string $cod_regione
 * @property string $regione
 * @property string $cod_sovracomunale
 * @property string $sovracomunale
 * @property string $cod_provincia
 * @property string $progressivo
 * @property string $cod_catastale
 * @property string $denominazione
 * @property string $cod_ripartizione_geografica
 * @property string $ripartizione_geografica
 * @property int $capoluogo
 * @property string $sigla_automobilistica
 * @property string $nuts1
 * @property string $nuts2
 * @property string $nuts3
 * @method static Builder|City newModelQuery()
 * @method static Builder|City newQuery()
 * @method static Builder|City query()
 * @method static Builder|City whereCapoluogo($value)
 * @method static Builder|City whereCodCatastale($value)
 * @method static Builder|City whereCodComuneAlfanumerico($value)
 * @method static Builder|City whereCodComuneNumerico($value)
 * @method static Builder|City whereCodProvincia($value)
 * @method static Builder|City whereCodRegione($value)
 * @method static Builder|City whereCodRipartizioneGeografica($value)
 * @method static Builder|City whereCodSovracomunale($value)
 * @method static Builder|City whereDenominazione($value)
 * @method static Builder|City whereNuts1($value)
 * @method static Builder|City whereNuts2($value)
 * @method static Builder|City whereNuts3($value)
 * @method static Builder|City whereProgressivo($value)
 * @method static Builder|City whereRegione($value)
 * @method static Builder|City whereRipartizioneGeografica($value)
 * @method static Builder|City whereSiglaAutomobilistica($value)
 * @method static Builder|City whereSovracomunale($value)
 * @mixin Eloquent
 * @property string|null $postal_code
 * @property float|null $latitude
 * @property float|null $longitude
 * @method static Builder|City whereLatitude($value)
 * @method static Builder|City whereLongitude($value)
 * @method static Builder|City wherePostalCode($value)
 * @method static \OctoCmsModule\Core\Factories\CityFactory factory(...$parameters)
 */
class City extends Model
{
    use HasFactory;

    protected $table = 'cities';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = 'cod_comune_alfanumerico';
    protected $keyType = 'string';

    /**
     * @return CityFactory
     */
    protected static function newFactory()
    {
        return CityFactory::new();
    }
}
