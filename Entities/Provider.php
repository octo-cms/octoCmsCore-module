<?php

namespace OctoCmsModule\Core\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;
use OctoCmsModule\Core\Factories\ProviderFactory;

/**
 * Class Provider
 *
 * @package OctoCmsModule\Core\Entities
 * @property int $id
 * @property string $label
 * @property bool $enabled
 * @property mixed $xls_mapping
 * @property-read int|null $import_temps_count
 * @method static \Illuminate\Database\Eloquent\Builder|Provider newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Provider newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Provider query()
 * @method static \Illuminate\Database\Eloquent\Builder|Provider whereEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Provider whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Provider whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Provider whereXlsMapping($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\OctoCmsModule\Core\Entities\ProviderImportData[] $providerImportData
 * @property-read int|null $provider_import_data_count
 * @property mixed $name
 * @property string $slug
 * @property string $record_price
 * @method static \Illuminate\Database\Eloquent\Builder|Provider whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Provider whereRecordPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Provider whereSlug($value)
 * @property string|null $acronym
 * @method static \Illuminate\Database\Eloquent\Builder|Provider whereAcronym($value)
 * @method static \OctoCmsModule\Core\Factories\ProviderFactory factory(...$parameters)
 */
class Provider extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'enabled',
        'xls_mapping',
        'record_price',
        'acronym',
        'slug'
    ];

    protected $casts = [
        'enabled' => 'boolean',
    ];

    public $timestamps = false;

    /**
     * @return ProviderFactory
     */
    protected static function newFactory()
    {
        return ProviderFactory::new();
    }

    /**
     * @param $value
     *
     * @return void
     */
    public function setXlsMappingAttribute($value)
    {
        $this->attributes['xls_mapping'] = serialize($value);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getXlsMappingAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * Name providerImportData
     *
     * @return HasMany
     */
    public function providerImportData()
    {
        return $this->hasMany(ProviderImportData::class);
    }
}
