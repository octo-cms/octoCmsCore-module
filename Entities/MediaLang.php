<?php

namespace OctoCmsModule\Core\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OctoCmsModule\Core\Factories\MediaLangFactory;

/**
 * OctoCmsModule\Core\Entities\MediaLang
 *
 * @property int         $id
 * @property int         $media_id
 * @property string      $lang
 * @property string|null $alt
 * @property string|null $caption
 * @property string|null $title
 * @property string|null $description
 * @property-read Media  $media
 * @method static Builder|MediaLang newModelQuery()
 * @method static Builder|MediaLang newQuery()
 * @method static Builder|MediaLang query()
 * @method static Builder|MediaLang whereAlt($value)
 * @method static Builder|MediaLang whereCaption($value)
 * @method static Builder|MediaLang whereDescription($value)
 * @method static Builder|MediaLang whereId($value)
 * @method static Builder|MediaLang whereLang($value)
 * @method static Builder|MediaLang whereMediaId($value)
 * @method static Builder|MediaLang whereTitle($value)
 * @mixin Eloquent
 * @method static \OctoCmsModule\Core\Factories\MediaLangFactory factory(...$parameters)
 */
class MediaLang extends Model
{
    use HasFactory;

    protected $fillable = [
        'media_id',
        'lang',
        'alt',
        'title',
        'caption',
        'description',
    ];

    public $timestamps = false;

    /**
     * @return MediaLangFactory
     */
    protected static function newFactory()
    {
        return MediaLangFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function media()
    {
        return $this->belongsTo(Media::class);
    }
}
