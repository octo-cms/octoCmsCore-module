<?php

namespace OctoCmsModule\Core\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OctoCmsModule\Core\Factories\CustomFieldEntityNumericFactory;

/**
 * Class CustomFieldEntityNumeric
 *
 * @package OctoCmsModule\Core\Entities
 * @property int $id
 * @property \OctoCmsModule\Core\Entities\CustomFieldEntity|null $value
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityNumeric newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityNumeric newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityNumeric query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityNumeric whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityNumeric whereValue($value)
 * @mixin \Eloquent
 * @method static \OctoCmsModule\Core\Factories\CustomFieldEntityNumericFactory factory(...$parameters)
 */
class CustomFieldEntityNumeric extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'custom_field_entity_numeric';

    protected $fillable = ['value'];

    protected $casts = ['value' => 'integer'];

    /**
     * @return CustomFieldEntityNumericFactory
     */
    protected static function newFactory()
    {
        return CustomFieldEntityNumericFactory::new();
    }

    /**
     * @param $value
     */
    public function setValueAttribute($value)
    {
        $this->attributes['value'] = gettype($value) === 'integer' ? $value : null;
    }
}
