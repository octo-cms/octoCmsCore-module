<?php

namespace OctoCmsModule\Core\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Sanctum\PersonalAccessToken;
use OctoCmsModule\Core\Factories\UserFactory;
use OctoCmsModule\Core\Traits\AddressableTrait;
use OctoCmsModule\Core\Traits\CustomFieldEntityTrait;
use OctoCmsModule\Core\Traits\EmailableTrait;
use OctoCmsModule\Core\Traits\PhoneableTrait;
use OctoCmsModule\Core\Traits\PicturableTrait;
use Plank\Mediable\Mediable;
use Plank\Mediable\MediableCollection;

/**
 * Class User
 *
 * @package OctoCmsModule\Core\Entities
 * @property int                                                        $id
 * @property string|null                                                $name
 * @property string                                                     $email
 * @property Carbon|null                                                $email_verified_at
 * @property string                                                     $password
 * @property string|null                                                $remember_token
 * @property Carbon|null                                                $created_at
 * @property Carbon|null                                                $updated_at
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null                                              $notifications_count
 * @property-read Collection|Role[]                                     $roles
 * @property-read int|null                                              $roles_count
 * @property-read Collection|PersonalAccessToken[]                      $tokens
 * @property-read int|null                                              $tokens_count
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read Collection|Media[]                                    $media
 * @property-read int|null                                              $media_count
 * @property-read Collection|Picture[]      $pictures
 * @property-read int|null                  $pictures_count
 * @method static MediableCollection|static[] all($columns = ['*'])
 * @method static MediableCollection|static[] get($columns = ['*'])
 * @method static Builder|User whereHasMedia($tags, $matchAll = false)
 * @method static Builder|User whereHasMediaMatchAll($tags)
 * @method static Builder|User withMedia($tags = [], $matchAll = false)
 * @method static Builder|User withMediaMatchAll($tags = [])
 * @property-read MediableCollection|User[] $children
 * @property-read int|null                  $children_count
 * @property-read Collection|CompanyData[]  $companyData
 * @property-read int|null                  $company_data_count
 * @property-read Collection|Email[]        $emails
 * @property-read int|null                  $emails_count
 * @property-read MediableCollection|User[] $parents
 * @property-read int|null                  $parents_count
 * @property-read Collection|Phone[]        $phones
 * @property-read int|null                  $phones_count
 * @property-read Collection|PrivateData[]  $privateData
 * @property-read int|null                  $private_data_count
 * @method static Builder|User type($type)
 * @property string $username
 * @property bool $temp_password
 * @property bool $active
 * @property-read Collection|\OctoCmsModule\Core\Entities\CustomFieldEntity[] $customFieldEntities
 * @property-read int|null $custom_field_entities_count
 * @method static Builder|User whereActive($value)
 * @method static Builder|User whereTempPassword($value)
 * @method static Builder|User whereUsername($value)
 * @property-read \OctoCmsModule\Core\Entities\Email|null $defaultEmail
 * @property-read Collection|\OctoCmsModule\Core\Entities\Address[] $addresses
 * @property-read int|null $addresses_count
 * @property-read \OctoCmsModule\Core\Entities\Phone|null $defaultPhone
 * @property-read \OctoCmsModule\Core\Entities\Address|null $defaultAddress
 * @property-read Collection|\OctoCmsModule\Core\Entities\UserEventLog[] $userEventLogs
 * @property-read int|null $user_event_logs_count
 * @method static Builder|User whereCustomFields($customFields, string $operator, $query)
 * @method static \OctoCmsModule\Core\Factories\UserFactory factory(...$parameters)
 */
class User extends Authenticatable
{
    use HasApiTokens, Mediable, PicturableTrait, EmailableTrait, PhoneableTrait, HasFactory,
        CustomFieldEntityTrait, AddressableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'password',
        'temp_password',
        'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'temp_password'     => 'boolean',
        'active'            => 'boolean',
    ];

    public function __construct(array $attributes = [])
    {
        Arr::set($attributes, 'active', true);
        Arr::set($attributes, 'temp_password', true);
        if(!Arr::has($attributes, 'password')) {
            Arr::set($attributes, 'password', Str::random(10));
        }

        parent::__construct($attributes);

    }

    /**
     * @return UserFactory
     */
    protected static function newFactory()
    {
        return UserFactory::new();
    }

    /**
     * @param string $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_role');
    }


    /**
     * @return BelongsToMany
     */
    public function parents()
    {
        return $this->belongsToMany(
            User::class,
            'user_relation',
            'parent_id',
            'child_id'
        )->withPivot('type');
    }

    /**
     * @return BelongsToMany
     */
    public function children()
    {
        return $this->belongsToMany(
            User::class,
            'user_relation',
            'child_id',
            'parent_id'
        )->withPivot('type');
    }

    /**
     * @param $query
     * @param $type
     * @return mixed
     */
    public function scopeType($query, $type)
    {
        return $query->wherePivotType($type);
    }

    /**
     * Name userEventLogs
     *
     * @return HasMany
     */
    public function userEventLogs(): HasMany
    {
        return $this->hasMany(UserEventLog::class);
    }
}
