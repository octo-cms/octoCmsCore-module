<?php

namespace OctoCmsModule\Core\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OctoCmsModule\Core\Factories\AddressFactory;


/**
 * OctoCmsModule\Core\Entities\Address
 *
 * @property int                  $id
 * @property int|null             $addressable_id
 * @property string|null          $addressable_type
 * @property int                  $default
 * @property string|null          $alias
 * @property string|null          $place_id
 * @property string|null          $street_address              indicates a precise street address.
 * @property string|null          $route                       indicates a named route (such as "US 101").;
 * intersection indicates a major intersection, usually of two major roads.
 * @property string|null          $political                   indicates a political entity. Usually, this type indicates a polygon of some
 *                 civil administration.
 * @property string|null          $country                     indicates the national political entity, and is typically the highest order
 *                 type returned by the Geocoder.
 * @property string|null          $administrative_area_level_1 indicates a first-order civil entity below the country level. Within the United
 *                 States, these administrative levels are states. Not all nations exhibit these administrative levels.
 *                 In most cases, administrative_area_level_1 short names will closely match ISO 3166-2 subdivisions and
 *                 other widely circulated lists; however this is not guaranteed as our geocoding results are based on a
 *                 variety of signals and location data.
 * @property string|null          $administrative_area_level_2 indicates a second-order civil entity below the country level. Within the United
 *                 States, these administrative levels are counties. Not all nations exhibit these administrative
 *                 levels.
 * @property string|null          $administrative_area_level_3 indicates a third-order civil entity below the country level. This type indicates
 *                 a minor civil division. Not all nations exhibit these administrative levels.
 * @property string|null          $administrative_area_level_4 indicates a fourth-order civil entity below the country level. This type indicates
 *                 a minor civil division. Not all nations exhibit these administrative levels.
 * @property string|null          $administrative_area_level_5 indicates a fifth-order civil entity below the country level. This type indicates
 *                 a minor civil division. Not all nations exhibit these administrative levels.
 * @property string|null          $colloquial_area             indicates a commonly-used alternative name for the entity.
 * @property string|null          $locality                    indicates an incorporated city or town political entity.
 * @property string|null          $sublocality                 indicates a first-order civil entity below a locality. For some locations may
 *                 receive one of the additional types: sublocality_level_1 to sublocality_level_5. Each sublocality
 *                 level is a civil entity. Larger numbers indicate a smaller geographic area.
 * @property string|null          $neighborhood                indicates a named neighborhood
 * @property string|null          $premise                     indicates a named location, usually a building or collection of buildings with a
 *                 common name
 * @property string|null          $subpremise                  indicates a first-order entity below a named location, usually a singular building
 *                 within a collection of buildings with a common name
 * @property string|null          $plus_code                   indicates an encoded location reference, derived from latitude and longitude.
 *                 Plus codes can be used as a replacement for street addresses in places where they do not exist (where
 *                 buildings are not numbered or streets are not named). See https://plus.codes for details.
 * @property string|null          $postal_code                 indicates a postal code as used to address postal mail within the country.
 * @property string|null          $natural_feature             indicates a prominent natural feature.
 * @property string|null          $airport                     indicates an airport.
 * @property string|null          $park                        indicates a named park.
 * @property string|null          $point_of_interest           indicates a named point of interest. Typically, these "POI"s are prominent
 *                 local entities that don't easily fit in another category, such as "Empire State Building"
 *                 or "Eiffel Tower".
 * @property string|null          $other
 * @property-read Model|\Eloquent $addressable
 * @method static Builder|Address newModelQuery()
 * @method static Builder|Address newQuery()
 * @method static Builder|Address query()
 * @method static Builder|Address whereAddressableId($value)
 * @method static Builder|Address whereAddressableType($value)
 * @method static Builder|Address whereAdministrativeAreaLevel1($value)
 * @method static Builder|Address whereAdministrativeAreaLevel2($value)
 * @method static Builder|Address whereAdministrativeAreaLevel3($value)
 * @method static Builder|Address whereAdministrativeAreaLevel4($value)
 * @method static Builder|Address whereAdministrativeAreaLevel5($value)
 * @method static Builder|Address whereAirport($value)
 * @method static Builder|Address whereAlias($value)
 * @method static Builder|Address whereColloquialArea($value)
 * @method static Builder|Address whereCountry($value)
 * @method static Builder|Address whereDefault($value)
 * @method static Builder|Address whereId($value)
 * @method static Builder|Address whereLocality($value)
 * @method static Builder|Address whereNaturalFeature($value)
 * @method static Builder|Address whereNeighborhood($value)
 * @method static Builder|Address whereOther($value)
 * @method static Builder|Address wherePark($value)
 * @method static Builder|Address wherePlaceId($value)
 * @method static Builder|Address wherePlusCode($value)
 * @method static Builder|Address wherePointOfInterest($value)
 * @method static Builder|Address wherePolitical($value)
 * @method static Builder|Address wherePostalCode($value)
 * @method static Builder|Address wherePremise($value)
 * @method static Builder|Address whereRoute($value)
 * @method static Builder|Address whereStreetAddress($value)
 * @method static Builder|Address whereSublocality($value)
 * @method static Builder|Address whereSubpremise($value)
 * @mixin Eloquent
 * @method static \OctoCmsModule\Core\Factories\AddressFactory factory(...$parameters)
 */
class Address extends Model
{
    use HasFactory;

    public const ALIAS_DEFAULT = 'default';

    protected $fillable = [
        'id',
        'default',
        'alias',
        'place_id',
        'street_address',
        'route',
        'political',
        'country',
        'administrative_area_level_1',
        'administrative_area_level_2',
        'administrative_area_level_3',
        'administrative_area_level_4',
        'administrative_area_level_5',
        'colloquial_area',
        'locality',
        'sublocality',
        'neighborhood',
        'premise',
        'subpremise',
        'plus_code',
        'postal_code',
        'natural_feature',
        'airport',
        'park',
        'point_of_interest',
        'other',
    ];

    public $timestamps = false;

    protected $casts = [
        'default' => 'boolean',
    ];

    /**
     * @return AddressFactory
     */
    protected static function newFactory()
    {
        return AddressFactory::new();
    }

    /**
     * Get the owning addressable model.
     */
    public function addressable()
    {
        return $this->morphTo('addressable', 'addressable_type', 'addressable_id', 'id');
    }
}
