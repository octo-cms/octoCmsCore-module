<?php

namespace OctoCmsModule\Core\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OctoCmsModule\Core\Factories\PictureLangFactory;

/**
 * OctoCmsModule\Core\Entities\PictureLang
 *
 * @property int          $id
 * @property int          $picture_id
 * @property string       $lang
 * @property string|null  $alt
 * @property string|null  $caption
 * @property string|null  $title
 * @property string|null  $description
 * @property-read Picture $picture
 * @method static Builder|PictureLang newModelQuery()
 * @method static Builder|PictureLang newQuery()
 * @method static Builder|PictureLang query()
 * @method static Builder|PictureLang whereAlt($value)
 * @method static Builder|PictureLang whereCaption($value)
 * @method static Builder|PictureLang whereDescription($value)
 * @method static Builder|PictureLang whereId($value)
 * @method static Builder|PictureLang whereLang($value)
 * @method static Builder|PictureLang wherePictureId($value)
 * @method static Builder|PictureLang whereTitle($value)
 * @mixin Eloquent
 * @method static \OctoCmsModule\Core\Factories\PictureLangFactory factory(...$parameters)
 */
class PictureLang extends Model
{
    use HasFactory;

    protected $fillable = [
        'picture_id',
        'lang',
        'alt',
        'title',
        'caption',
        'description',
    ];

    public $timestamps = false;

    /**
     * @return PictureLangFactory
     */
    protected static function newFactory()
    {
        return PictureLangFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function picture()
    {
        return $this->belongsTo(Picture::class);
    }
}
