<?php

namespace OctoCmsModule\Core\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OctoCmsModule\Core\Factories\CountryFactory;

/**
 * OctoCmsModule\Core\Entities\Country
 *
 * @property string      $sigla_alpha_2
 * @property string      $sigla_numerica
 * @property string      $sigla_alpha_3
 * @property string      $name_it
 * @property string|null $name_en
 * @property string      $name_es
 * @property string      $name_de
 * @property string      $name_fr
 * @method static Builder|Country newModelQuery()
 * @method static Builder|Country newQuery()
 * @method static Builder|Country query()
 * @method static Builder|Country whereNameDe($value)
 * @method static Builder|Country whereNameEn($value)
 * @method static Builder|Country whereNameEs($value)
 * @method static Builder|Country whereNameFr($value)
 * @method static Builder|Country whereNameIt($value)
 * @method static Builder|Country whereSiglaAlpha2($value)
 * @method static Builder|Country whereSiglaAlpha3($value)
 * @method static Builder|Country whereSiglaNumerica($value)
 * @mixin Eloquent
 * @property string $nome_stati
 * @method static \Illuminate\Database\Eloquent\Builder|\OctoCmsModule\Core\Entities\Country whereNomeStati($value)
 * @method static \OctoCmsModule\Core\Factories\CountryFactory factory(...$parameters)
 */
class Country extends Model
{
    use HasFactory;

    protected $table = 'countries';
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = 'sigla_alpha_2';
    protected $keyType = 'string';

    public const COLUMN_LANG_NAMES = [
        'name_en',
        'name_it',
        'name_es',
        'name_fr',
        'name_de',
    ];

    /**
     * @return CountryFactory
     */
    protected static function newFactory()
    {
        return CountryFactory::new();
    }

    /**
     * @param $columnName
     *
     * @return bool
     */
    public static function checkColumnName($columnName)
    {
        foreach (self::COLUMN_LANG_NAMES as $name) {
            if ($name === $columnName) {
                return true;
            }
        }

        return false;
    }
}
