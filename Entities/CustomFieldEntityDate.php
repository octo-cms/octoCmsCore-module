<?php

namespace OctoCmsModule\Core\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OctoCmsModule\Core\Factories\CustomFieldEntityDateFactory;

/**
 * Class CustomFieldEntityDate
 *
 * @package OctoCmsModule\Core\Entities
 * @property int $id
 * @property \OctoCmsModule\Core\Entities\CustomFieldEntity|null $value
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityDate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityDate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityDate query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityDate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityDate whereValue($value)
 * @mixin \Eloquent
 * @method static \OctoCmsModule\Core\Factories\CustomFieldEntityDateFactory factory(...$parameters)
 */
class CustomFieldEntityDate extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'custom_field_entity_date';

    protected $fillable = ['value'];

    protected $casts = ['value' => 'date:Y-m-d'];

    /**
     * @return CustomFieldEntityDateFactory
     */
    protected static function newFactory()
    {
        return CustomFieldEntityDateFactory::new();
    }

    /**
     * @param $value
     */
    public function setValueAttribute($value)
    {
        if ($value instanceof Carbon) {
            $this->attributes['value'] = $value;
        } else {
            $this->attributes['value'] = Carbon::canBeCreatedFromFormat($value, 'Y-m-d')
                ? Carbon::createFromFormat('Y-m-d', $value)
                : null;
        }
    }
}
