<?php

namespace OctoCmsModule\Core\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use OctoCmsModule\Core\Factories\TagFactory;

/**
 * OctoCmsModule\Core\Entities\Tag
 *
 * @property int $id
 * @property string $slug
 * @property string $name
 * @property int $suggest
 * @property int $count
 * @property int|null $tag_group_id
 * @property string|null $description
 * @property-read \Conner\Tagging\Model\TagGroup|null $group
 * @method static Builder|Tag inGroup($groupName)
 * @method static \Illuminate\Database\Eloquent\Builder|Tag newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tag newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Tag query()
 * @method static Builder|Tag suggested()
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereSuggest($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Tag whereTagGroupId($value)
 * @mixin \Eloquent
 * @method static \OctoCmsModule\Core\Factories\TagFactory factory(...$parameters)
 */
class Tag extends \Conner\Tagging\Model\Tag
{
    use HasFactory;

    /**
     * @return TagFactory
     */
    protected static function newFactory()
    {
        return TagFactory::new();
    }
}
