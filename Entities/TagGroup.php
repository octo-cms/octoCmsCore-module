<?php

namespace OctoCmsModule\Core\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use OctoCmsModule\Core\Factories\TagGroupFactory;

/**
 * OctoCmsModule\Core\Entities\TagGroup
 *
 * @property int $id
 * @property string $slug
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\Conner\Tagging\Model\Tag[] $tags
 * @property-read int|null $tags_count
 * @method static \Illuminate\Database\Eloquent\Builder|TagGroup newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TagGroup newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TagGroup query()
 * @method static \Illuminate\Database\Eloquent\Builder|TagGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TagGroup whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TagGroup whereSlug($value)
 * @mixin \Eloquent
 * @method static \OctoCmsModule\Core\Factories\TagGroupFactory factory(...$parameters)
 */
class TagGroup extends \Conner\Tagging\Model\TagGroup
{
    use HasFactory;

    /**
     * @return TagGroupFactory
     */
    protected static function newFactory()
    {
        return TagGroupFactory::new();
    }
}
