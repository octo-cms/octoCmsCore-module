<?php

namespace OctoCmsModule\Core\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OctoCmsModule\Core\Factories\CustomFieldEntityStringFactory;

/**
 * Class CustomFieldEntityString
 *
 * @package OctoCmsModule\Core\Entities
 * @property int $id
 * @property \OctoCmsModule\Core\Entities\CustomFieldEntity|null $value
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityString newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityString newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityString query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityString whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityString whereValue($value)
 * @mixin \Eloquent
 * @method static \OctoCmsModule\Core\Factories\CustomFieldEntityStringFactory factory(...$parameters)
 */
class CustomFieldEntityString extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'custom_field_entity_string';

    protected $fillable = ['value'];

    protected $casts = ['value' => 'string'];

    /**
     * @return CustomFieldEntityStringFactory
     */
    protected static function newFactory()
    {
        return CustomFieldEntityStringFactory::new();
    }

    /**
     * @param $value
     */
    public function setValueAttribute($value)
    {
        $this->attributes['value'] = gettype($value) === 'string' ? $value : null;
    }
}
