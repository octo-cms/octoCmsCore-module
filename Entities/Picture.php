<?php

namespace OctoCmsModule\Core\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use OctoCmsModule\Core\Factories\PictureFactory;
use OctoCmsModule\Core\Services\PictureService;

/**
 * OctoCmsModule\Core\Entities\Picture
 *
 * @property int                             $id
 * @property int|null                        $picturable_id
 * @property string|null                     $picturable_type
 * @property string                          $src
 * @property string                          $tag
 * @property string|null                     $webp
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Model|Eloquent             $picturable
 * @property-read Collection|PictureLang[]   $pictureLangs
 * @property-read int|null                   $picture_langs_count
 * @method static Builder|Picture newModelQuery()
 * @method static Builder|Picture newQuery()
 * @method static Builder|Picture query()
 * @method static Builder|Picture whereCreatedAt($value)
 * @method static Builder|Picture whereId($value)
 * @method static Builder|Picture wherePicturableId($value)
 * @method static Builder|Picture wherePicturableType($value)
 * @method static Builder|Picture whereSrc($value)
 * @method static Builder|Picture whereTag($value)
 * @method static Builder|Picture whereUpdatedAt($value)
 * @method static Builder|Picture whereWebp($value)
 * @mixin Eloquent
 * @method static \OctoCmsModule\Core\Factories\PictureFactory factory(...$parameters)
 */
class Picture extends Model
{
    use HasFactory;

    public const TAG_MAIN = 'main';

    protected $fillable = [
        'tag',
        'src',
        'webp',
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($picture) {
            if (Storage::disk('gcs')->exists($picture->src)) {
                Storage::disk('gcs')->delete($picture->src);
            }

            if (Storage::disk('gcs')->exists($picture->webp)) {
                Storage::disk('gcs')->delete($picture->webp);
            }
        });
    }

    /**
     * @return PictureFactory
     */
    protected static function newFactory()
    {
        return PictureFactory::new();
    }

    /**
     * @return HasMany
     */
    public function pictureLangs()
    {
        return $this->hasMany(PictureLang::class);
    }

    /**
     * @return MorphTo
     */
    public function picturable()
    {
        return $this->morphTo('picturable', 'picturable_type', 'picturable_id', 'id');
    }

    /**
     * @return string
     */
    public function getUrlSrc()
    {
        return PictureService::BUCKET_BASE_URL . '/' . env('GOOGLE_CLOUD_STORAGE_BUCKET') . '/' . $this->src;
    }

    /**
     * @return string
     */
    public function getUrlWebp()
    {
        return PictureService::BUCKET_BASE_URL . '/' . env('GOOGLE_CLOUD_STORAGE_BUCKET') . '/' . $this->webp;
    }
}
