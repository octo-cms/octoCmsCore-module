<?php

namespace OctoCmsModule\Core\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use OctoCmsModule\Core\Factories\CustomFieldEntityFactory;


/**
 * OctoCmsModule\Core\Entities\CustomFieldEntity
 *
 * @property int $id
 * @property int|null $custom_field_id
 * @property int|null $entity_id
 * @property string|null $entity_type
 * @property int|null $valuable_id
 * @property string|null $valuable_type
 * @property-read \OctoCmsModule\Core\Entities\CustomField|null $customField
 * @property-read Model|\Eloquent $entity
 * @property-read Model|\Eloquent $valuable
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntity newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntity newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntity query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntity whereCustomFieldId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntity whereEntityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntity whereEntityType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntity whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntity whereValuableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntity whereValuableType($value)
 * @mixin \Eloquent
 * @property int|null $value_id
 * @property string|null $value_type
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntity whereValueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntity whereValueType($value)
 * @method static \OctoCmsModule\Core\Factories\CustomFieldEntityFactory factory(...$parameters)
 */
class CustomFieldEntity extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'custom_field_entity';

    protected $fillable = [
        'custom_field_id',
        'entity_id',
        'entity_type',
        'valuable_id',
        'valuable_type',
    ];

    /**
     * @return CustomFieldEntityFactory
     */
    protected static function newFactory()
    {
        return CustomFieldEntityFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function customField()
    {
        return $this->belongsTo(CustomField::class);
    }


    /**
     * Name valuable
     *
     * @return MorphTo
     */
    public function valuable()
    {
        return $this->morphTo('valuable', 'valuable_type', 'valuable_id', 'id');
    }

    /**
     * Name entity
     *
     * @return MorphTo
     */
    public function entity()
    {
        return $this->morphTo('entity', 'entity_type', 'entity_id', 'id');
    }
}
