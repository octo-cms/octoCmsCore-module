<?php

namespace OctoCmsModule\Core\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use OctoCmsModule\Core\Factories\RegistryFactory;
use OctoCmsModule\Core\Traits\AddressableTrait;
use OctoCmsModule\Core\Traits\CustomFieldEntityTrait;
use OctoCmsModule\Core\Traits\EmailableTrait;
use OctoCmsModule\Core\Traits\PhoneableTrait;

/**
 * OctoCmsModule\Core\Entities\Registry
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $code
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \OctoCmsModule\Core\Entities\CompanyRegistry|null $companyRegistry
 * @property-read \Illuminate\Database\Eloquent\Collection|\OctoCmsModule\Core\Entities\CustomFieldEntity[] $customFieldRegistries
 * @property-read int|null $custom_field_registries_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\OctoCmsModule\Core\Entities\Email[] $emails
 * @property-read int|null $emails_count
 * @property-read \OctoCmsModule\Core\Entities\PrivateRegistry|null $privateRegistry
 * @method static \Illuminate\Database\Eloquent\Builder|Registry newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Registry newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Registry query()
 * @method static \Illuminate\Database\Eloquent\Builder|Registry whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Registry whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Registry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Registry whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Registry whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Registry whereUserId($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\OctoCmsModule\Core\Entities\CustomFieldEntity[] $customFieldEntities
 * @property-read int|null $custom_field_entities_count
 * @property-read \OctoCmsModule\Core\Entities\Email|null $defaultEmail
 * @property-read \Illuminate\Database\Eloquent\Collection|\OctoCmsModule\Core\Entities\Address[] $addresses
 * @property-read int|null $addresses_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\OctoCmsModule\Core\Entities\ProviderImportData[] $providerImportData
 * @property-read int|null $provider_import_data_count
 * @property-read \OctoCmsModule\Core\Entities\Phone|null $defaultPhone
 * @property-read \Illuminate\Database\Eloquent\Collection|\OctoCmsModule\Core\Entities\Phone[] $phones
 * @property-read int|null $phones_count
 * @property-read \OctoCmsModule\Core\Entities\Address|null $defaultAddress
 * @method static \Illuminate\Database\Eloquent\Builder|Registry whereCustomFields($customFields, string $operator, $query)
 * @method static \OctoCmsModule\Core\Factories\RegistryFactory factory(...$parameters)
 */
class Registry extends Model
{
    use HasFactory, EmailableTrait, CustomFieldEntityTrait, AddressableTrait, PhoneableTrait;

    public const TYPE_PRIVATE = 'private';
    public const TYPE_COMPANY = 'company';

    protected $fillable = ['code', 'type'];

    protected $table = 'registry';

    /**
     * @return RegistryFactory
     */
    protected static function newFactory()
    {
        return RegistryFactory::new();
    }

    /**
     * @return HasOne
     */
    public function companyRegistry()
    {
        return $this->hasOne(CompanyRegistry::class);
    }

    /**
     * @return HasOne
     */
    public function privateRegistry()
    {
        return $this->hasOne(PrivateRegistry::class);
    }

    /**
     * Name providerImportData
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function providerImportData()
    {
        return $this->hasMany(ProviderImportData::class);
    }
}
