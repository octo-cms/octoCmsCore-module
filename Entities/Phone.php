<?php

namespace OctoCmsModule\Core\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use OctoCmsModule\Core\Factories\PhoneFactory;

/**
 * OctoCmsModule\Core\Entities\Phone
 *
 * @property-read Model|\Eloquent $phoneable
 * @method static \Illuminate\Database\Eloquent\Builder|Phone newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Phone newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Phone query()
 * @mixin \Eloquent
 * @property int $id
 * @property int|null $phoneable_id
 * @property string|null $phoneable_type
 * @property string|null $label
 * @property string $phone
 * @property bool $default
 * @method static \Illuminate\Database\Eloquent\Builder|Phone whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phone whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phone whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phone wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phone wherePhoneableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Phone wherePhoneableType($value)
 * @method static \OctoCmsModule\Core\Factories\PhoneFactory factory(...$parameters)
 */
class Phone extends Model
{
    use HasFactory;

    protected $fillable = [
        'label',
        'phone',
        'default',
    ];

    protected $casts = [
        'default' => 'boolean',
    ];

    public $timestamps = false;

    /**
     * @return MorphTo
     */
    public function phoneable()
    {
        return $this->morphTo();
    }

    /**
     * @return string
     */
    protected static function newFactory()
    {
        return PhoneFactory::new();
    }
}
