<?php

namespace OctoCmsModule\Core\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OctoCmsModule\Core\Factories\CustomFieldEntityTextFactory;

/**
 * Class CustomFieldEntityText
 *
 * @package OctoCmsModule\Core\Entities
 * @property int $id
 * @property \OctoCmsModule\Core\Entities\CustomFieldEntity|null $value
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityText newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityText newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityText query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityText whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityText whereValue($value)
 * @mixin \Eloquent
 * @method static \OctoCmsModule\Core\Factories\CustomFieldEntityTextFactory factory(...$parameters)
 */
class CustomFieldEntityText extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'custom_field_entity_text';

    protected $fillable = ['value'];

    protected $casts = ['value' => 'string'];

    /**
     * @return CustomFieldEntityTextFactory
     */
    protected static function newFactory()
    {
        return CustomFieldEntityTextFactory::new();
    }

    /**
     * @param $value
     */
    public function setValueAttribute($value)
    {
        $this->attributes['value'] = gettype($value) === 'string' ? $value : null;
    }
}
