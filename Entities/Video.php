<?php

namespace OctoCmsModule\Core\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Carbon;
use OctoCmsModule\Core\Factories\VideoFactory;

/**
 * OctoCmsModule\Core\Entities\Video
 *
 * @property int                             $id
 * @property int|null                        $videable_id
 * @property string|null                     $videable_type
 * @property string                          $source_type
 * @property mixed                           $data
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Model|Eloquent             $videable
 * @property-read Collection|VideoLang[]     $videoLangs
 * @property-read int|null                   $video_langs_count
 * @method static Builder|Video newModelQuery()
 * @method static Builder|Video newQuery()
 * @method static Builder|Video query()
 * @method static Builder|Video whereCreatedAt($value)
 * @method static Builder|Video whereData($value)
 * @method static Builder|Video whereId($value)
 * @method static Builder|Video whereSourceType($value)
 * @method static Builder|Video whereUpdatedAt($value)
 * @method static Builder|Video whereVideableId($value)
 * @method static Builder|Video whereVideableType($value)
 * @mixin Eloquent
 * @method static \OctoCmsModule\Core\Factories\VideoFactory factory(...$parameters)
 */
class Video extends Model
{
    use HasFactory;
    public const VIDEABLE_TYPE_NEWS_CONTENT = 'NewsContent';

    protected $fillable = [
        'source_type',
        'data'
    ];

    /**
     * @return VideoFactory
     */
    protected static function newFactory()
    {
        return VideoFactory::new();
    }

    /**
     * @param $value
     *
     * @return void
     */
    public function setDataAttribute($value)
    {
        $this->attributes['data'] = serialize($value);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getDataAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @return MorphTo
     */
    public function videable()
    {
        return $this->morphTo('videable', 'videable_type', 'videable_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function videoLangs()
    {
        return $this->hasMany(VideoLang::class);
    }
}
