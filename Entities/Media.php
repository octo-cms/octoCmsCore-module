<?php

namespace OctoCmsModule\Core\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;
use OctoCmsModule\Core\Factories\MediaFactory;

/**
 * OctoCmsModule\Core\Entities\Media
 *
 * @property int                         $id
 * @property string                      $disk
 * @property string                      $directory
 * @property string                      $filename
 * @property string                      $extension
 * @property string                      $mime_type
 * @property string                      $aggregate_type
 * @property int                         $size
 * @property Carbon|null                 $created_at
 * @property Carbon|null                 $updated_at
 * @property-read string                 $basename
 * @property-read Collection|MediaLang[] $mediaLangs
 * @property-read int|null               $media_langs_count
 * @method static Builder|\Plank\Mediable\Media forPathOnDisk($disk, $path)
 * @method static Builder|\Plank\Mediable\Media inDirectory($disk, $directory, $recursive = false)
 * @method static Builder|\Plank\Mediable\Media inOrUnderDirectory($disk, $directory)
 * @method static Builder|Media newModelQuery()
 * @method static Builder|Media newQuery()
 * @method static Builder|Media query()
 * @method static Builder|\Plank\Mediable\Media unordered()
 * @method static Builder|Media whereAggregateType($value)
 * @method static Builder|\Plank\Mediable\Media whereBasename($basename)
 * @method static Builder|Media whereCreatedAt($value)
 * @method static Builder|Media whereDirectory($value)
 * @method static Builder|Media whereDisk($value)
 * @method static Builder|Media whereExtension($value)
 * @method static Builder|Media whereFilename($value)
 * @method static Builder|Media whereId($value)
 * @method static Builder|Media whereMimeType($value)
 * @method static Builder|Media whereSize($value)
 * @method static Builder|Media whereUpdatedAt($value)
 * @mixin Eloquent
 * @method static \OctoCmsModule\Core\Factories\MediaFactory factory(...$parameters)
 */
class Media extends \Plank\Mediable\Media
{
    use HasFactory;

    public const TAG_MAIN = 'main';

    /**
     * @return MediaFactory
     */
    protected static function newFactory()
    {
        return MediaFactory::new();
    }

    /**
     * @return HasMany
     */
    public function mediaLangs()
    {
        return $this->hasMany(MediaLang::class);
    }

    /**
     * Name findAggregateType
     * @param string $extension Extension
     *
     * @return string
     */
    public static function findAggregateType(string $extension): string
    {
        foreach (config('mediable.aggregate_types') as $type => $attributes) {
            if (in_array($extension, $attributes['extensions'])) {
                return $type;
            }
        }

        return '';
    }
}
