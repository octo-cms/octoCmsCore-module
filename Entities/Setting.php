<?php

namespace OctoCmsModule\Core\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OctoCmsModule\Core\Factories\SettingFactory;

/**
 * Class Setting
 *
 * @package OctoCmsModule\Core\Entities
 * @author danielepasi
 * @method static Builder|Setting newModelQuery()
 * @method static Builder|Setting newQuery()
 * @method static Builder|Setting query()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @property string $value
 * @method static Builder|Setting whereId($value)
 * @method static Builder|Setting whereName($value)
 * @method static Builder|Setting whereValue($value)
 * @method static \OctoCmsModule\Core\Factories\SettingFactory factory(...$parameters)
 */
class Setting extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'value'
    ];

    /**
     * @return SettingFactory
     */
    protected static function newFactory()
    {
        return SettingFactory::new();
    }

    /**
     * @param  $value
     * @return void
     */
    public function setValueAttribute($value)
    {
        $this->attributes['value'] = serialize($value);
    }

    /**
     * @param  $value
     * @return mixed
     */
    public function getValueAttribute($value)
    {
        return unserialize($value);
    }
}
