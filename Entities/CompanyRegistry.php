<?php

namespace OctoCmsModule\Core\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OctoCmsModule\Core\Factories\CompanyRegistryFactory;

/**
 * OctoCmsModule\Core\Entities\CompanyRegistry
 *
 * @property int $id
 * @property int $registry_id
 * @property string|null $businessname
 * @property string|null $referral
 * @property string|null $piva
 * @property string|null $codfis
 * @property string|null $web
 * @property string|null $company_form
 * @property-read \OctoCmsModule\Core\Entities\Registry $registry
 * @method static \Illuminate\Database\Eloquent\Builder|CompanyRegistry newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CompanyRegistry newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CompanyRegistry query()
 * @method static \Illuminate\Database\Eloquent\Builder|CompanyRegistry whereBusinessname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompanyRegistry whereCodfis($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompanyRegistry whereCompanyForm($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompanyRegistry whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompanyRegistry wherePiva($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompanyRegistry whereReferral($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompanyRegistry whereRegistryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CompanyRegistry whereWeb($value)
 * @mixin \Eloquent
 * @method static \OctoCmsModule\Core\Factories\CompanyRegistryFactory factory(...$parameters)
 */
class CompanyRegistry extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'company_registry';

    protected $fillable = [
        'businessname',
        'referral',
        'piva',
        'codfis',
        'web',
        'company_form',
    ];

    /**
     * @return CompanyRegistryFactory
     */
    protected static function newFactory()
    {
        return CompanyRegistryFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function registry()
    {
        return $this->belongsTo(Registry::class);
    }
}
