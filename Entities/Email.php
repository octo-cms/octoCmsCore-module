<?php

namespace OctoCmsModule\Core\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use OctoCmsModule\Core\Factories\EmailFactory;

/**
 * OctoCmsModule\Core\Entities\Email
 *
 * @property-read Model|Eloquent                                                                                            $emailable
 * @method static Builder|Email newModelQuery()
 * @method static Builder|Email newQuery()
 * @method static Builder|Email query()
 * @mixin Eloquent
 * @property int                                                                                                            $id
 * @property int|null                                                                                                       $emailable_id
 * @property string|null                                                                                                    $emailable_type
 * @property string|null                                                                                                    $label
 * @property string                                                                                                         $email
 * @property bool                                                                                                           $default
 * @method static Builder|Email whereDefault($value)
 * @method static Builder|Email whereEmail($value)
 * @method static Builder|Email whereEmailableId($value)
 * @method static Builder|Email whereEmailableType($value)
 * @method static Builder|Email whereId($value)
 * @method static Builder|Email whereLabel($value)
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null                                                                                                  $notifications_count
 * @method static \OctoCmsModule\Core\Factories\EmailFactory factory(...$parameters)
 */
class Email extends Model
{
    use HasFactory, Notifiable;

    public const EMAILABLE_TYPE_REGISTRY = 'Registry';

    protected $fillable = [
        'emailable_id',
        'emailable_type',
        'default',
        'label',
        'email',
    ];

    protected $casts = [
        'default' => 'boolean',
    ];

    public $timestamps = false;

    /**
     * @return MorphTo
     */
    public function emailable()
    {
        return $this->morphTo();
    }

    /**
     * @return string
     */
    protected static function newFactory()
    {
        return EmailFactory::new();
    }

    /**
     * Name getEmailAttribute
     * @param $value
     *
     * @return string
     */
    public function getEmailAttribute($value)
    {
        return Str::lower($value);
    }

    /**
     * Name setEmailAttribute
     * @param $value
     *
     * @return void
     */
    public function setEmailAttribute($value)
    {
        $this->attributes['email']=Str::lower($value);
    }

}
