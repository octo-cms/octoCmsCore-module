<?php

namespace OctoCmsModule\Core\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use OctoCmsModule\Core\Factories\VideoLangFactory;

/**
 * OctoCmsModule\Core\Entities\VideoLang
 *
 * @property int                             $id
 * @property int                             $video_id
 * @property string                          $lang
 * @property string|null                     $alt
 * @property string|null                     $caption
 * @property string|null                     $title
 * @property string|null                     $description
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Video                      $video
 * @method static Builder|VideoLang newModelQuery()
 * @method static Builder|VideoLang newQuery()
 * @method static Builder|VideoLang query()
 * @method static Builder|VideoLang whereAlt($value)
 * @method static Builder|VideoLang whereCaption($value)
 * @method static Builder|VideoLang whereCreatedAt($value)
 * @method static Builder|VideoLang whereDescription($value)
 * @method static Builder|VideoLang whereId($value)
 * @method static Builder|VideoLang whereLang($value)
 * @method static Builder|VideoLang whereTitle($value)
 * @method static Builder|VideoLang whereUpdatedAt($value)
 * @method static Builder|VideoLang whereVideoId($value)
 * @mixin Eloquent
 * @method static \OctoCmsModule\Core\Factories\VideoLangFactory factory(...$parameters)
 */
class VideoLang extends Model
{
    use HasFactory;

    protected $fillable = [
        'lang',
        'alt',
        'title',
        'caption',
        'description',
    ];

    /**
     * @return VideoLangFactory
     */
    protected static function newFactory()
    {
        return VideoLangFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function video()
    {
        return $this->belongsTo(Video::class);
    }
}
