<?php

namespace OctoCmsModule\Core\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OctoCmsModule\Core\Factories\CustomFieldFactory;

/**
 * Class CustomField
 *
 * @package OctoCmsModule\Core\Entities
 * @property int $id
 * @property string $name
 * @property string $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\OctoCmsModule\Core\Entities\CustomFieldEntity[] $customFieldRegistries
 * @property-read int|null $custom_field_registries_count
 * @method static \Illuminate\Database\Eloquent\Builder|CustomField newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomField newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomField query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomField whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomField whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomField whereType($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\OctoCmsModule\Core\Entities\CustomFieldEntity[] $customFieldEntities
 * @property-read int|null $custom_field_entities_count
 * @method static \OctoCmsModule\Core\Factories\CustomFieldFactory factory(...$parameters)
 */
class CustomField extends Model
{
    use HasFactory;

    public const TYPE_STRING = 'string';
    public const TYPE_TEXT = 'text';
    public const TYPE_DECIMAL = 'decimal';
    public const TYPE_NUMERIC = 'numeric';
    public const TYPE_DATE = 'date';

    protected $fillable = [
        'name',
        'type'
    ];

    public $timestamps = false;

    /**
     * @return CustomFieldFactory
     */
    protected static function newFactory()
    {
        return CustomFieldFactory::new();
    }

    /**
     * @return HasMany
     */
    public function customFieldEntities()
    {
        return $this->hasMany(CustomFieldEntity::class);
    }
}
