<?php

namespace OctoCmsModule\Core\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OctoCmsModule\Core\Factories\UserEventLogFactory;

/**
 * OctoCmsModule\Core\Entities\UserEventLog
 *
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property string|null $browser
 * @property mixed $browser_langs
 * @property string|null $device
 * @property string|null $device_type
 * @property string|null $platform
 * @property string|null $ip_address
 * @property mixed $data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \OctoCmsModule\Core\Entities\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|UserEventLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserEventLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserEventLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserEventLog whereBrowser($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEventLog whereBrowserLangs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEventLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEventLog whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEventLog whereDevice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEventLog whereDeviceType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEventLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEventLog whereIpAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEventLog wherePlatform($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEventLog whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEventLog whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserEventLog whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $is_desktop
 * @method static \Illuminate\Database\Eloquent\Builder|UserEventLog whereIsDesktop($value)
 * @method static \OctoCmsModule\Core\Factories\UserEventLogFactory factory(...$parameters)
 */
class UserEventLog extends Model
{
    use HasFactory;

    public const EVENT_LOGIN = 'login';
    public const EVENT_LOGOUT = 'logout';

    protected $fillable = [
        'ip_address',
        'browser',
        'browser_langs',
        'type',
        'platform',
        'data',
        'device',
        'device_type',
    ];

    /**
     * @param $value
     *
     * @return void
     */
    public function setDataAttribute($value)
    {
        $this->attributes['data'] = serialize($value);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getDataAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @param $value
     *
     * @return void
     */
    public function setBrowserLangsAttribute($value)
    {
        $this->attributes['browser_langs'] = serialize($value);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getBrowserLangsAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * @return UserEventLogFactory
     */
    protected static function newFactory(): UserEventLogFactory
    {
        return UserEventLogFactory::new();
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
