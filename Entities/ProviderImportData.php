<?php

namespace OctoCmsModule\Core\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OctoCmsModule\Core\Factories\ProviderImportDataFactory;


/**
 * OctoCmsModule\Core\Entities\ProviderImportData
 *
 * @property int $id
 * @property mixed $date
 * @property int|null $provider_import_id
 * @property int|null $provider_id
 * @property int|null $registry_id
 * @property string|null $registry_type
 * @property string|null $name
 * @property string|null $surname
 * @property string|null $email
 * @property string $price
 * @property mixed $data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \OctoCmsModule\Core\Entities\Provider|null $provider
 * @property-read \OctoCmsModule\Core\Entities\ProviderImport|null $providerImport
 * @property-read \OctoCmsModule\Core\Entities\Registry|null $registry
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImportData newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImportData newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImportData query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImportData whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImportData whereData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImportData whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImportData whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImportData whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImportData whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImportData wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImportData whereProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImportData whereProviderImportId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImportData whereRegistryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImportData whereRegistryType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImportData whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImportData whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \OctoCmsModule\Core\Factories\ProviderImportDataFactory factory(...$parameters)
 */
class ProviderImportData extends Model
{
    use HasFactory;

    public const REGISTRY_TYPE_NEW = 'new';
    public const REGISTRY_TYPE_RECOVERY = 'recovery';

    protected $table = 'provider_import_data';

    protected $fillable = [
        'date',
        'name',
        'surname',
        'email',
        'registry_type',
        'price',
    ];

    protected $casts = [
        'date'    => 'date:Y-m-d',
    ];

    /**
     * Name newFactory
     *
     * @return ProviderImportDataFactory
     */
    protected static function newFactory()
    {
        return ProviderImportDataFactory::new();
    }

    /**
     * @param $value
     *
     * @return void
     */
    public function setDataAttribute($value)
    {
        $this->attributes['data'] = serialize($value);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getDataAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * Name providerImport
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function providerImport()
    {
        return $this->belongsTo(ProviderImport::class);
    }

    /**
     * Name registry
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function registry()
    {
        return $this->belongsTo(Registry::class);
    }

    /**
     * Name provider
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function provider()
    {
        return $this->belongsTo(Provider::class);
    }
}
