<?php

namespace OctoCmsModule\Core\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OctoCmsModule\Core\Factories\CustomFieldEntityDecimalFactory;

/**
 * Class CustomFieldEntityDecimal
 *
 * @package OctoCmsModule\Core\Entities
 * @property int $id
 * @property \OctoCmsModule\Core\Entities\CustomFieldEntity|null $value
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityDecimal newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityDecimal newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityDecimal query()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityDecimal whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CustomFieldEntityDecimal whereValue($value)
 * @mixin \Eloquent
 * @method static \OctoCmsModule\Core\Factories\CustomFieldEntityDecimalFactory factory(...$parameters)
 */
class CustomFieldEntityDecimal extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'custom_field_entity_decimal';

    protected $fillable = ['value'];

    protected $casts = ['value' => 'float'];

    /**
     * @return CustomFieldEntityDecimalFactory
     */
    protected static function newFactory()
    {
        return CustomFieldEntityDecimalFactory::new();
    }

    /**
     * @param $value
     */
    public function setValueAttribute($value)
    {
        $this->attributes['value'] = gettype($value) === 'double' ? $value : null;
    }
}
