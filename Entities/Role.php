<?php

namespace  OctoCmsModule\Core\Entities;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OctoCmsModule\Core\Factories\RoleFactory;

/**
 * Class Role
 *
 * @package OctoCmsModule\Core\Entities
 * @property int                                                  $id
 * @property string                                               $name
 * @property-read Collection|User[] $users
 * @property-read int|null                                        $users_count
 * @method static Builder|Role newModelQuery()
 * @method static Builder|Role newQuery()
 * @method static Builder|Role query()
 * @method static Builder|Role whereId($value)
 * @method static Builder|Role whereName($value)
 * @mixin Eloquent
 * @method static \OctoCmsModule\Core\Factories\RoleFactory factory(...$parameters)
 */
class Role extends Model
{
    use HasFactory;

    public const ROLE_ADMIN = 'admin';
    public const ROLE_CUSTOMER = 'customer';

    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    /**
     * @return RoleFactory
     */
    protected static function newFactory()
    {
        return RoleFactory::new();
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_role');
    }
}
