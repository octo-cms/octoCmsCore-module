<?php

namespace OctoCmsModule\Core\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;
use OctoCmsModule\Core\Factories\ProviderFactory;
use OctoCmsModule\Core\Factories\ProviderImportFactory;



/**
 * OctoCmsModule\Core\Entities\ProviderImport
 *
 * @property int $id
 * @property string $status
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\OctoCmsModule\Core\Entities\ProviderImportData[] $providerImportData
 * @property-read int|null $provider_import_data_count
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImport newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImport newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImport query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImport whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImport whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImport whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImport whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property mixed $errors
 * @method static \Illuminate\Database\Eloquent\Builder|ProviderImport whereErrors($value)
 * @method static \OctoCmsModule\Core\Factories\ProviderImportFactory factory(...$parameters)
 */
class ProviderImport extends Model
{
    use HasFactory;

    public const STATUS_PENDING = 'pending';
    public const STATUS_LOADED = 'loaded';
    public const STATUS_SYNCED = 'synced';
    public const STATUS_REGISTRY_SYNCING = 'registry-syncing';
    public const STATUS_COMPLETED = 'completed';
    public const STATUS_ERROR = 'error';

    protected $table = 'provider_imports';

    protected $fillable = [
        'status',
        'errors',
    ];

    /**
     * Name newFactory
     *
     * @return ProviderImportFactory
     */
    protected static function newFactory()
    {
        return ProviderImportFactory::new();
    }

    /**
     * @param $value
     *
     * @return void
     */
    public function setErrorsAttribute($value)
    {
        $this->attributes['errors'] = serialize($value);
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function getErrorsAttribute($value)
    {
        return unserialize($value);
    }

    /**
     * Name providerImportData
     *
     * @return HasMany
     */
    public function providerImportData()
    {
        return $this->hasMany(ProviderImportData::class);
    }

}
