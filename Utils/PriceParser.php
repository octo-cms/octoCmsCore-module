<?php

namespace OctoCmsModule\Core\Utils;

class PriceParser
{
    public static function parse($price)
    {

        if (is_numeric($price)) {
            return $price;
        }

        if (preg_match('/^[0-9]+/', $price, $matches) &&
            $matches[0] === $price) {   //125
            return intval($price);
        }

        if (preg_match('/\b[0-9]+,[0-9]{0,2}\b/', $price, $matches) &&
            $matches[0] === $price) {   //  125,00
            $price = str_replace(',', '.', $price);
            return floatval($price);
        }

        if (preg_match('/\b[0-9]+[.][0-9]+,[0-9]{0,2}\b/', $price, $matches) &&
            $matches[0] === $price) {   // 1.250,00
            $price = str_replace('.', '', $price);
            $price = str_replace(',', '.', $price);

            return floatval($price);
        }

        if (preg_match('/\b[0-9]+[.][0-9]{0,2}\b/', $price, $matches) &&
            $matches[0] === $price) {   //125.00
            return floatval($price);
        }

        return null;
    }
}
