<?php

namespace OctoCmsModule\Core\Utils;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

/**
 * Class LanguageUtils
 *
 * @package OctoCmsModule\Core\Utils
 */
class LanguageUtils
{
    /**
     * @param Collection  $languages
     * @param string      $attribute
     * @param string|null $language
     *
     * @return string
     */
    public static function getLangValue(Collection $languages, string $attribute, string $language = null)
    {

        if (empty($language)) {
            $language = app()->getLocale();
        }

        $item = $languages->where('lang', '=', $language)->first();

        return Arr::get($item, $attribute, null);
    }
}
