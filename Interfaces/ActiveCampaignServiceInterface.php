<?php

namespace OctoCmsModule\Core\Interfaces;

use OctoCmsModule\Core\DTO\ContactFormDataDTO;

/**
 * Interface ActiveCampaignServiceInterface
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Interfaces
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
interface ActiveCampaignServiceInterface
{
    /**
     * Name createContact
     *
     * @param ContactFormDataDTO $contactFormDataDTO ContactFormDataDTO
     *
     * @return array|null
     */
    public function createContact(ContactFormDataDTO $contactFormDataDTO):? array;

    /**
     * Name subscribeContactToList
     *
     * @param array|null $contact Contact from Active Campaign
     *
     * @return bool
     */
    public function subscribeContactToList(?array $contact): bool;
}
