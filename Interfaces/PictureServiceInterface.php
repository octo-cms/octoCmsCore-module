<?php

namespace OctoCmsModule\Core\Interfaces;

/**
 * Interface MenuServiceInterface
 *
 * @package OctoCmsModule\Core\Interfaces
 */
interface PictureServiceInterface
{

    /**
     * @param  array  $pictureFields
     * @param  string $path
     * @return array|null
     */
    public function uploadPicture(array $pictureFields, string $path): ?array;

    /**
     * @param  mixed $entity
     * @param  array $pictures
     * @return bool
     */
    public function savePicturesEntity($entity, array $pictures): bool;
}
