<?php

namespace OctoCmsModule\Core\Interfaces;

use OctoCmsModule\Core\Entities\User;

/**
 * Interface UserServiceInterface
 *
 * @package OctoCmsModule\Core\Interfaces
 */
interface UserServiceInterface
{

    /**
     * @param User  $user
     * @param array $fields
     *
     * @return User
     */
    public function saveUser(User $user, array $fields): User;

    /**
     * @param User $user
     *
     * @return User
     */
    public function generateNewPassword(User $user): User;

    /**
     * Name saveUserEventLog
     * @param User   $user
     * @param string $type
     * @param array  $fields
     *
     * @return void
     */
    public function saveUserEventLog(User $user, string $type, array $fields = []): void;

    /**
     * Name saveCustomFields
     *
     * @param User  $user         User
     * @param array $customFields array
     *
     * @return void
     */
    public function saveCustomFields(User $user, array $customFields);
}
