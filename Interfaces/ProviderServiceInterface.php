<?php

namespace OctoCmsModule\Core\Interfaces;

use Illuminate\Http\UploadedFile;
use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Core\Entities\ProviderImport;

/**
 * Interface ProviderServiceInterface
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Interfaces
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
interface ProviderServiceInterface
{
    /**
     * Name saveProvider
     *
     * @param Provider $provider Provider
     * @param array    $fields   Fields
     *
     * @return Provider
     */
    public function saveProvider(Provider $provider, array $fields): Provider;

    /**
     * Name uploadImport
     *
     * @param UploadedFile $file    UploadedFile
     * @param array        $options Array
     *
     * @return ProviderImport
     */
    public function uploadImport(UploadedFile $file, array $options = []): ProviderImport;

    /**
     * Name parseProviderImport
     *
     * @param ProviderImport $providerImport ProviderImport
     * @param string         $filePath       File Path
     *
     * @return mixed
     */
    public function parseProviderImport(ProviderImport $providerImport, string $filePath);

    /**
     * Name syncProviderImport
     *
     * @param ProviderImport $providerImport ProviderImport
     *
     * @return mixed
     */
    public function syncProviderImport(ProviderImport $providerImport);

    /**
     * Name syncProviderImportWithRegistry
     *
     * @param ProviderImport $providerImport ProviderImport
     *
     * @return mixed
     */
    public function syncProviderImportWithRegistry(ProviderImport $providerImport);
}
