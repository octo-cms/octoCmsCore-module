<?php

namespace OctoCmsModule\Core\Interfaces;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection as CollectionEloquent;
use Illuminate\Support\Collection;

/**
 * Interface EntityIdsServiceInterface
 *
 * @package OctoCmsModule\Core\Interfaces
 */
interface EntityIdsServiceInterface
{
    /**
     * @param Builder $items
     * @param array   $fields
     * @return mixed
     */
    public function getEntityIdsDTO(Builder $items, array $fields = []);

    /**
     * @param CollectionEloquent|Collection $collection
     * @param string                        $key
     * @param string                        $name
     * @param string                        $description
     * @return mixed
     */
    public function parseCollection($collection, string $key, string $name, string $description);

    /**
     * @param             $collection
     * @param string      $langKey
     * @param string      $key
     * @param string      $name
     * @param string|null $description
     * @return array
     */
    public function parseMultiLangCollection(
        $collection,
        string $langKey,
        string $key,
        string $name,
        string $description = null
    );
}
