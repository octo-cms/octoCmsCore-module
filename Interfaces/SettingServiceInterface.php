<?php

namespace OctoCmsModule\Core\Interfaces;

/**
 * Interface SettingServiceInterface
 *
 * @package OctoCmsModule\Core\Interfaces
 */
interface SettingServiceInterface
{
    /**
     * @param string $name
     * @param bool   $enableCache
     *
     * @return mixed
     */
    public function getSettingByName(string $name, bool $enableCache = true);

    /**
     * @param array $fields
     */
    public function saveSettings(array $fields);
}
