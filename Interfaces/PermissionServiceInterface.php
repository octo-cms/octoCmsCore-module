<?php

declare(strict_types=1);

namespace OctoCmsModule\Core\Interfaces;

use OctoCmsModule\Core\Entities\User;

/**
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Interfaces
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 */
interface PermissionServiceInterface
{
    /**
     * Name isAllowed
     *
     * @param User  $user  User
     * @param mixed $roles Role to check
     */
    public function isAllowed(User $user, $roles): bool;

    /**
     * Name getUserRoles
     *
     * @param User $user User
     *
     * @return array|mixed[]
     */
    public function getUserRoles(User $user): array;
}
