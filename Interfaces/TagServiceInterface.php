<?php

namespace OctoCmsModule\Core\Interfaces;

use OctoCmsModule\Core\Entities\Tag;

/**
 * Interface TagServiceInterface
 *
 * @package OctoCmsModule\Core\Interfaces
 */
interface TagServiceInterface
{
    /**
     * @param Tag    $tag
     * @param string $group
     *
     * @return Tag
     */
    public function saveTag(Tag $tag, string $group): Tag;
}
