<?php

namespace OctoCmsModule\Core\Interfaces;

use OctoCmsModule\Core\DTO\ContactFormDataDTO;
use OctoCmsModule\Core\Entities\Registry;

/**
 * Interface RegistryServiceInterface
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Interfaces
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
interface RegistryServiceInterface
{

    /**
     * Name sendEmailToAdmin
     *
     * @param ContactFormDataDTO $contactFormDataDTO ContactFormDataDTO
     *
     * @return mixed
     */
    public function sendEmailToAdmin(ContactFormDataDTO $contactFormDataDTO);

    /**
     * Name saveRegistry
     *
     * @param Registry $registry Registry
     * @param array    $fields   array
     *
     * @return mixed
     */
    public function saveRegistry(Registry $registry, array $fields): Registry;

    /**
     * Name updatePersonalData
     *
     * @param Registry $registry Registry
     * @param array    $fields   array
     *
     * @return mixed
     */
    public function updatePersonalData(Registry $registry, array $fields): Registry;
}
