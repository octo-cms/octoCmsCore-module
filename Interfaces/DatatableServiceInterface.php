<?php

namespace OctoCmsModule\Core\Interfaces;

use Illuminate\Database\Eloquent\Builder;

/**
 * Interface DatatableServiceInterface
 *
 * @package OctoCmsModule\Core\Interfaces
 */
interface DatatableServiceInterface
{

    /**
     * @param  array   $fields
     * @param  Builder $items
     * @return mixed
     */
    public function getDatatableDTO(array $fields, Builder $items);
}
