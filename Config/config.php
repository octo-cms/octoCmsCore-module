<?php

return [
    'name'  => 'Core',
    'admin' => [
        'sidebar' => [
            [
                'order'  => 1,
                'label'  => 'registry',
                'childs' => [
                    [
                        'label' => 'list',
                        'route' => 'admin.vue-route.core.registry'
                    ],
                    [
                        'label' => 'import',
                        'route' => 'admin.vue-route.core.registry'
                    ],
                    [
                        'label' => 'users',
                        'route' => 'admin.vue-route.core.users'
                    ]
                ]
            ]
        ]
    ]
];
