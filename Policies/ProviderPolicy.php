<?php

namespace OctoCmsModule\Core\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Interfaces\PermissionServiceInterface;

/**
 * Class ProviderPolicy
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Policies
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class ProviderPolicy
{
    use HandlesAuthorization;

    protected $permissionService;

    /**
     * ProviderPolicy constructor.
     *
     * @param PermissionServiceInterface $permissionService PermissionServiceInterface
     */
    public function __construct(PermissionServiceInterface $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    /**
     * Name index
     *
     * @param User $user User
     *
     * @return bool
     */
    public function index(User $user)
    {
        return true;
    }

    /**
     * Name show
     *
     * @param User     $user     User
     * @param Provider $provider Provider
     *
     * @return bool
     */
    public function show(User $user, Provider $provider)
    {
        return true;
    }

    /**
     * Name store
     *
     * @param User $user User
     *
     * @return bool
     */
    public function store(User $user)
    {
        return true;
    }

    /**
     * Name update
     *
     * @param User     $user     User
     * @param Provider $provider Provider
     *
     * @return bool
     */
    public function update(User $user, Provider $provider)
    {
        return true;
    }

    /**
     * Name datatableIndex
     *
     * @param User $user User
     *
     * @return bool
     */
    public function datatableIndex(User $user)
    {
        return true;
    }
}
