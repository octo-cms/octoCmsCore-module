<?php

namespace OctoCmsModule\Core\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Interfaces\PermissionServiceInterface;

/**
 * Class RolePolicy
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Core\Policies
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class RolePolicy
{
    use HandlesAuthorization;

    protected $permissionService;

    /**
     * RolePolicy constructor.
     * @param PermissionServiceInterface $permissionService PermissionServiceInterface
     */
    public function __construct(PermissionServiceInterface $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    /**
     * Name index
     * @param User $user
     *
     * @return bool
     */
    public function index(User $user)
    {
        return true;
    }
}
