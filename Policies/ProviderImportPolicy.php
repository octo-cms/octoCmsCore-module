<?php

namespace OctoCmsModule\Core\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Interfaces\PermissionServiceInterface;

/**
 * Class ProviderImportPolicy
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Policies
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class ProviderImportPolicy
{
    use HandlesAuthorization;

    protected $permissionService;

    /**
     * ProviderImportPolicy constructor.
     *
     * @param PermissionServiceInterface $permissionService PermissionServiceInterface
     */
    public function __construct(PermissionServiceInterface $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    /**
     * Name upload
     *
     * @param User $user User
     *
     * @return bool
     */
    public function upload(User $user)
    {
        return true;
    }

    /**
     * Name show
     *
     * @param User           $user           User
     * @param ProviderImport $providerImport ProviderImport
     *
     * @return bool
     */
    public function show(User $user, ProviderImport $providerImport)
    {
        return true;
    }

    /**
     * Name syncWithRegistry
     *
     * @param User           $user           User
     * @param ProviderImport $providerImport ProviderImport
     *
     * @return bool
     */
    public function syncWithRegistry(User $user, ProviderImport $providerImport)
    {
        return true;
    }

    /**
     * Name datatableProviderImportData
     *
     * @param User $user User
     *
     * @return bool
     */
    public function datatableProviderImportData(User $user)
    {
        return true;
    }

    /**
     * Name datatableProviderImport
     *
     * @param User $user User
     *
     * @return bool
     */
    public function datatableIndex(User $user)
    {
        return true;
    }

    /**
     * Name destroy
     *
     * @param User           $user           user
     * @param ProviderImport $providerImport providerImport
     *
     * @return bool
     */
    public function destroy(User $user, ProviderImport $providerImport)
    {
        return true;
    }
}
