<?php

namespace OctoCmsModule\Core\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Interfaces\PermissionServiceInterface;

/**
 * Class UserPolicy
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Policies
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class UserPolicy
{
    use HandlesAuthorization;

    protected $permissionService;

    /**
     * UserPolicy constructor.
     *
     * @param PermissionServiceInterface $permissionService PermissionServiceInterface
     */
    public function __construct(PermissionServiceInterface $permissionService)
    {
        $this->permissionService = $permissionService;
    }

    /**
     * Name store
     *
     * @param User $user User
     *
     * @return bool
     */
    public function store(User $user): bool
    {
        return true;
    }

    /**
     * Name datatableIndex
     *
     * @param User $user User
     *
     * @return bool
     */
    public function datatableIndex(User $user): bool
    {
        return true;
    }

    /**
     * Name update
     *
     * @param User $user   User
     * @param User $target Target
     *
     * @return bool
     */
    public function update(User $user, User $target): bool
    {
        return true;
    }

    /**
     * Name show
     *
     * @param User $user   User
     * @param User $target Target
     *
     * @return bool
     */
    public function show(User $user, User $target): bool
    {
        return true;
    }

    /**
     * Name update
     *
     * @param User $user   User
     * @param User $target Target
     *
     * @return bool
     */
    public function generateNewPassword(User $user, User $target): bool
    {
        return true;
    }

    /**
     * Name userEventLogDatatableIndexAccesses
     *
     * @param User $user User
     *
     * @return bool
     */
    public function userEventLogDatatableIndexAccesses(User $user): bool
    {
        return true;
    }

    /**
     * Name indexByRole
     *
     * @param User $user User
     *
     * @return bool
     */
    public function indexByRole(User $user): bool
    {
        return true;
    }
}
