<?php

namespace OctoCmsModule\Core\Jobs;

use Google\Cloud\Logging\Logger;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Octopus\Logger\DTOs\LogMessageDTO;
use Octopus\Logger\Services\LoggingServiceInterface;

/**
 * Class LoggingJob
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Jobs
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class LoggingJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /** @var LogMessageDTO */
    private $logMessageDTO;
    /** @var int */
    private $severity;
    /** @var string */
    private $location;
    /** @var string */
    private $namespace;

    /**
     * LoggingJob constructor.
     * @param LogMessageDTO $logMessageDTO LogMessageDTO
     * @param array         $options       Options
     */
    public function __construct(LogMessageDTO $logMessageDTO, array $options = [])
    {
        $severity = Logger::INFO;
        $location = config('octopus-logger.resource.labels.location');
        $namespace = config('octopus-logger.resource.labels.namespace');
        extract($options, EXTR_IF_EXISTS);

        $this->logMessageDTO = $logMessageDTO;
        $this->severity = $severity;
        $this->location = $location;
        $this->namespace = $namespace;
    }

    /**
     * Name handle
     * @param LoggingServiceInterface $loggingService LoggingServiceInterface
     *
     * @return void
     */
    public function handle(LoggingServiceInterface $loggingService)
    {
        $loggingService->writeLog($this->logMessageDTO, [
            'severity'  => $this->severity,
            'location'  => $this->location,
            'namespace' => $this->namespace,
        ]);
    }
}
