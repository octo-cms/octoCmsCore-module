<?php

namespace OctoCmsModule\Core\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use OctoCmsModule\Core\DTO\ContactFormDataDTO;
use OctoCmsModule\Core\Interfaces\ActiveCampaignServiceInterface;

/**
 * Class CreateActiveCampaignJob
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Jobs
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class CreateActiveCampaignJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * ContactFormDataDTO
     *
     * @var ContactFormDataDTO
     */
    protected $contactFormDataDTO;

    /**
     * CreateActiveCampaignJob constructor.
     *
     * @param ContactFormDataDTO $contactFormDataDTO ContactFormDataDTO
     */
    public function __construct(ContactFormDataDTO $contactFormDataDTO)
    {
        $this->contactFormDataDTO = $contactFormDataDTO;
    }

    /**
     * Name handle
     *
     * @param ActiveCampaignServiceInterface $activeCampaignService ActiveCampaignServiceInterface
     *
     * @return void
     */
    public function handle(ActiveCampaignServiceInterface $activeCampaignService)
    {
        $contact = $activeCampaignService->createContact($this->contactFormDataDTO);

        $activeCampaignService->subscribeContactToList($contact);
    }
}
