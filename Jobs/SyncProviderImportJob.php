<?php

namespace OctoCmsModule\Core\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Interfaces\ProviderServiceInterface;

/**
 * Class CreateActiveCampaignJob
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Jobs
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class SyncProviderImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    /**
     * ProviderImport
     *
     * @var ProviderImport
     */
    protected $providerImport;

    /**
     * SyncProviderImportJob constructor.
     *
     * @param ProviderImport $providerImport ProviderImport
     */
    public function __construct(ProviderImport $providerImport)
    {
        $this->providerImport = $providerImport;
    }

    /**
     * Name handle
     *
     * @param ProviderServiceInterface $providerService ProviderServiceInterface
     *
     * @return void
     */
    public function handle(ProviderServiceInterface $providerService)
    {
        $providerService->syncProviderImport($this->providerImport);
    }
}
