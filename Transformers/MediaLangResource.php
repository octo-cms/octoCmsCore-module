<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class MediaLangResource
 *
 * @package OctoCmsModule\Core\Transformers
 */
class MediaLangResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'media_id'    => $this->media_id,
            'alt'         => $this->alt,
            'caption'     => $this->caption,
            'title'       => $this->title,
            'description' => $this->description,
            'lang'        => $this->lang,
        ];
    }
}
