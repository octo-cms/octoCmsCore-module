<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class AddressResource
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Transformers
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class AddressResource extends JsonResource
{
    /**
     * Name toArray
     *
     * @param Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'default'                     => $this->default,
            'alias'                       => $this->alias,
            'street_address'              => $this->street_address,
            'administrative_area_level_1' => $this->administrative_area_level_1,
            'administrative_area_level_2' => $this->administrative_area_level_2,
            'locality'                    => $this->locality,
            'postal_code'                 => $this->postal_code,
            'country'                     => $this->country,
        ];
    }
}
