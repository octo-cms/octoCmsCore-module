<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PhoneResource
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Core\Transformers
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class PhoneResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'phone'   => $this->phone,
            'default' => $this->default,
            'label'   => $this->label,
        ];
    }
}
