<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class VideoResource
 *
 * @package OctoCmsModule\Core\Transformers
 */
class VideoResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'data'        => $this->data,
            'source_type' => $this->source_type,
            'videoLangs'  => VideoLangResource::collection($this->whenLoaded('videoLangs')),
        ];
    }
}
