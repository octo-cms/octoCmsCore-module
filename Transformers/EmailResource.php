<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class EmailResource
 *
 * @package OctoCmsModule\Core\Transformers
 */
class EmailResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'email'   => $this->email,
            'default' => $this->default,
            'label'   => $this->label,
        ];
    }
}
