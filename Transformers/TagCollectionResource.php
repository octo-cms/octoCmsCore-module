<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TagCollectionResource
 *
 * @package OctoCmsModule\Core\Transformers
 */
class TagCollectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'slug'    => optional($this)->slug,
            'name'    => optional($this)->name,
            'count'   => optional($this)->count,
            'suggest' => optional($this)->suggest,
        ];
    }
}
