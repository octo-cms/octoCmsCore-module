<?php

declare(strict_types=1);

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @package OctoCmsModule\Core\Transformers
 */
class CountryResource extends JsonResource
{
    //phpcs:disable
    /**
     * @return array|mixed[]
     */
    public function toArray($request): array
    {
        return parent::toArray($request);
    }
    //phpcs:enable
}
