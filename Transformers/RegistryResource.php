<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use OctoCmsModule\Core\Traits\ParseCustomFieldsResourceTrait;

/**
 * Class RegistryResource
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Transformers
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class RegistryResource extends JsonResource
{
    use ParseCustomFieldsResourceTrait;

    /**
     * Name toArray
     *
     * @param Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'              => $this->id,
            'code'            => $this->code,
            'type'            => $this->type,
            'privateRegistry' => new PrivateRegistryResource($this->whenLoaded('privateRegistry')),
            'companyRegistry' => new CompanyRegistryResource($this->whenLoaded('companyRegistry')),
            'emails'          => EmailResource::collection($this->whenLoaded('emails')),
            'phones'          => PhoneResource::collection($this->whenLoaded('phones')),
            'addresses'       => AddressResource::collection($this->whenLoaded('addresses')),
            'defaultEmail'  => new EmailResource($this->whenLoaded('defaultEmail')),
            'defaultPhone'  => new PhoneResource($this->whenLoaded('defaultPhone')),
            $this->mergeWhen(
                $this->relationLoaded('customFieldEntities'),
                [
                    'customFields' => $this->parseCustomFields()
                ]
            ),
        ];
    }
}
