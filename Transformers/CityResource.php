<?php

declare(strict_types=1);

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @package OctoCmsModule\Core\Transformers
 */
class CityResource extends JsonResource
{
    //phpcs:disable
    /**
     * @return array|mixed[]
     */
    public function toArray($request): array
    {

        return [
            'denominazione'           => $this->denominazione,
            'cod_comune_alfanumerico' => $this->cod_comune_alfanumerico,
            'sovracomunale'           => $this->sovracomunale,
            'sigla_automobilistica'   => $this->sigla_automobilistica,
            'regione'                 => $this->regione,
            'selectData'              => [
                'name'  => $this->denominazione,
                'value' => $this->cod_comune_alfanumerico,
            ],
        ];

    }

    //phpcs:enable
}
