<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ImportTempResource
 *
 * @package OctoCmsModule\Core\Transformers
 */
class ImportTempResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'provider_id' => $this->provider_id,
            'registry_id' => $this->registry_id,
            'data'        => $this->data,
            'registry'    => new RegistryResource($this->whenLoaded('registry')),
            'provider'    => new ProviderResource($this->whenLoaded('provider')),
        ];
    }
}
