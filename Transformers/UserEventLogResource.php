<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UserEventLogResource
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Transformers
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2021
 * @link     https://octopus.srl
 */
class UserEventLogResource extends JsonResource
{
    /**
     * Name toArray
     *
     * @param Request $request Request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'           => $this->id,
            'browser'      => $this->browser,
            'browserLangs' => $this->browser_langs,
            'data'         => $this->data,
            'device'       => $this->device,
            'deviceType'   => $this->device_type,
            'ipAddress'    => $this->ip_address,
            'platform'     => $this->platform,
            'type'         => $this->type,
            'date'         => optional($this->created_at)->format('Y-m-d'),
            'time'         => optional($this->created_at)->format('H:i:s'),
        ];
    }
}
