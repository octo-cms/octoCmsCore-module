<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PrivateRegistryResource
 *
 * @package OctoCmsModule\Core\Transformers
 */
class PrivateRegistryResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'          => $this->id,
            'registry_id' => $this->registry_id,
            'codfis'      => $this->codfis,
            'name'        => $this->name,
            'surname'     => $this->surname,
            'gender'      => $this->gender,
            'birth_date'  => optional($this->birth_date)->format('Y-m-d'),
        ];
    }
}
