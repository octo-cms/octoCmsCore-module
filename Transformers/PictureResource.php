<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class MediaResource
 *
 * @package OctoCmsModule\Core\Transformers
 */
class PictureResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'src'          => $this->src,
            'tag'          => $this->tag,
            'webp'         => $this->webp,
            'urlSrc'       => $this->getUrlSrc(),
            'urlWebp'      => $this->getUrlWebp(),
            'pictureLangs' => PictureLangResource::collection($this->whenLoaded('pictureLangs')),
        ];
    }
}
