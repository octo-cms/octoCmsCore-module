<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ProviderResource
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Transformers
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class ProviderImportDataResource extends JsonResource
{
    /**
     * Name toArray
     *
     * @param Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'       => $this->id,
            'date'     => optional($this->date)->format('Y-m-d'),
            'name'     => $this->name,
            'surname'  => $this->surname,
            'email'    => $this->email,
            'price'    => $this->price,
            'data'     => $this->data,
            'registry' => new RegistryResource($this->whenLoaded('registry')),
            'provider' => new ProviderResource($this->whenLoaded('provider')),
        ];
    }
}
