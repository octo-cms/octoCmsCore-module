<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class VideoLangResource
 *
 * @package OctoCmsModule\Core\Transformers
 */
class VideoLangResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'video_id'    => $this->video_id,
            'lang'        => $this->lang,
            'alt'         => $this->alt,
            'caption'     => $this->caption,
            'title'       => $this->title,
            'description' => $this->description,
        ];
    }
}
