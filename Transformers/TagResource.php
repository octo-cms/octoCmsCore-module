<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TagResource
 *
 * @package OctoCmsModule\Core\Transformers
 */
class TagResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'slug'    => optional($this->tag)->slug,
            'name'    => optional($this->tag)->name,
            'count'   => optional($this->tag)->count,
            'suggest' => optional($this->tag)->suggest,
        ];
    }
}
