<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class ProviderResource
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Transformers
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class ProviderImportResource extends JsonResource
{
    /**
     * Name toArray
     *
     * @param Request $request Request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'      => $this->id,
            'status'  => $this->status,
            'errors'  => $this->errors,
            'created' => optional($this->created_at)->format('Y-m-d H:i:s'),
            'created_date' => optional($this->created_at)->format('Y-m-d'),
            'created_time' => optional($this->created_at)->format('H:i:s')
        ];
    }
}
