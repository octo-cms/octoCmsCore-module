<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CompanyRegistryResource
 *
 * @package OctoCmsModule\Core\Transformers
 */
class CompanyRegistryResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->id,
            'registry_id'  => $this->registry_id,
            'businessname' => $this->businessname,
            'referral'     => $this->referral,
            'piva'         => $this->piva,
            'codfis'       => $this->codfis,
            'web'          => $this->web,
            'company_form' => $this->company_form,
        ];
    }
}
