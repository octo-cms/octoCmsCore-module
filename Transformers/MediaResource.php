<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class MediaResource
 *
 * @package OctoCmsModule\Core\Transformers
 */
class MediaResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'             => $this->id,
            'disk'           => $this->disk,
            'directory'      => $this->directory,
            'filename'       => $this->filename,
            'extension'      => $this->extension,
            'mime_type'      => $this->mime_type,
            'aggregate_type' => $this->aggregate_type,
            'size'           => $this->size,
            'tag'            => optional($this->pivot)->tag,
            'url'            => $this->getUrl(),
            'created_at'     => optional($this->created_at)->format('Y-m-d'),
            'updated_at'     => optional($this->updated_at)->format('Y-m-d'),
            'mediaLangs'     => MediaLangResource::collection($this->whenLoaded('mediaLangs')),
        ];
    }
}
