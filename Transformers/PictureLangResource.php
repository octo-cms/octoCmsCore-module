<?php

namespace OctoCmsModule\Core\Transformers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class PictureLangResource
 *
 * @package OctoCmsModule\Core\Transformers
 */
class PictureLangResource extends JsonResource
{
    /**
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'picture_id'  => $this->picture_id,
            'alt'         => $this->alt,
            'caption'     => $this->caption,
            'title'       => $this->title,
            'description' => $this->description,
            'lang'        => $this->lang,
        ];
    }
}
