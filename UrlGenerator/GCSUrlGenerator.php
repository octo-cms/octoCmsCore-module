<?php

namespace  OctoCmsModule\Core\UrlGenerator;

use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Filesystem\FilesystemManager;
use OctoCmsModule\Core\Services\PictureService;
use Plank\Mediable\UrlGenerators\BaseUrlGenerator;

/**
 * Class GCSUrlGenerator
 *
 * @package  OctoCmsModule\Core\UrlGenerator
 * @author  danielepasi
 */
class GCSUrlGenerator extends BaseUrlGenerator
{
    /**
     * Filesystem Manager.
     * @var FilesystemManager
     */
    protected $filesystem;

    /**
     * Constructor.
     * @param Config            $config
     * @param FilesystemManager $filesystem
     */
    public function __construct(Config $config, FilesystemManager $filesystem)
    {
        parent::__construct($config);
        $this->filesystem = $filesystem;
    }

    /**
     * {@inheritdoc}
     */
    public function getAbsolutePath(): string
    {
        return $this->getUrl();
    }

    /**
     * {@inheritdoc}
     */
    public function getUrl(): string
    {
        return PictureService::BUCKET_BASE_URL . '/' .
            env('GOOGLE_CLOUD_STORAGE_BUCKET') . '/' .
            $this->media->getDiskPath();

//        /** @var Cloud $filesystem */
//        $filesystem = $this->filesystem->disk($this->media->disk);
//        return $filesystem->url($this->media->getDiskPath());
    }
}
