<?php

namespace OctoCmsModule\Core\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class CoreDatabaseSeeder
 *
 * @package OctoCmsModule\Core\Database\Seeders
 * @author  danielepasi
 */
class CoreDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
//        $this->call(InstallDatabaseSeeder::class);
    }
}
