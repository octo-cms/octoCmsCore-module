<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomFieldEntityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_field_entity', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('custom_field_id')->unsigned()->nullable();
            $table->bigInteger('entity_id')->unsigned()->nullable();
            $table->string('entity_type')->nullable();
            $table->bigInteger('valuable_id')->unsigned()->nullable();
            $table->string('valuable_type')->nullable();

            $table->foreign('custom_field_id')->references('id')
                ->on('custom_fields')->onDelete('cascade');
        });

        Schema::create('custom_field_entity_string', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('value')->nullable();
        });

        Schema::create('custom_field_entity_text', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('value')->nullable();
        });

        Schema::create('custom_field_entity_decimal', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('value')->nullable();
        });

        Schema::create('custom_field_entity_numeric', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('value')->nullable();
        });

        Schema::create('custom_field_entity_date', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('value')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_field_entity');
        Schema::dropIfExists('custom_field_entitiy_string');
        Schema::dropIfExists('custom_field_entitiy_text');
        Schema::dropIfExists('custom_field_entitiy_decimal');
        Schema::dropIfExists('custom_field_entitiy_numeric');
        Schema::dropIfExists('custom_field_entitiy_date');
    }
}
