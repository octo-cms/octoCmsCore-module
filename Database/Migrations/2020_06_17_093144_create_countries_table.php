<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->string('sigla_alpha_2', 10)->primary();
            $table->string('sigla_numerica', 10);
            $table->string('sigla_alpha_3', 10);
            $table->string('name_it', 100);
            $table->string('name_en', 100);
            $table->string('name_es', 100);
            $table->string('name_de', 100);
            $table->string('name_fr', 100);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
