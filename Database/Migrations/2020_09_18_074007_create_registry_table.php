<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registry', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->string('code', 50)->nullable();
            $table->string('type', 50)->default('private');

            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('set null');

            $table->timestamps();
        });

        Schema::create('private_registry', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('registry_id')->unsigned();
            $table->string('name', 100)->nullable();
            $table->string('surname', 100)->nullable();
            $table->string('gender', 20)->nullable();
            $table->string('codfis', 20)->nullable();
            $table->date('birth_date')->nullable();

            $table->foreign('registry_id')->references('id')
                ->on('registry')->onDelete('cascade');

        });

        Schema::create('company_registry', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('registry_id')->unsigned();
            $table->string('businessname', 100)->nullable();
            $table->string('referral', 100)->nullable();
            $table->string('piva', 20)->nullable();
            $table->string('codfis', 20)->nullable();
            $table->string('web', 100)->nullable();
            $table->string('company_form', 100)->nullable();

            $table->foreign('registry_id')->references('id')
                ->on('registry')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
