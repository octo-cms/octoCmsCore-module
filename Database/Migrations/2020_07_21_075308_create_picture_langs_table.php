<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePictureLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('picture_langs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('picture_id')->unsigned();
            $table->string('lang', 5);
            $table->string('alt')->nullable();
            $table->string('caption')->nullable();
            $table->string('title')->nullable();
            $table->text('description')->nullable();

            $table->foreign('picture_id')->references('id')
                ->on('pictures')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('picture_langs');
    }
}
