<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProvidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('providers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('slug', 50)->unique();
            $table->string('acronym', 10)->nullable();
            $table->boolean('enabled');
            $table->text('xls_mapping')->nullable();
            $table->decimal('record_price', 6,2)->default(0);
        });

        Schema::create('provider_imports', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status', 50)->default('pending');
            $table->text('errors')->nullable();
            $table->timestamps();
        });

        Schema::create('provider_import_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->bigInteger('provider_import_id')->unsigned()->nullable();
            $table->bigInteger('provider_id')->unsigned()->nullable();
            $table->bigInteger('registry_id')->unsigned()->nullable();
            $table->string('registry_type', 10)->nullable();
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('email')->nullable();
            $table->decimal('price', 6,2)->default(0);
            $table->text('data')->nullable();

            $table->foreign('provider_import_id')->references('id')
                ->on('provider_imports')->onDelete('cascade');
            $table->foreign('provider_id')->references('id')
                ->on('providers')->onDelete('set null');
            $table->foreign('registry_id')->references('id')
                ->on('registry')->onDelete('set null');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('providers');
        Schema::dropIfExists('provider_imports');
        Schema::dropIfExists('provider_import_data');
    }
}
