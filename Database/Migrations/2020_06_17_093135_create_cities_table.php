<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->string('cod_comune_alfanumerico', 10)->primary();
            $table->integer('cod_comune_numerico');
            $table->string('cod_regione', 10);
            $table->string('regione');
            $table->string('cod_sovracomunale', 10);
            $table->string('sovracomunale');
            $table->string('cod_provincia', 10)->index();
            $table->string('progressivo', 10);
            $table->string('cod_catastale', 10);
            $table->string('denominazione');
            $table->string('cod_ripartizione_geografica', 10);
            $table->string('ripartizione_geografica');
            $table->boolean('capoluogo');
            $table->string('sigla_automobilistica', 10);
            $table->string('nuts1', 10);
            $table->string('nuts2', 10);
            $table->string('nuts3', 10);
            $table->string('postal_code', 10)->nullable();
            $table->decimal('latitude', 10,6)->nullable();
            $table->decimal('longitude', 10, 6)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cities');
    }
}
