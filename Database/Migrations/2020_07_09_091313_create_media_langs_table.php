<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 *
 * Class CreateMediaLangsTable
 */
class CreateMediaLangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_langs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('media_id')->unsigned();
            $table->string('lang', 5);
            $table->string('alt')->nullable();
            $table->string('caption')->nullable();
            $table->string('title')->nullable();
            $table->text('description')->nullable();

            $table->foreign('media_id')->references('id')
                ->on('media')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('media_langs');
    }
}
