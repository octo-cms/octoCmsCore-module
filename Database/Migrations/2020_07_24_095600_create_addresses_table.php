<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use OctoCmsModule\Core\Entities\Address;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('addressable_id')->unsigned()->nullable();
            $table->string('addressable_type')->nullable();
            $table->boolean('default')->default(false);
            $table->string('alias', 50)->nullable();
            $table->string('place_id')->nullable();

            $table->string('street_address')->nullable()
                ->comment('indicates a precise street address.');
            $table->string('route')->nullable()
                ->comment('indicates a named route (such as "US 101").;
intersection indicates a major intersection, usually of two major roads.');
            $table->string('political')->nullable()
                ->comment('indicates a political entity. Usually, this type indicates a polygon of some
                civil administration.');
            $table->string('country')->nullable()
                ->comment('indicates the national political entity, and is typically the highest order
                type returned by the Geocoder.');
            $table->string('administrative_area_level_1')->nullable()
                ->comment('indicates a first-order civil entity below the country level. Within the United
                States, these administrative levels are states. Not all nations exhibit these administrative levels.
                In most cases, administrative_area_level_1 short names will closely match ISO 3166-2 subdivisions and
                other widely circulated lists; however this is not guaranteed as our geocoding results are based on a
                variety of signals and location data.');
            $table->string('administrative_area_level_2')->nullable()
                ->comment('indicates a second-order civil entity below the country level. Within the United
                States, these administrative levels are counties. Not all nations exhibit these administrative
                levels.');
            $table->string('administrative_area_level_3')->nullable()
                ->comment('indicates a third-order civil entity below the country level. This type indicates
                a minor civil division. Not all nations exhibit these administrative levels.');
            $table->string('administrative_area_level_4')->nullable()
                ->comment('indicates a fourth-order civil entity below the country level. This type indicates
                a minor civil division. Not all nations exhibit these administrative levels.');
            $table->string('administrative_area_level_5')->nullable()
                ->comment('indicates a fifth-order civil entity below the country level. This type indicates
                a minor civil division. Not all nations exhibit these administrative levels.');
            $table->string('colloquial_area')->nullable()
                ->comment('indicates a commonly-used alternative name for the entity.');
            $table->string('locality')->nullable()
                ->comment('indicates an incorporated city or town political entity.');
            $table->string('sublocality')->nullable()
                ->comment('indicates a first-order civil entity below a locality. For some locations may
                receive one of the additional types: sublocality_level_1 to sublocality_level_5. Each sublocality
                level is a civil entity. Larger numbers indicate a smaller geographic area.');
            $table->string('neighborhood')->nullable()
                ->comment('indicates a named neighborhood');
            $table->string('premise')->nullable()
                ->comment('indicates a named location, usually a building or collection of buildings with a
                common name');
            $table->string('subpremise')->nullable()
                ->comment('indicates a first-order entity below a named location, usually a singular building
                within a collection of buildings with a common name');
            $table->string('plus_code')->nullable()
                ->comment('indicates an encoded location reference, derived from latitude and longitude.
                Plus codes can be used as a replacement for street addresses in places where they do not exist (where
                buildings are not numbered or streets are not named). See https://plus.codes for details.');
            $table->string('postal_code')->nullable()
                ->comment('indicates a postal code as used to address postal mail within the country.');
            $table->string('natural_feature')->nullable()
                ->comment('indicates a prominent natural feature.');
            $table->string('airport')->nullable()
                ->comment('indicates an airport.');
            $table->string('park')->nullable()
                ->comment('indicates a named park.');
            $table->string('point_of_interest')->nullable()
                ->comment('indicates a named point of interest. Typically, these "POI"s are prominent
                local entities that don\'t easily fit in another category, such as "Empire State Building"
                or "Eiffel Tower".');

            $table->text('other')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
