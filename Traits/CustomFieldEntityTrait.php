<?php

declare(strict_types=1);

namespace OctoCmsModule\Core\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use OctoCmsModule\Core\Entities\CustomField;
use OctoCmsModule\Core\Entities\CustomFieldEntity;
use OctoCmsModule\Core\Entities\CustomFieldEntityDate;
use OctoCmsModule\Core\Entities\CustomFieldEntityDecimal;
use OctoCmsModule\Core\Entities\CustomFieldEntityNumeric;
use OctoCmsModule\Core\Entities\CustomFieldEntityString;
use OctoCmsModule\Core\Entities\CustomFieldEntityText;

use function is_array;

/**
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Traits
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2021
 */
trait CustomFieldEntityTrait
{
    /**
     * Name customFieldEntities
     */
    public function customFieldEntities(): MorphMany
    {
        return $this->morphMany(CustomFieldEntity::class, 'entity', 'entity_type', 'entity_id');
    }

    /**
     * Name saveCustomFieldValue
     *
     * @param string $customFieldName Custom Field Name
     * @param mixed  $value           Custom Field Value
     */
    public function saveCustomFieldValue(string $customFieldName, $value):? CustomFieldEntity
    {
        /**
         * CustomField
         *
         * @var CustomField $customField
         */
        $customField = CustomField::where(['name' => $customFieldName])
            ->firstOrFail();

        /**
         * CustomFieldEntity
         *
         * @var CustomFieldEntity $customFieldEntity
         */
        $customFieldEntity = $this->customFieldEntities()
            ->where(['custom_field_id' => $customField->id])
            ->first();

        if (empty($customFieldEntity)) {
            $customFieldEntity = new CustomFieldEntity();
            $customFieldEntity->customField()->associate($customField);
            $customFieldEntity->entity()->associate($this);
            $customFieldEntity->save();

            switch ($customField->type) {
                case CustomField::TYPE_STRING:
                    $customFieldValue = new CustomFieldEntityString();
                    break;
                case CustomField::TYPE_DATE:
                    $customFieldValue = new CustomFieldEntityDate();
                    break;
                case CustomField::TYPE_DECIMAL:
                    $customFieldValue = new CustomFieldEntityDecimal();
                    break;
                case CustomField::TYPE_NUMERIC:
                    $customFieldValue = new CustomFieldEntityNumeric();
                    break;
                case CustomField::TYPE_TEXT:
                    $customFieldValue = new CustomFieldEntityText();
                    break;
                default:
                    return null;
            }

            $customFieldValue->value = $value;
            $customFieldValue->save();

            $customFieldEntity->valuable()->associate($customFieldValue);
        } else {
            $customFieldEntity->valuable()->update(
                [
                    'value' => $value
                ]
            );
        }

        $customFieldEntity->save();

        return $customFieldEntity;
    }

    /**
     * Name scopeWhereCustomFields
     *
     * @param Builder $builder      Builder
     * @param mixed   $customFields Custom Fields
     * @param string  $operator     Operator
     * @param mixed   $query        Query
     */
    public function scopeWhereCustomFields(Builder $builder, $customFields, string $operator, $query): Builder
    {
        if (!is_array($customFields)) {
            $customFields = [$customFields];
        }

        return $builder->whereHas(
            'customFieldEntities',
            static function (Builder $customFieldEntityBuilder) use ($customFields, $operator, $query): void {
                $customFieldEntityBuilder->whereHas(
                    'customField',
                    static function (Builder $customFieldBuilder) use ($customFields): void {
                        foreach ($customFields as $customField) {
                            $customFieldBuilder->orWhere('name', '=', $customField);
                        }
                    }
                )->whereHas(
                    'valuable',
                    static function (Builder $customFieldEntityValue) use ($operator, $query): void {
                        $customFieldEntityValue->where('value', $operator, $query);
                    }
                );
            }
        );
    }

    /**
     * Name parseCustomFields
     *
     * @return array|mixed[]
     */
    public function parseCustomFields(): array
    {
        $data = [];

        foreach ($this->customFieldEntities as $customFieldEntity) {
            $data[$customFieldEntity->customField->name] = $customFieldEntity->valuable->value;
        }

        return $data;
    }
}
