<?php

namespace OctoCmsModule\Core\Traits;

/**
 * Trait SaveEmailRequestTrait
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Traits
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
trait SavePhoneRequestTrait
{

    /**
     * Name getPhoneRules
     *
     * @return string[]
     */
    public function getPhoneRules()
    {
        return [
            'phones.*.id'      => 'sometimes|nullable',
            'phones.*.label'   => 'sometimes|nullable',
            'phones.*.phone'   => 'sometimes|nullable',
            'phones.*.default' => 'sometimes|nullable',
        ];
    }
}
