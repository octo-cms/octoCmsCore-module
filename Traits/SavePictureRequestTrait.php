<?php

namespace OctoCmsModule\Core\Traits;

/**
 * Trait SavePictureRequestTrait
 *
 * @package OctoCmsModule\Core\Traits
 */
trait SavePictureRequestTrait
{

    /**
     * @param  string $prefix
     * @return string[]
     */
    public function getPictureRules($prefix = '')
    {
        return [
            $prefix . 'pictures'                              => 'sometimes|array',
            $prefix . 'pictures.*.id'                         => 'sometimes|nullable',
            $prefix . 'pictures.*.src'                        => 'sometimes',
            $prefix . 'pictures.*.tag'                        => 'sometimes',
            $prefix . 'pictures.*.webp'                       => 'sometimes',
            $prefix . 'pictures.*.newImage'                   => 'sometimes|array',
            $prefix . 'pictures.*.newImage.src'               => 'sometimes',
            $prefix . 'pictures.*.newImage.height'            => 'sometimes',
            $prefix . 'pictures.*.newImage.width'             => 'sometimes',
            $prefix . 'pictures.*.pictureLangs'               => 'sometimes|array',
            $prefix . 'pictures.*.pictureLangs.*.lang'        => 'sometimes',
            $prefix . 'pictures.*.pictureLangs.*.alt'         => 'sometimes',
            $prefix . 'pictures.*.pictureLangs.*.caption'     => 'sometimes',
            $prefix . 'pictures.*.pictureLangs.*.description' => 'sometimes',
            $prefix . 'pictures.*.pictureLangs.*.title'       => 'sometimes',
            $prefix . 'pictures.*.pictureLangs.*.picture_id'  => 'sometimes',
        ];
    }
}
