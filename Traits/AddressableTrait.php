<?php

namespace OctoCmsModule\Core\Traits;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Entities\Address;

/**
 * Trait AddressableTrait
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Traits
 * @author   Adriano Camacaro <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
trait AddressableTrait
{
    /**
     * Addresses relationship
     *
     * @return MorphMany
     */
    public function addresses()
    {
        return $this->morphMany(
            Address::class,
            'addressable',
            'addressable_type',
            'addressable_id'
        );
    }

    /**
     * Name defaultEmail
     *
     * @return MorphOne
     */
    public function defaultAddress()
    {
        return $this->morphOne(
            Address::class,
            'addressable',
            'addressable_type',
            'addressable_id'
        )
            ->where('default', '=', true);
    }

    /**
     * Name syncAddresses
     *
     * @param array $addresses Addresses Array
     *
     * @return void
     */
    public function syncAddresses(array $addresses)
    {

        $this->addresses()
            ->whereIn(
                'id',
                array_diff(
                    $this->addresses->pluck('id')->toArray(),
                    Arr::pluck($addresses, 'id')
                )
            )->delete();

        $checkDefault = false;

        foreach ($addresses as $address) {
            $item = $this->addresses()->find(Arr::get($address, 'id', null));

            if (empty($item)) {
                $item = new Address();
                $item->addressable()->associate($this);
            }

            $item->alias = Arr::get($address, 'alias', null);
            $item->street_address = Arr::get($address, 'street_address', null);
            $item->country = Arr::get($address, 'country', null);
            $item->administrative_area_level_1 = Arr::get($address, 'administrative_area_level_1', null);
            $item->administrative_area_level_2 = Arr::get($address, 'administrative_area_level_2', null);
            $item->locality = Arr::get($address, 'locality', null);
            $item->postal_code = Arr::get($address, 'postal_code', null);
            $item->default = Arr::get($address, 'default', false);

            if ($item->default) {
                $checkDefault = true;
            }

            $item->save();
        }

        if (!$checkDefault && !empty($addresses)) {
            $this->addresses()->first()->update(
                [
                'default' => true
                ]
            );
        }
    }
}
