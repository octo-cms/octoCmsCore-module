<?php

namespace  OctoCmsModule\Core\Traits;

use OctoCmsModule\Sitebuilder\Utils\ImageUtils;

/**
 * Trait ImageSrcTrait
 *
 * @package OctoCmsModule\Core\Traits
 */
trait ImageSrcTrait
{
    /**
     * @param  null $data
     * @return string
     */
    public function getImageSrc($data = null)
    {
        if (empty($data)) {
            return '';
        }

        return ImageUtils::getSrc($data);
    }
}
