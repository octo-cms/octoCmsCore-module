<?php

namespace OctoCmsModule\Core\Traits;

/**
 * Trait SaveEmailRequestTrait
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Traits
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
trait SaveEmailRequestTrait
{

    /**
     * Name getEmailRules
     *
     * @return string[]
     */
    public function getEmailRules()
    {
        return [
            'emails.*.id'      => 'sometimes|nullable',
            'emails.*.label'   => 'sometimes|nullable',
            'emails.*.email'   => 'sometimes|nullable',
            'emails.*.default' => 'sometimes|nullable',
        ];
    }
}
