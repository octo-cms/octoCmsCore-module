<?php

namespace OctoCmsModule\Core\Traits;

/**
 * Trait SaveAddressRequestTrait
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Traits
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
trait SaveAddressRequestTrait
{

    /**
     * Name getAddressRules
     *
     * @return string[]
     */
    public function getAddressRules()
    {
        return [
            'addresses.*.id'                          => 'sometimes|nullable',
            'addresses.*.alias'                       => 'sometimes|nullable',
            'addresses.*.street_address'              => 'sometimes|nullable',
            'addresses.*.administrative_area_level_1' => 'sometimes|nullable',
            'addresses.*.administrative_area_level_2' => 'sometimes|nullable',
            'addresses.*.country'                     => 'sometimes|nullable',
            'addresses.*.locality'                    => 'sometimes|nullable',
            'addresses.*.postal_code'                 => 'sometimes|nullable',
            'addresses.*.default'                     => 'sometimes|nullable'
        ];
    }
}
