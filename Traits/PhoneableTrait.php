<?php

namespace OctoCmsModule\Core\Traits;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Entities\Phone;

/**
 * Trait PhoneableTrait
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Traits
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
trait PhoneableTrait
{
    /**
     * Name phones
     *
     * @return MorphMany
     */
    public function phones(): MorphMany
    {
        return $this->morphMany(Phone::class, 'phoneable', 'phoneable_type', 'phoneable_id');
    }

    /**
     * Name defaultPhone
     *
     * @return MorphOne
     */
    public function defaultPhone(): MorphOne
    {
        return $this->morphOne(Phone::class, 'phoneable', 'phoneable_type', 'phoneable_id')
            ->where('default', '=', true);
    }

    /**
     * Name syncPhones
     *
     * @param array $phones Phones Array
     *
     * @return void
     */
    public function syncPhones(array $phones)
    {

        $this->phones()
            ->whereIn(
                'id',
                array_diff(
                    $this->phones->pluck('id')->toArray(),
                    Arr::pluck($phones, 'id')
                )
            )->delete();

        $checkDefault = false;

        foreach ($phones as $phone) {
            $item = $this->phones->find(Arr::get($phone, 'id', null));

            if (empty($item)) {
                $item = new Phone();
                $item->phoneable()->associate($this);
            }

            $item->label = Arr::get($phone, 'label', null);
            $item->phone = Arr::get($phone, 'phone', 'N.D.');
            $item->default = Arr::get($phone, 'default', false);

            if ($item->default) {
                $checkDefault = true;
            }

            $item->save();
        }

        if (!$checkDefault && !empty($phones)) {
            $this->phones()->first()->update(
                [
                'default' => true
                ]
            );
        }
    }
}
