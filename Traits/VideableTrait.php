<?php

namespace OctoCmsModule\Core\Traits;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use OctoCmsModule\Core\Entities\Video;

/**
 * Trait VideableTrait
 *
 * @package OctoCmsModule\Core\Traits
 */
trait VideableTrait
{
    /**
     * @return MorphMany
     */
    public function videos()
    {
        return $this->morphMany(Video::class, 'videable', 'videable_type', 'videable_id');
    }
}
