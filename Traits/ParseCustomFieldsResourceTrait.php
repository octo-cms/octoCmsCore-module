<?php

namespace OctoCmsModule\Core\Traits;

/**
 * Trait ParseCustomFieldsResourceTrait
 *
 * @category Octo
 * @package OctoCmsModule\Core\Traits
 * @author Camacaro Adriano <acamacaro@octopuslab.it>
 * @license copyright Octopus Srl 2020
 * @link https://octopus.srl
 */
trait ParseCustomFieldsResourceTrait
{
    /**
     * Name parseCustomFields
     *
     * @return array
     */
    protected function parseCustomFields()
    {
        $data = [];

        foreach ($this->customFieldEntities as $customFieldEntity) {
            $data[$customFieldEntity->customField->name] = $customFieldEntity->valuable->value;
        }

        return $data;
    }
}
