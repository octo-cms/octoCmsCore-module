<?php

namespace OctoCmsModule\Core\Traits;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Entities\Email;

/**
 * Trait EmailableTrait
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Traits
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
trait EmailableTrait
{
    /**
     * Name emails
     *
     * @return MorphMany
     */
    public function emails()
    {
        return $this->morphMany(Email::class, 'emailable', 'emailable_type', 'emailable_id');
    }

    /**
     * Name defaultEmail
     *
     * @return MorphOne
     */
    public function defaultEmail()
    {
            return $this->morphOne(Email::class, 'emailable', 'emailable_type', 'emailable_id')
                ->where('default', '=', true);
    }

    /**
     * Name syncEmails
     *
     * @param array $emails Array Emails
     *
     * @return void
     */
    public function syncEmails(array $emails)
    {

        $this->emails()
            ->whereIn(
                'id',
                array_diff(
                    $this->emails->pluck('id')->toArray(),
                    Arr::pluck($emails, 'id')
                )
            )->delete();

        $checkDefault = false;

        foreach ($emails as $email) {
            $item = $this->emails->find(Arr::get($email, 'id', null));

            if (empty($item)) {
                $item = new Email();
                $item->emailable()->associate($this);
            }

            $item->label = Arr::get($email, 'label', null);
            $item->email = Arr::get($email, 'email', 'N.D.');
            $item->default = Arr::get($email, 'default', false);

            if ($item->default) {
                $checkDefault = true;
            }

            $item->save();
        }

        if (!$checkDefault && !empty($emails)) {
            $this->emails()->first()->update(
                [
                'default' => true
                ]
            );
        }
    }
}
