<?php

namespace OctoCmsModule\Core\Traits;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use OctoCmsModule\Core\Entities\Picture;

/**
 * Trait PicturableTrait
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Traits
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
trait PicturableTrait
{
    /**
     * Name pictures
     *
     * @return MorphMany
     */
    public function pictures()
    {
        return $this->morphMany(
            Picture::class,
            'picturable',
            'picturable_type',
            'picturable_id'
        );
    }

    /**
     * Name getPictures
     *
     * @param string $tag Tag
     *
     * @return mixed
     */
    public function getPictures(string $tag = '')
    {
        $pictureBuilder = $this->pictures();

        if (!empty($tag)) {
            $pictureBuilder->where('tag', '=', $tag);
        }

        return $pictureBuilder;
    }

    /**
     * Name loadPictures
     *
     * @param string $tag tag
     *
     * @return $this
     */
    public function loadPictures(string $tag = '')
    {
        if (!empty($tag)) {
            $this->load(
                [
                'pictures' => function ($query) use ($tag) {
                    $query->where('tag', '=', $tag);
                }
                ],
                'pictures.pictureLangs'
            );
        } else {
            $this->load('pictures', 'pictures.pictureLangs');
        }

        return $this;
    }
}
