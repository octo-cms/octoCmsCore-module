<?php

namespace OctoCmsModule\Core\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * Class PIva
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Rules
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class PIva implements Rule
{

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute string
     * @param mixed  $value     mixed
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value=trim($value);

        if ($value == '') {
            return true;
        }
        if (strlen($value) != 11) {
            return false;
        }
        if (!preg_match("/^[0-9]+$/", $value)) {
            return false;
        }

        $s = 0;
        for ($i = 0; $i <= 9; $i += 2) {
            $s += ord($value[$i]) - ord('0');
        }
        for ($i = 1; $i <= 9; $i += 2) {
            $c = 2 * (ord($value[$i]) - ord('0'));
            if ($c > 9) {
                $c = $c - 9;
            }
            $s += $c;
        }
        if ((10 - $s % 10) % 10 != ord($value[10]) - ord('0')) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Partita iva non corretta';
    }
}
