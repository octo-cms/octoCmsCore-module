<?php

namespace OctoCmsModule\Core\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * Class CodFis
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Rules
 * @author   Roberto La Rocca <accoral@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class CodFis implements Rule
{

    const REG_CHECK_STRING = "/^[A-Z]{6}[0-9]{2}[A-Z][0-9]{2}[A-Z][0-9]{2}[A-Z|0-9][A-Z]$/i";

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute string
     * @param mixed  $value     mixed
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {

        /* potrebbe essere un codice fiscale con la verifica della partita iva */
        if (strlen($value)==11) {
            $piva=new PIva();
            return $piva->passes($attribute, $value);
        }

        if (strlen($value) != 16) {
            return false;
        }

        $value = strtoupper($value);

        if (!preg_match(self::REG_CHECK_STRING, $value)) {
            return false;
        }

        $s = 0;
        for ($a = 1; $a <= 13; $a += 2) {
            $c = $value[$a];
            $v = ord($c);

            if ('0' <= $c && $c <= '9') {
                $s += $v - 48;
            } else {
                $s += $v - 65;
            }
        }

        for ($a = 0; $a <= 14; $a += 2) {
            $c = $value[$a];

            switch ($c) {
                case '0':
                    $s += 1;
                    break;
                case '1':
                    $s += 0;
                    break;
                case '2':
                    $s += 5;
                    break;
                case '3':
                    $s += 7;
                    break;
                case '4':
                    $s += 9;
                    break;
                case '5':
                    $s += 13;
                    break;
                case '6':
                    $s += 15;
                    break;
                case '7':
                    $s += 17;
                    break;
                case '8':
                    $s += 19;
                    break;
                case '9':
                    $s += 21;
                    break;
                case 'A':
                    $s += 1;
                    break;
                case 'B':
                    $s += 0;
                    break;
                case 'C':
                    $s += 5;
                    break;
                case 'D':
                    $s += 7;
                    break;
                case 'E':
                    $s += 9;
                    break;
                case 'F':
                    $s += 13;
                    break;
                case 'G':
                    $s += 15;
                    break;
                case 'H':
                    $s += 17;
                    break;
                case 'I':
                    $s += 19;
                    break;
                case 'J':
                    $s += 21;
                    break;
                case 'K':
                    $s += 2;
                    break;
                case 'L':
                    $s += 4;
                    break;
                case 'M':
                    $s += 18;
                    break;
                case 'N':
                    $s += 20;
                    break;
                case 'O':
                    $s += 11;
                    break;
                case 'P':
                    $s += 3;
                    break;
                case 'Q':
                    $s += 6;
                    break;
                case 'R':
                    $s += 8;
                    break;
                case 'S':
                    $s += 12;
                    break;
                case 'T':
                    $s += 14;
                    break;
                case 'U':
                    $s += 16;
                    break;
                case 'V':
                    $s += 10;
                    break;
                case 'W':
                    $s += 22;
                    break;
                case 'X':
                    $s += 25;
                    break;
                case 'Y':
                    $s += 24;
                    break;
                case 'Z':
                    $s += 23;
                    break;
            }
        }

        if (($s % 26 + 65) != ord($value[15])) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Codice fiscale errato';
    }
}
