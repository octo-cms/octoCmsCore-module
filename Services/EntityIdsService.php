<?php

namespace OctoCmsModule\Core\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection as CollectionEloquent;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use OctoCmsModule\Core\DTO\EntityIdsDTO;
use OctoCmsModule\Core\Interfaces\EntityIdsServiceInterface;
use OctoCmsModule\Core\Utils\LanguageUtils;

/**
 * Class EntityIdsService
 *
 * @package OctoCmsModule\Core\Services
 */
class EntityIdsService implements EntityIdsServiceInterface
{
    /**
     * @param  array   $fields
     * @param  Builder $items
     * @return mixed|EntityIdsService
     */
    public function getEntityIdsDTO(Builder $items, array $fields = [])
    {
        $rowsInPage = 10;
        $orderBy = 'updated_at';
        $currentPage = 1;

        extract($fields, EXTR_IF_EXISTS);

        $entityIdsDTO = new EntityIdsDTO();

        $entityIdsDTO->total = $items->count();

        $entityIdsDTO->builder = $items
            ->orderBy($orderBy)
            ->take($rowsInPage)
            ->offset($rowsInPage * ($currentPage - 1));

        $entityIdsDTO->currentPage = $currentPage;

        $entityIdsDTO->rowsInPage = $rowsInPage;

        return $entityIdsDTO;
    }

    /**
     * @param  CollectionEloquent|Collection $collection
     * @param  string                        $key
     * @param  string                        $name
     * @param  string|null                   $description
     * @return array|mixed
     */
    public function parseCollection($collection, string $key, string $name, string $description = null)
    {
        $array = [];

        foreach ($collection->toArray() as $item) {
            $array[Arr::get($item, $key, null)] = [
                'id'          => Arr::get($item, $key, null),
                'name'        => Arr::get($item, $name, null),
                'description' => $description ? Arr::get($item, $description, null) : null
            ];
        }

        return $array;
    }

    /**
     * @param  $collection
     * @param  string      $langKey
     * @param  string      $key
     * @param  string      $name
     * @param  string|null $description
     * @return array
     */
    public function parseMultiLangCollection(
        $collection,
        string $langKey,
        string $key,
        string $name,
        string $description = null
    ) {
        $array = [];

        foreach ($collection->toArray() as $item) {
            $array[Arr::get($item, $key, null)] = [
                'id'          => Arr::get($item, $key, null),
                'name'        => LanguageUtils::getLangValue(collect($item[$langKey]), $name),
                'description' => $description
                    ? LanguageUtils::getLangValue(collect($item[$langKey]), $description)
                    : null
            ];
        }

        return $array;
    }
}
