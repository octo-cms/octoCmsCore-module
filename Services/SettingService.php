<?php

namespace OctoCmsModule\Core\Services;

use Illuminate\Support\Arr;
use OctoCmsModule\Core\Constants\CacheTagConst;
use OctoCmsModule\Core\Entities\Setting;
use OctoCmsModule\Core\Interfaces\SettingServiceInterface;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Class SettingService
 *
 * @package OctoCmsModule\Core\Services
 * @author  danielepasi
 */
class SettingService implements SettingServiceInterface
{

    /**
     * @param string $name
     * @param bool   $enableCache
     *
     * @return mixed|string|null
     * @throws InvalidArgumentException
     */
    public function getSettingByName(string $name, bool $enableCache = true)
    {
        $value = CacheService::get(CacheTagConst::SETTING, $name);

        if (!empty($value) && $enableCache) {
            return $value;
        }

        /** @var Setting $setting */
        $setting = Setting::whereName($name)->first();

        if (!empty($setting)) {
            CacheService::set(CacheTagConst::SETTING, $name, $setting->value);
            return $setting->value;
        }

        return null;
    }

    /**
     * @param array $fields
     */
    public function saveSettings(array $fields)
    {
        foreach ($fields as $settingData) {
            Setting::updateOrCreate(
                ['name' => Arr::get($settingData, 'name', null)],
                ['value' => Arr::get($settingData, 'value', null)]
            );
        }

        CacheService::flushCacheByTag(CacheTagConst::SETTING);
    }
}
