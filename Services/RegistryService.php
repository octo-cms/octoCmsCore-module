<?php

namespace OctoCmsModule\Core\Services;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use OctoCmsModule\Core\Constants\SettingNameConst;
use OctoCmsModule\Core\DTO\ContactFormDataDTO;
use OctoCmsModule\Core\Entities\Registry;
use OctoCmsModule\Core\Exceptions\OctoCmsException;
use OctoCmsModule\Core\Interfaces\RegistryServiceInterface;
use OctoCmsModule\Core\Interfaces\SettingServiceInterface;
use OctoCmsModule\Core\Notifications\AdminNewRegistryNotification;

/**
 * Class RegistryService
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Services
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class RegistryService implements RegistryServiceInterface
{
    protected $settingService;

    /**
     * RegistryService constructor.
     *
     * @param SettingServiceInterface $settingService SettingServiceInterface
     */
    public function __construct(SettingServiceInterface $settingService)
    {
        $this->settingService = $settingService;
    }

    /**
     * Name sendEmailToAdmin
     *
     * @param ContactFormDataDTO $contactFormDataDTO ContactFormDataDTO
     *
     * @return bool
     */
    public function sendEmailToAdmin(ContactFormDataDTO $contactFormDataDTO)
    {

        $adminMail = $this->settingService->getSettingByName(SettingNameConst::ADMIN_MAIL);

        if (empty($adminMail)) {
            return false;
        }

        Notification::route('mail', $adminMail)
            ->notify(new AdminNewRegistryNotification($contactFormDataDTO));

        return true;
    }

    /**
     * Name saveRegistry
     *
     * @param Registry $registry Registry
     * @param array    $fields   array
     *
     * @return Registry
     * @throws OctoCmsException
     */
    public function saveRegistry(Registry $registry, array $fields): Registry
    {
        $this->updatePersonalData($registry, $fields);

        $registry->syncEmails(Arr::get($fields, 'emails', []));
        $registry->syncAddresses(Arr::get($fields, 'addresses', []));
        $registry->syncPhones(Arr::get($fields, 'phones', []));

        foreach (Arr::get($fields, 'customFields', []) as $name => $value) {
            $registry->saveCustomFieldValue($name, $value);
        }

        return $registry;
    }

    /** @inheritdoc
     * @throws OctoCmsException
     */
    public function updatePersonalData(Registry $registry, array $fields): Registry
    {
        DB::beginTransaction();
        $registry->fill($fields);
        $registry->save();

        switch (Arr::get($fields, 'type')) {
            case Registry::TYPE_PRIVATE:
                $registry->privateRegistry()->updateOrCreate(
                    ['registry_id' => $registry->id],
                    $fields['privateRegistry']
                );
                break;

            case Registry::TYPE_COMPANY:
                $registry->companyRegistry()->updateOrCreate(
                    ['registry_id' => $registry->id],
                    $fields['companyRegistry']
                );
                break;

            default:
                DB::rollBack();
                throw new OctoCmsException("Invalid registry Type");
        }

        DB::commit();

        return $registry;
    }
}
