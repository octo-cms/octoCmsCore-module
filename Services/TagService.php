<?php

namespace OctoCmsModule\Core\Services;

use OctoCmsModule\Core\Entities\Tag;
use OctoCmsModule\Core\Entities\TagGroup;
use Conner\Tagging\TaggingUtility;
use OctoCmsModule\Core\Interfaces\TagServiceInterface;

/**
 * Class TagService
 *
 * @package OctoCmsModule\Core\Services
 */
class TagService implements TagServiceInterface
{
    /**
     * @param Tag    $tag
     * @param string $group
     *
     * @return Tag
     */
    public function saveTag(Tag $tag, string $group): Tag
    {
        if (!empty($group)) {
            $tagGroupSlug = TaggingUtility::normalize($group);

            if (empty(TagGroup::query()
                ->where('slug', $tagGroupSlug)
                ->first())) {
                $tagGroup = new TagGroup();
                $tagGroup->name = $tagGroupSlug;
                $tagGroup->save();
            }

            $tag->setGroup($tagGroupSlug);
        }

        $tag->save();

        return $tag;
    }
}
