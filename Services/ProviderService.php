<?php

namespace OctoCmsModule\Core\Services;

use Google\Cloud\Logging\Logger;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\Entities\Provider;
use OctoCmsModule\Core\Entities\ProviderImport;
use OctoCmsModule\Core\Interfaces\ProviderServiceInterface;
use OctoCmsModule\Core\Jobs\LoggingJob;
use OctoCmsModule\Core\Jobs\ParseProviderImportJob;
use OctoCmsModule\Core\Jobs\SyncProviderImportJob;
use OctoCmsModule\Core\Utils\PriceParser;
use Octopus\Logger\DTOs\LogMessageDTO;
use Throwable;

/**
 * Class ProviderService
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Services
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class ProviderService implements ProviderServiceInterface
{
    /**
     * Name saveProvider
     *
     * @param Provider $provider Provider
     * @param array    $fields   Fields
     *
     * @return Provider
     */
    public function saveProvider(Provider $provider, array $fields): Provider
    {
        $fields['record_price'] = PriceParser::parse(Arr::get($fields, 'record_price', 0));
        $provider->fill($fields);
        $provider->save();

        return $provider;
    }

    /**
     * Name uploadImport
     *
     * @param UploadedFile $file    UploadedFile
     * @param array        $options Options Array
     *
     * @return ProviderImport
     */
    public function uploadImport(UploadedFile $file, array $options = []): ProviderImport
    {

        extract($options, EXTR_IF_EXISTS);

        $providerImport = new ProviderImport(
            [
                'status' => ProviderImport::STATUS_PENDING
            ]
        );

        $providerImport->save();
        $providerImport->refresh();

        $filePath = $file->storeAs(
            'imports',
            'import-' . $providerImport->id . '.' . $file->extension(),
            'temp'
        );

        ParseProviderImportJob::withChain(
            [new SyncProviderImportJob($providerImport)]
        )->catch(function (Throwable $e) use ($providerImport) {

            if (method_exists($e, 'failures')) {
                $errors = [];
                $failures = $e->failures();
                foreach ($failures as $failure) {
                    $errors[] = [
                        'row'       => $failure->row(),
                        'attribute' => $failure->attribute(),
                        'errors'    => $failure->errors(),
                        'values'    => $failure->values()
                    ];
                }
            } else {
                $errors = ['details' => $e->getMessage()];
            }

            $providerImport->update([
                'status' => ProviderImport::STATUS_ERROR,
                'errors' => $errors
            ]);

            LoggingJob::dispatch(
                new LogMessageDTO(
                    [
                        'status'           => ProviderImport::STATUS_ERROR,
                        'providerImportId' => $providerImport->id,
                        'errors'           => $errors
                    ],
                    __CLASS__,
                    'uploadImport'
                ),
                ['severity' => Logger::ERROR]
            );
        })
            ->dispatch($providerImport, storage_path('temp/' . $filePath));

        return $providerImport;
    }

    /**
     * Name parseProviderImport
     *
     * @param ProviderImport $providerImport ProviderImport
     * @param string         $filePath       FilePath
     *
     * @return mixed|void
     */
    public function parseProviderImport(ProviderImport $providerImport, string $filePath)
    {
        // TODO: Implement parseProviderImport() method.
    }

    /**
     * Name syncProviderImport
     *
     * @param ProviderImport $providerImport ProviderImport
     *
     * @return mixed|void
     */
    public function syncProviderImport(ProviderImport $providerImport)
    {
        // TODO: Implement syncProviderImport() method.
    }

    /**
     * Name syncProviderImportWithRegistry
     *
     * @param ProviderImport $providerImport ProviderImport
     *
     * @return mixed|void
     */
    public function syncProviderImportWithRegistry(ProviderImport $providerImport)
    {
        // TODO: Implement syncProviderImport() method.
    }
}
