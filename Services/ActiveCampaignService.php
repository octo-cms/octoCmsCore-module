<?php

namespace OctoCmsModule\Core\Services;

use Illuminate\Http\Client\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use OctoCmsModule\Core\Constants\SettingNameConst;
use OctoCmsModule\Core\DTO\ContactFormDataDTO;
use OctoCmsModule\Core\Interfaces\ActiveCampaignServiceInterface;
use OctoCmsModule\Core\Interfaces\SettingServiceInterface;
use OctoCmsModule\Core\Jobs\LoggingJob;
use Octopus\Logger\DTOs\LogMessageDTO;

/**
 * Class ActiveCampaignService
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Services
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class ActiveCampaignService implements ActiveCampaignServiceInterface
{
    public const STATUS_SUBSCRIBE_CONTACT   = 1;
    public const STATUS_UNSUBSCRIBE_CONTACT = 2;

    /**
     * SettingServiceInterface
     *
     * @var SettingServiceInterface
     */
    protected $settingService;

    /**
     * ApiKey
     *
     * @var string
     */
    protected $apiKey;

    /**
     * Base Url
     *
     * @var string
     */
    protected $url;

    /**
     * Check if service is enabled
     *
     * @var bool
     */
    protected $enabled;

    /**
     * List Id
     *
     * @var array|\ArrayAccess|mixed
     */
    protected $listId;


    /**
     * ActiveCampaignService constructor.
     *
     * @param SettingServiceInterface $settingService SettingServiceInterface
     */
    public function __construct(
        SettingServiceInterface $settingService
    ) {
        $this->settingService = $settingService;

        $settings = $this->settingService->getSettingByName(SettingNameConst::ACTIVE_CAMPAIGN);

        $this->enabled = Arr::get($settings, 'enabled', false);
        $this->apiKey = Arr::get($settings, 'api_key', '');
        $this->url = Arr::get($settings, 'url', '');
        $this->listId = Arr::get($settings, 'list_id', '');
    }

    /**
     * Name createContact
     *
     * @param ContactFormDataDTO $contactFormDataDTO ContactFormDataDTO
     *
     * @return mixed|array|bool|null
     */
    public function createContact(ContactFormDataDTO $contactFormDataDTO): ?array
    {
        if (!$this->enabled) {
            return null;
        }

        LoggingJob::dispatch(
            new LogMessageDTO(
                [
                    'action' => 'create contact request',
                    'body'   => $contactFormDataDTO->toArray(),
                ],
                __CLASS__,
                __FUNCTION__
            )
        );

        /**
         * Active Campaign Response
         *
         * @var Response
         */
        $contactResponse = Http::withHeaders(['Api-Token' => $this->apiKey])
            ->post($this->url . '/api/3/contacts', ['contact' => $contactFormDataDTO->toArray()]);

        LoggingJob::dispatch(
            new LogMessageDTO(
                [
                    'status' => $contactResponse->status(),
                    'body'   => $contactResponse->body(),
                ],
                __CLASS__,
                __FUNCTION__
            )
        );

        if ($contactResponse->failed()) {
            return null;
        }

        return $contactResponse->json();
    }

    /**
     * Name subscribeContactToList
     *
     * @param array|null $contact Contact Data
     *
     * @return bool
     */
    public function subscribeContactToList(?array $contact): bool
    {
        if (!Arr::has($contact, 'contact.id')) {
            return false;
        }

        $contactListData = [
            'list'    => $this->listId,
            'contact' => Arr::get($contact, 'contact.id', null),
            'status'  => self::STATUS_SUBSCRIBE_CONTACT,
        ];

        LoggingJob::dispatch(
            new LogMessageDTO(
                [
                    'action' => 'add contact to list request',
                    'body'   => $contactListData,
                ],
                __CLASS__,
                __FUNCTION__
            )
        );

        $listResponse = Http::withHeaders(['Api-Token' => $this->apiKey])
            ->post($this->url . '/api/3/contactLists', ['contactList' => $contactListData]);

        LoggingJob::dispatch(
            new LogMessageDTO(
                [
                    'status' => $listResponse->status(),
                    'body'   => $listResponse->body(),
                ],
                __CLASS__,
                __FUNCTION__
            )
        );

        return $listResponse->successful();
    }
}
