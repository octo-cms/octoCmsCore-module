<?php

namespace OctoCmsModule\Core\Services;

use Illuminate\Support\Facades\Cache;
use Psr\SimpleCache\InvalidArgumentException;

/**
 * Class CacheService
 *
 * @package OctoCmsModule\Core\Services
 */
class CacheService
{
    /**
     * @param string $tag
     * @param string $key
     * @param        $value
     *
     * @return bool|null
     * @throws InvalidArgumentException
     */
    public static function set(string $tag, string $key, $value)
    {
        if (!config('octo-cms.cache_enabled')) {
            return null;
        }

        return Cache::tags([$tag])->set($key, serialize($value));
    }

    /**
     * @param string $tag
     * @param string $key
     *
     * @return mixed|null
     */
    public static function get(string $tag, string $key)
    {
        if (!config('octo-cms.cache_enabled')) {
            return null;
        }

        $value = Cache::tags([$tag])->get($key, null);

        return !empty($value) ? unserialize($value) : null;
    }

    /**
     * @param string $tag
     *
     * @return bool
     */
    public static function flushCacheByTag(string $tag)
    {
        return Cache::tags([$tag])->flush();
    }
}
