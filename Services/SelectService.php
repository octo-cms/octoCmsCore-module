<?php

namespace OctoCmsModule\Core\Services;

/**
 * Class SelectService
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Services
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class SelectService
{
    /**
     * Name parseCollection
     *
     * @param mixed  $collection Collection
     * @param string $nameKey    Key Name
     * @param string $valueKey   Key Value
     * @param array  $options    Options
     *
     * @return array
     */
    public function parseCollection(
        $collection,
        string $nameKey,
        string $valueKey,
        array $options = []
    ): array {
        $indexName = 'name';
        $indexValue = 'value';
        extract($options, EXTR_IF_EXISTS);

        $results = [];

        foreach ($collection as $item) {
            $results[] = [
                $indexName  => $item[$nameKey],
                $indexValue => $item[$valueKey],
            ];
        }

        return $results;
    }
}
