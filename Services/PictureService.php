<?php

namespace OctoCmsModule\Core\Services;

use Exception;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Image;
use OctoCmsModule\Core\Entities\Media;
use OctoCmsModule\Core\Entities\MediaWebp;
use OctoCmsModule\Core\Entities\Picture;
use OctoCmsModule\Core\Entities\PictureLang;
use OctoCmsModule\Core\Interfaces\PictureServiceInterface;
use Intervention\Image\Facades\Image as ImageFacade;
use Storage;
use Throwable;

/**
 * Class PictureService
 *
 * @package OctoCmsModule\Core\Services
 */
class PictureService implements PictureServiceInterface
{
    public const BUCKET_BASE_URL = 'https://storage.googleapis.com';

    public function __construct()
    {
    }

    /**
     * @param array  $pictureFields
     * @param string $path
     *
     * @return array|null
     */
    public function uploadPicture(array $pictureFields, string $path): ?array
    {
        /**
 * @var UploadedFile
*/
        $fileImage = $this->storeTempImage(Arr::get($pictureFields, 'src', ''));

        $this->cropImage(
            $fileImage,
            Arr::get($pictureFields, 'width', null),
            Arr::get($pictureFields, 'height', null)
        );

        $imagePath = Storage::disk('gcs')->putFileAs(
            $path,
            $fileImage,
            $fileImage->getBasename()
        );

        $webP = ImageFacade::make($fileImage)->encode('webp');
        $fileNameWebP = md5(uniqid()) . '.webp';
        $pathImageWebP = storage_path('temp') . '/' . $fileNameWebP;
        $webP->save($pathImageWebP);

        $webPPath = Storage::disk('gcs')->putFileAs(
            $path,
            $webP->basePath(),
            $fileNameWebP
        );

        @unlink($fileImage->getRealPath());
        @unlink($webP->basePath());

        return [
            'src'  => $imagePath,
            'webp' => $webPPath,
        ];
    }

    /**
     * @param  $entity
     * @param  array $pictures
     * @return bool
     * @throws Throwable
     */
    public function savePicturesEntity($entity, array $pictures): bool
    {

        foreach ($pictures as $pictureData) {
            $tag = Arr::get($pictureData, 'tag', Picture::TAG_MAIN);
            /**
 * @var Picture $picture
*/
            $picture = Arr::has($pictureData, 'newImage')
                ? $this->uploadPictureEntity(
                    $entity,
                    Arr::get($pictureData, 'newImage'),
                    [
                        'removePrevImage' => true,
                        'path'            => $entity::GCS_PATH . '/' . $entity->id,
                        'tag'             => $tag
                    ]
                )
                : $entity->getPictures($tag)->first();

            if (Arr::has($pictureData, 'pictureLangs') && !empty($picture)) {
                $this->savePictureLangs($picture, Arr::get($pictureData, 'pictureLangs'));
            }
        }

        return true;
    }

    /**
     * @param $entity
     * @param array $pictureFields
     * @param array $options
     *
     * @return Picture|null
     * @throws Throwable
     */
    protected function uploadPictureEntity($entity, array $pictureFields, array $options = []): ?Picture
    {
        $tag = Media::TAG_MAIN;
        $removePrevImage = false;
        $path = 'undefined';
        extract($options, EXTR_IF_EXISTS);

        DB::beginTransaction();

        try {
            if ($removePrevImage && !empty($pictures = $entity->getPictures($tag)->get())) {
                foreach ($pictures as $picture) {
                    $picture->delete();
                }
            }

            /**
 * @var array $paths
*/
            $paths = $this->uploadPicture($pictureFields, $path);

            /**
 * @var Picture $picture
*/
            $picture = new Picture(
                [
                'src'  => Arr::get($paths, 'src', ''),
                'webp' => Arr::get($paths, 'webp', ''),
                'tag'  => $tag,
                ]
            );

            $picture->picturable()->associate($entity)->save();

            DB::commit();

            return $picture;
        } catch (Throwable $e) {
            DB::rollBack();

            return null;
        }
    }

    /**
     * @param Picture $picture
     * @param array   $pictureLangs
     */
    protected function savePictureLangs(Picture $picture, array $pictureLangs)
    {
        foreach ($pictureLangs as $pictureLang) {
            PictureLang::updateOrCreate(
                [
                    'picture_id' => $picture->id,
                    'lang'       => Arr::get($pictureLang, 'lang', ''),
                ],
                [
                    'alt'         => Arr::get($pictureLang, 'alt', ''),
                    'caption'     => Arr::get($pictureLang, 'caption', ''),
                    'title'       => Arr::get($pictureLang, 'title', ''),
                    'description' => Arr::get($pictureLang, 'description', ''),
                ]
            );
        }
    }

    /**
     * @param string $src
     *
     * @return UploadedFile
     */
    protected function storeTempImage(string $src)
    {
        $extension = explode('/', mime_content_type($src))[1];
        @list($type, $file_data) = explode(';', $src);
        @list(, $file_data) = explode(',', $file_data);

        $fileName = md5(uniqid()) . '.' . $extension;
        Storage::disk('temp')->put($fileName, base64_decode($file_data));

        return new UploadedFile(storage_path('temp/' . $fileName), $fileName);
    }

    /**
     * @param $entity
     * @param string $tag
     *
     * @throws Exception
     */
    protected function removePrevImage($entity, string $tag)
    {
        /**
 * @var Media $media
*/
        $media = $entity->firstMedia($tag);
        /**
 * @var MediaWebp $mediaWebp
*/
        $mediaWebp = $media->mediaWebp;

        $entity->detachMedia($media);

        Storage::disk('gcs')->delete($media->getDiskPath());
        Storage::disk('gcs')->delete($mediaWebp->getDiskPath());

        $media->delete();
    }

    /**
     * @param UploadedFile $file
     * @param null         $width
     * @param null         $height
     */
    protected function cropImage(UploadedFile $file, $width = null, $height = null)
    {
        /**
 * @var Image
*/
        $image = ImageFacade::make($file);

        if (!empty($height) || !empty($width)) {
            $image->resize(
                $width,
                $height,
                function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                }
            );
        }

        $image->save();
    }
}
