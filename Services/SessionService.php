<?php

namespace OctoCmsModule\Core\Services;

/**
 * Class CacheService
 *
 * @package OctoCmsModule\Core\Services
 */
class SessionService
{
    /**
     * @param  string $key
     * @param  $value
     * @return bool
     */
    public static function set(string $key, $value)
    {
        session([$key => serialize($value)]);
        session()->save();
        return true;
    }

    /**
     * @param  string $key
     * @return mixed|null
     */
    public static function get(string $key)
    {
        $value = session($key, null);
        return !empty($value) ? unserialize($value) : null;
    }
}
