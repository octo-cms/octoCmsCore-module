<?php

declare(strict_types=1);

namespace OctoCmsModule\Core\Services;

use Illuminate\Support\Arr;
use OctoCmsModule\Core\Constants\CacheTagConst;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Interfaces\PermissionServiceInterface;

use function array_intersect;
use function array_unique;
use function config;
use function is_array;
use function strval;

/**
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Services
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 */
class PermissionService implements PermissionServiceInterface
{
    /** @inheritDoc **/
    public function isAllowed(User $user, $roles): bool
    {
        if (!is_array($roles)) {
            $roles = [$roles];
        }

        $userRoles = CacheService::get(CacheTagConst::USER_ROLES, strval($user->id));

        if (empty($userRoles)) {
            $userRoles = $this->getUserRoles($user);
            CacheService::set(
                CacheTagConst::USER_ROLES,
                strval($user->id),
                $userRoles
            );
        }

        return !empty(array_intersect($userRoles, $roles));
    }

    /** @inheritDoc **/
    public function getUserRoles(User $user): array
    {
        $hierarchyRoles = config('octo-cms.hierarchy_roles', []);
        $userRoles = [];
        foreach ($user->roles->pluck('name')->toArray() as $userRole) {
            $userRoles = Arr::collapse([
                $userRoles,
                [ $userRole ] ,
                Arr::get($hierarchyRoles, $userRole, [])
            ]);
        }

        return array_unique($userRoles);
    }
}
