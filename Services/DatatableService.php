<?php

namespace OctoCmsModule\Core\Services;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use OctoCmsModule\Core\DTO\DatatableDTO;
use OctoCmsModule\Core\Interfaces\DatatableServiceInterface;

/**
 * Class DatatableService
 *
 * @package OctoCmsModule\Core\Services
 */
class DatatableService implements DatatableServiceInterface
{

    /**
     * @param array   $fields
     * @param Builder $items
     * @return mixed|DatatableDTO
     */
    public function getDatatableDTO(array $fields, Builder $items)
    {

        $datatableDTO = new DatatableDTO();
        $datatableDTO->total = $items->count();

        $limit = Arr::get($fields, 'rowsInPage', 10);
        $items = $items
            ->orderBy(
                Arr::get($fields, 'order', 'id'),
                Arr::get($fields, 'direction', 'desc')
            )
            ->take($limit)
            ->offset($limit * (Arr::get($fields, 'currentPage', 1) - 1));

        $datatableDTO->builder = $items;
        $datatableDTO->currentPage = Arr::get($fields, 'currentPage', 1);
        $datatableDTO->rowsInPage = Arr::get($fields, 'rowsInPage', 10);

        return $datatableDTO;
    }
}
