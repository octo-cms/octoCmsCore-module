<?php

declare(strict_types=1);

namespace OctoCmsModule\Core\Services;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Jenssegers\Agent\Agent;
use OctoCmsModule\Core\Entities\User;
use OctoCmsModule\Core\Entities\UserEventLog;
use OctoCmsModule\Core\Interfaces\UserServiceInterface;
use OctoCmsModule\Core\Notifications\NewPasswordNotification;
use OctoCmsModule\Core\Notifications\UserRegistrationNotification;

use function collect;
use function extract;
use function optional;

use const EXTR_IF_EXISTS;

/**
 * @link     https://octopus.srl
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Services
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 */
class UserService implements UserServiceInterface
{
    /**
     * Name updateUser
     *
     * @param User          $user   user instance
     * @param array|mixed[] $fields array with user data
     */
    public function saveUser(User $user, array $fields): User
    {
        $user->fill(Arr::only($fields, ['username', 'active']));

        $user->save();

        $this->saveRoles($user, $fields);

        $this->saveCustomFields($user, Arr::get($fields, 'customFields', []));

        $user->syncEmails(Arr::get($fields, 'emails', []));
        $user->syncAddresses(Arr::get($fields, 'addresses', []));
        $user->syncPhones(Arr::get($fields, 'phones', []));

        if ($user->wasRecentlyCreated) {
            $passwordTemp = Str::random(10);
            $user->password = $passwordTemp;
            $user->load('defaultEmail');
            optional($user->defaultEmail)->notify(new UserRegistrationNotification($passwordTemp));
        }

        return $user;
    }

    /**
     * Name generateNewPassword
     *
     * @param User $user user instance
     */
    public function generateNewPassword(User $user): User
    {
        /**
         * New random generated password
         *
         * @var string $newPassword
         */
        $newPassword = Str::random(10);

        $user->password = $newPassword;

        $user->save();

        optional($user->defaultEmail)->notify(new NewPasswordNotification($newPassword));

        return $user;
    }

    /**
     * Name saveRoles
     *
     * @param User          $user   User
     * @param array|mixed[] $fields Fields
     */
    protected function saveRoles(User $user, array $fields): void
    {
        $user->roles()->sync(
            collect(Arr::get($fields, 'roles', []))->pluck('id')->toArray()
        );
    }

    /**
     * Name saveCustomFields
     *
     * @param User          $user         User
     * @param array|mixed[] $customFields array
     */
    public function saveCustomFields(User $user, array $customFields): void
    {
        foreach ($customFields as $name => $value) {
            $user->saveCustomFieldValue($name, $value);
        }
    }

    /**
     * Name saveUserEventLog
     *
     * @param User          $user   User
     * @param string        $type   Event Type
     * @param array|mixed[] $fields additional fields
     */
    public function saveUserEventLog(User $user, string $type, array $fields = []): void
    {
        // phpcs:disable
        $data = [];
        $ipAddress = null;
        extract($fields, EXTR_IF_EXISTS);
        // phpcs:enable

        /**
         * Request Agent
         *
         * @var Agent $agent Agent Instance
         */
        $agent = new Agent();

        $userEventLog = new UserEventLog(
            [
                'type'          => $type,
                'data'          => $data,
                'ip_address'    => $ipAddress,
                'browser'       => $agent->browser(),
                'browser_langs' => $agent->languages(),
                'platform'      => $agent->platform(),
                'device'        => $agent->device(),
                'device_type'   => $agent->deviceType(),
            ]
        );

        $userEventLog->user()->associate($user);

        $userEventLog->save();
    }
}
