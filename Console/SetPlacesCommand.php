<?php

namespace OctoCmsModule\Core\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

/**
 * Class SetPlacesCommand
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Console
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class SetPlacesCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'set-places';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Name handle
     *
     * @return void
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle()
    {
        $this->info('Running Set Places Command ...');

        DB::unprepared(
            File::get(
                module_path('Core') . '/Database/Seeders/places.sql'
            )
        );

        $this->info('Running Set Countries Command ...');

        DB::unprepared(
            File::get(
                module_path('Core') . '/Database/Seeders/countries.sql'
            )
        );
    }
}
