<?php

namespace OctoCmsModule\Core\Console;

use Illuminate\Console\Command;
use OctoCmsModule\Core\Entities\Setting;

/**
 * Class InstallCoreCommand
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Console
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class InstallCoreCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'install:core';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Name handle
     *
     * @return void
     */
    public function handle()
    {
        $this->info('Running Install Core Command ...');

        Setting::updateOrCreate(['name' => 'admin_mail'], ['value' => '']);
        Setting::updateOrCreate(
            ['name' => 'active_campaign'],
            [
                'value' => [
                    'enabled' => false,
                    'api_key' => null,
                    'url'     => null,
                    'list_id' => null,
                ],
            ]
        );
    }
}
