<?php

namespace OctoCmsModule\Core\Console;

use Illuminate\Console\Command;
use OctoCmsModule\Core\Entities\Role;
use OctoCmsModule\Core\Entities\User;

/**
 * Class CreateUserCommand
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Console
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class CreateUserCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'createUser {user} {psw} {role?} ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $username = $this->argument('user');
        $psw = $this->argument('psw');

        $role = $this->argument('role');

        /**
         * User
         *
         * @var User $user user instance to be created
         */
        $user = new User(
            [
                'name'              => null,
                'password'          => $psw,
                'username'          => $username,
                'email_verified_at' => null
            ]
        );

        $user->save();

        if (!empty($role)) {
            /**
             * Role
             *
             * @var Role $role
             */
            $role = Role::firstOrCreate(['name' => $role]);

            $role->users()->attach($user);

            $user->load('roles');
        }

        $this->info('User Created');
    }
}
