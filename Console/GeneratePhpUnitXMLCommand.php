<?php

namespace OctoCmsModule\Core\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Nwidart\Modules\Facades\Module;

/**
 * Class GeneratePhpUnitXMLCommand
 * Description ...
 *
 * @category Octo
 * @package  OctoCmsModule\Core\Console
 * @author   Pasi Daniele <pasidaniele@gmail.com>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class GeneratePhpUnitXMLCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate-php-unit-xml';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * GeneratePhpUnitXMLCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Name handle
     *
     * @return void
     */
    public function handle()
    {
        $modules = [];
        $path = base_path() . '/phpunit.xml';

        /**
         * Module
         *
         * @var \Nwidart\Modules\Laravel\Module $module
         */
        foreach (Module::all() as $module) {
            if ($module->isEnabled()) {
                $modules[] = [
                    'folder' => $module->getName(),
                    'suite'  => $module->getLowerName()
                ];
            }
        }

        $content = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" .
            view('core::xml', ['modules' => $modules])->render();

        @unlink($path);

        File::put($path, $content);
    }
}
