<?php

namespace OctoCmsModule\Core\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

/**
 * Class RemoveTempExcelFolder
 * Description ...
 *
 * @category Octo
 * @package OctoCmsModule\Core\Console
 * @author   Camacaro Adriano <acamacaro@octopuslab.it>
 * @license  copyright Octopus Srl 2020
 * @link     https://octopus.srl
 */
class RemoveExcelTempFolderCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remove-excel-temp-folder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Deleting storage/framework/laravel-excel directory...');

        Storage::deleteDirectory(storage_path('framework/laravel-excel'));
    }
}
