<?php

use OctoCmsModule\Core\Http\Controllers\V1\CacheController;
use OctoCmsModule\Core\Http\Controllers\V1\ModuleController;
use OctoCmsModule\Core\Http\Controllers\V1\PlaceController;
use OctoCmsModule\Core\Http\Controllers\V1\ProviderController;
use OctoCmsModule\Core\Http\Controllers\V1\ProviderImportController;
use OctoCmsModule\Core\Http\Controllers\V1\RegistryController;
use OctoCmsModule\Core\Http\Controllers\V1\RoleController;
use OctoCmsModule\Core\Http\Controllers\V1\SettingController;
use OctoCmsModule\Core\Http\Controllers\V1\TagController;
use OctoCmsModule\Core\Http\Controllers\V1\UserController;

Route::group(['prefix' => 'admin/v1'], function () {

    Route::post('login', [UserController::class, 'login'])->name('core.login');

    Route::post('recovery-password', [UserController::class, 'recoveryPassword'])
        ->name('core.recovery.password');

    Route::group(['middleware' => ['auth:sanctum']], function () {

        Route::post('logout', [UserController::class, 'logout'])->name('core.logout');

        Route::group(['prefix' => 'tags'], function () {
            Route::post('get-by-group', [TagController::class, 'getByGroup'])
                ->name('core.tags.get.by.group');
            Route::post('', [TagController::class, 'store'])->name('core.tags.store');
        });

        Route::group(['prefix' => 'modules'], function () {
            Route::get('', [ModuleController::class, 'index'])->name('core.modules.index');
        });

        Route::group(['prefix' => 'select'], function () {
            Route::group(['prefix' => 'places'], function () {
                Route::post('countries', [PlaceController::class, 'countriesIndex'])
                    ->name('core.select.places.countries');
                Route::post('regions', [PlaceController::class, 'regionsIndex'])
                    ->name('core.select.places.regions');
                Route::post('provinces', [PlaceController::class, 'provincesIndex'])
                    ->name('core.select.places.provinces');
                Route::post('cities', [PlaceController::class, 'citiesIndex'])
                    ->name('core.select.places.cities');
            });
        });

        Route::group(['prefix' => 'settings'], function () {
            Route::get('', [SettingController::class, 'index'])->name('core.settings.index');
            Route::post('', [SettingController::class, 'updateSettingByName'])
                ->name('core.settings.update.setting.by.name');
            Route::put('', [SettingController::class, 'updateSettings'])->name('core.settings.update');
        });

        Route::group(['prefix' => 'cache'], function () {
            Route::post('flush', [CacheController::class, 'flushCache'])
                ->name('core.cache.flush');
        });

        Route::group(['prefix' => 'datatables/core'], function () {
            Route::post('registries', [RegistryController::class, 'datatableIndex'])
                ->name('admin.datatables.core.registries');
            Route::post('providers', [ProviderController::class, 'datatableIndex'])
                ->name('admin.datatables.core.providers');
            Route::post('users', [UserController::class, 'datatableIndex'])
                ->name('admin.datatables.core.users');
            Route::post('provider-import/{id}/data', [ProviderImportController::class, 'datatableProviderImportData'])
                ->name('admin.datatables.core.provider.import.data');
            Route::post('provider-import', [ProviderImportController::class, 'datatableIndex'])
                ->name('admin.datatables.core.provider.import');
            Route::post('user-event-logs/{id}/accesses', [UserController::class, 'userEventLogDatatableIndexAccesses'])
                ->name('admin.datatables.core.user.event.log.accesses');
        });

        Route::group(['prefix' => 'providers'], function () {
            Route::get('', [ProviderController::class, 'index'])->name('core.providers.index');
            Route::post('', [ProviderController::class, 'store'])->name('core.providers.store');

            Route::group(['prefix' => '{id}'], function () {
                Route::get('', [ProviderController::class, 'show'])->name('core.providers.show');
                Route::delete('', [ProviderController::class, 'delete'])->name('core.providers.delete');
                Route::put('', [ProviderController::class, 'update'])->name('core.providers.update');
            }
            );

            Route::group(['prefix' => 'imports'], function () {
                Route::post('upload', [ProviderImportController::class, 'upload'])
                    ->name('core.provider.import.upload');
                Route::get('{id}', [ProviderImportController::class, 'show'])
                    ->name('core.provider.import.show');
                Route::post('{id}/registry-sync', [ProviderImportController::class, 'syncWithRegistry'])
                    ->name('core.provider.import.registry.sync');
                Route::delete('{id}', [ProviderImportController::class, 'destroy'])
                    ->name('core.provider.import.delete');
            }
            );
        }
        );

        Route::group(['prefix' => 'users'], function () {
            Route::post('', [UserController::class, 'store'])
                ->name('core.users.store');
            Route::post('update-password', [UserController::class, 'updatePassword'])
                ->name('core.users.update.password');
            Route::get('current', [UserController::class, 'currentUser'])
                ->name('core.users.current.user');
            Route::get('/{role}/index', [UserController::class, 'indexByRole'])
                ->name('core.users.index.by.role');

            Route::group(['prefix' => '{id}'], function () {
                Route::put('generate-new-password', [UserController::class, 'generateNewPassword'])
                    ->name('core.users.generate.new.password');
                Route::get('', [UserController::class, 'show'])
                    ->name('core.users.show');
                Route::put('', [UserController::class, 'update'])
                    ->name('core.users.update');

            });
        });

        Route::group(['prefix' => 'roles'], function () {
            Route::get('', [RoleController::class, 'index'])->name('core.roles.index');
        });

        Route::group(['prefix' => 'registries'], function () {
            Route::post('', [RegistryController::class, 'store'])->name('core.registries.store');
            Route::group(['prefix' => '{id}'], function () {
                Route::get('', [RegistryController::class, 'show'])
                    ->name('core.registries.show');
                Route::put('personal-data', [RegistryController::class, 'updatePersonalData'])
                    ->name('core.registries.update.personal-data');
                Route::put('', [RegistryController::class, 'update'])
                    ->name('core.registries.update');
            });
        });
    });
});
