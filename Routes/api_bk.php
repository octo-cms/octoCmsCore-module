<?php

Route::group(['prefix' => 'admin/v1'], function () {

    Route::post('login', getRouteAction("Core", "V1\UserController", 'login'))
        ->name('core.login');

    Route::post('recovery-password', getRouteAction("Core", "V1\UserController", 'recoveryPassword'))
        ->name('core.recovery.password');

    Route::group(['middleware' => ['auth:sanctum']], function () {

        Route::post('logout', getRouteAction("Core", "V1\UserController", 'logout'))
            ->name('core.logout');

        Route::group(['prefix' => 'tags'], function () {
            Route::post('get-by-group', getRouteAction("Core", "V1\TagController", 'getByGroup'))
                ->name('core.tags.get.by.group');

            Route::post('', getRouteAction("Core", "V1\TagController", 'store'))
                ->name('core.tags.store');
        });

        Route::group(['prefix' => 'modules'], function () {
            Route::get('', getRouteAction("Core", "V1\ModuleController", 'index'))
                ->name('core.modules.index');
        });

        Route::group(['prefix' => 'select'], function () {

            Route::group(['prefix' => 'places'], function () {

                Route::post('countries', getRouteAction("Core", "V1\PlaceController", 'countriesIndex'))
                    ->name('core.select.places.countries');

                Route::post('regions', getRouteAction("Core", "V1\PlaceController", 'regionsIndex'))
                    ->name('core.select.places.regions');

                Route::post('provinces', getRouteAction("Core", "V1\PlaceController", 'provincesIndex'))
                    ->name('core.select.places.provinces');

                Route::post('cities', getRouteAction("Core", "V1\PlaceController", 'citiesIndex'))
                    ->name('core.select.places.cities');
            });
        });

        Route::group(['prefix' => 'settings'], function () {
            Route::get('', getRouteAction("Core", "V1\SettingController", 'index'))
                ->name('core.settings.index');

            Route::post(
                '',
                getRouteAction("Core", "V1\SettingController", 'updateSettingByName')
            )->name('core.settings.update.setting.by.name');

            Route::put(
                '',
                getRouteAction("Core", "V1\SettingController", 'updateSettings')
            )->name('core.settings.update');
        }
        );

        Route::group(
            ['prefix' => 'cache'],
            function () {
                Route::post('flush', getRouteAction("Core", "V1\CacheController", 'flushCache'))
                    ->name('core.cache.flush');
            }
        );

        Route::group(
            ['prefix' => 'datatables/core'],
            function () {
                Route::post('registries', getRouteAction("Core", "V1\RegistryController", 'datatableIndex'))
                    ->name('admin.datatables.core.registries');
                Route::post('providers', getRouteAction("Core", "V1\ProviderController", 'datatableIndex'))
                    ->name('admin.datatables.core.providers');
                Route::post('users', getRouteAction("Core", "V1\UserController", 'datatableIndex'))
                    ->name('admin.datatables.core.users');
                Route::post(
                    'provider-import/{id}/data',
                    getRouteAction("Core", "V1\ProviderImportController", 'datatableProviderImportData')
                )->name('admin.datatables.core.provider.import.data');
                Route::post('provider-import', getRouteAction("Core", "V1\ProviderImportController", 'datatableIndex'))
                    ->name('admin.datatables.core.provider.import');
                Route::post(
                    'user-event-logs/{id}/accesses',
                    getRouteAction("Core", "V1\UserController", 'userEventLogDatatableIndexAccesses')
                )->name('admin.datatables.core.user.event.log.accesses');
            }
        );

        Route::group(
            ['prefix' => 'providers'],
            function () {
                Route::get('', getRouteAction("Core", "V1\ProviderController", 'index'))
                    ->name('core.providers.index');

                Route::post('', getRouteAction("Core", "V1\ProviderController", 'store'))
                    ->name('core.providers.store');

             /*
                 Route::get('', getRouteAction("Core", "V1\ProviderController", 'show'))
                    ->name('core.providers.show');
             */

                Route::group(
                    ['prefix' => 'imports'],
                    function () {
                        Route::post('upload', getRouteAction("Core", "V1\ProviderImportController", 'upload'))
                            ->name('core.provider.import.upload');

                        Route::get('{id}', getRouteAction("Core", "V1\ProviderImportController", 'show'))
                            ->name('core.provider.import.show');

                        Route::post('{id}/registry-sync', getRouteAction("Core", "V1\ProviderImportController", 'syncWithRegistry'))
                            ->name('core.provider.import.registry.sync');

                        Route::delete('{id}', getRouteAction("Core", "V1\ProviderImportController", 'destroy'))
                            ->name('core.provider.import.delete');
                    }
                );

                Route::group(
                    ['prefix' => '{id}'],
                    function () {
                        Route::get('', getRouteAction("Core", "V1\ProviderController", 'show'))
                            ->name('core.providers.show');
                        Route::delete('', getRouteAction("Core", "V1\ProviderController", 'delete'))
                            ->name('core.providers.delete');
                        Route::put('', getRouteAction("Core", "V1\ProviderController", 'update'))
                            ->name('core.providers.update');
                    }
                );
            }
        );

        Route::group(['prefix' => 'users'], function () {
            Route::post('', getRouteAction("Core", "V1\UserController", 'store'))
                ->name('core.users.store');
            Route::post('update-password', getRouteAction("Core", "V1\UserController", 'updatePassword'))
                ->name('core.users.update.password');
            Route::get('current', getRouteAction("Core", "V1\UserController", 'currentUser'))
                ->name('core.users.current.user');
            Route::get('/{role}/index', getRouteAction("Core", "V1\UserController", 'indexByRole'))
                ->name('core.users.index.by.role');

            Route::group(['prefix' => '{id}'], function () {
                Route::put('generate-new-password', getRouteAction("Core", "V1\UserController", 'generateNewPassword'))
                    ->name('core.users.generate.new.password');
                Route::get('', getRouteAction("Core", "V1\UserController", 'show'))
                    ->name('core.users.show');
                Route::put('', getRouteAction("Core", "V1\UserController", 'update'))
                    ->name('core.users.update');

            });
        });

        Route::group(
            ['prefix' => 'roles'],
            function () {
                Route::get('', getRouteAction("Core", "V1\RoleController", 'index'))
                    ->name('core.roles.index');
            }
        );

        Route::group(['prefix' => 'registries'], function () {
            Route::post('', getRouteAction("Core", "V1\RegistryController", 'store'))
                ->name('core.registries.store');
            Route::group(['prefix' => '{id}'], function () {
                Route::get('', getRouteAction("Core", "V1\RegistryController", 'show'))
                    ->name('core.registries.show');
                Route::put('personal-data', getRouteAction("Core", "V1\RegistryController", 'updatePersonalData'))
                    ->name('core.registries.update.personal-data');
                Route::put('', getRouteAction("Core", "V1\RegistryController", 'update'))
                    ->name('core.registries.update');
            });
        });
    }
    );
}
);
