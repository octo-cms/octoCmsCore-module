<?php

use Illuminate\Support\Facades\Route;
use OctoCmsModule\Core\Http\Controllers\VueRouteController;

Route::group(['prefix' => env('MIX_ADMIN_PREFIX'), 'middleware' => ['auth-admin']], function () {
    Route::group(['prefix' => 'core'], function () {
        Route::get('registry', [VueRouteController::class, 'registry'])
            ->name('admin.vue-route.core.registry');
        Route::get('users', [VueRouteController::class, 'users'])
            ->name('admin.vue-route.core.users');
    });
});
