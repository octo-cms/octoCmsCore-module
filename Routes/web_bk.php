<?php

use Illuminate\Support\Facades\Route;

Route::group(
    ['prefix' => env('MIX_ADMIN_PREFIX'), 'middleware' => ['auth-admin']],
    function () {
        Route::group(
            ['prefix' => 'core'],
            function () {
                Route::get(
                    'registry',
                    getRouteAction("Core", "VueRouteController", 'registry')
                )
                ->name('admin.vue-route.core.registry');
                Route::get(
                    'users',
                    getRouteAction("Core", "VueRouteController", 'users')
                )
                ->name('admin.vue-route.core.users');
            }
        );
    }
);
